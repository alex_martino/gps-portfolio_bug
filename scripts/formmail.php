<?
// Start our session, so we can track variables across pages
session_start();

// Setup our recipients
$recipient = ($_POST['recipient'] ? $_POST['recipient'] : '');
$cc = ($_POST['cc'] ? $_POST['cc'] : '');
$bcc = ($_POST['bcc'] ? $_POST['bcc'] : '');

// Reset our variables to force post vars
$subject = $_POST['subject'];
$required = $_POST['required'];
$redirect = $_POST['redirect'];
$from_address = $_POST['email_address'];

// referers.. domains/ips that you will allow forms to reside on.
$referers = array ($_SERVER['HTTP_HOST']);

// field / value seperator
define("SEPARATOR", ($separator) ? $separator : ": ");

// content newline
define("NEWLINE", ($newline) ? $newline : "\n");

// ---------- START ---------- //
// If there was a captcha form post, handle it 
if ($_SESSION['use_captcha'] == true and $_SESSION['valid_captcha'] != true) {
	exit();
}

// First check the referrers to see if they are coming from an authorized domain
if ($referers) {
	check_referer($referers);
}

// Check for a recipient email address and check the validity of it
$recipient_in = split(',', $recipient);
for ($i = 0; $i < count($recipient_in); $i++) {
   $recipient_to_test = trim($recipient_in[$i]);
   if (!eregi("^[_\\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\\.)+[a-z]{2,6}$", $recipient_to_test)) {
      print_error("<b>I NEED VALID RECIPIENT EMAIL ADDRESS ($recipient_to_test) TO CONTINUE</b>");
   }
}

// Handle the required fields
if ($required) {
	// seperate at the commas
	$required = ereg_replace( " +", "", $required);
	$required = split(",", $required);
	for ($i = 0; $i < count($required); $i++) {
		$string = trim($required[$i]);
	  
		// check if they exsist
		if (!$_POST[$string]) {
			// if the missing_fields_redirect option is on: redirect them
			if ($missing_fields_redirect) {
				header ("Location: $missing_fields_redirect");
				exit;
			}
			$missing_field_list .= "<b>Missing: $required[$i]</b><br>\n";
		}
	}
   
	// send error to our mighty error function
	if ($missing_field_list) {
		print_error($missing_field_list, "missing");
	}
}

// check the email fields for validity
if ($from_address) {
   $from_address = trim($from_address);
   if (!eregi("^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$", $from_address))
      print_error("your <b>email address</b> is invalid");
}

// sort alphabetic or prepare an order
if ($sort == "alphabetic") {
   uksort($HTTP_POST_VARS, "strnatcasecmp");
} else if ((ereg('^order:.*,.*', $sort)) && ($list = explode(',', ereg_replace('^order:', '', $sort)))) {
   $sort = $list;
}
   
// prepare the content
$content = parse_form($_POST, $sort);

// Get environmental variables
$content .= "\n------ Environmental Variables ------\n";
$content .= "REMOTE ADDR: ". $_SERVER['REMOTE_ADDR']."\n";
$content .= "BROWSER: ". $_SERVER['HTTP_USER_AGENT']."\n";

// Send it off to each recipient
mail_it(stripslashes($content), ($subject) ? stripslashes($subject) : "Form Submission", $from_address, $recipient);

// if the redirect option is set: redirect them
if ($redirect) {
   header("Location: $redirect");
   exit;
} else {
   echo "Thank you for your submission\n";
   exit;
}
// <----------    THE END    ----------> //


// Error Function
function print_error($reason, $type = 0) {
   build_body($title, $bgcolor, $text_color, $link_color, $vlink_color, $alink_color, $style_sheet);
   // for missing required data
   if ($type == "missing") {
      if ($missing_field_redirect) {
         header("Location: $missing_field_redirect?error=$reason");
         exit;
      } else {
		  ?>
		  
		  The form was not submitted for the following reasons:<p>
		  <ul><? echo $reason."\n"; ?></ul>
		  Please use your browser's back button to return to the form and try again.
		  
		  <?
      }
   } else { // every other error
		?>
	  
		<p>The form was not submitted because of the following reasons:</p>
	  	
		<?
   }

   exit();
}

// Function to check the referrers
function check_referer($referers) {
	if (count($referers)) {
		$found = false;
		
		$temp = explode("/", getenv("HTTP_REFERER"));
		$referer = $temp[2];
		
		if ($referer == "") {$referer = $_SERVER['HTTP_REFERER'];
			list($remove, $stuff) = split('//', $referer,2);
			list($home, $stuff) = split('/', $stuff,2);
			$referer = $home;
		}
		
		for ($x = 0; $x < count($referers); $x++) {
			if (eregi ($referers[$x], $referer)) {
				$found = true;
			}
		}
		
		if ($referer == "")
			$found = false;
			
		if (!$found){
			print_error("You are coming from an <b>unauthorized domain.</b>");
			error_log("Illegal Referer. (".getenv("HTTP_REFERER").")", 0);
		}
		return $found;
	} else {
         return true; // not a good idea, if empty, it will allow it.
	}
}

// This function takes the sorts, excludes certain keys and 
// makes a pretty content string.
function parse_form($array, $sort = "") {
	// build reserved keyword array
	$reserved_keys[] = "required";
	$reserved_keys[] = "redirect";
	$reserved_keys[] = "recipient";
	$reserved_keys[] = "subject";
	$reserved_keys[] = "sort";
	$reserved_keys[] = "recaptcha_challenge_field";
	$reserved_keys[] = "recaptcha_response_field";
   
   if (count($array)) {
      if (is_array($sort)) {
         foreach ($sort as $field) {
            $reserved_violation = 0;
            for ($ri = 0; $ri < count($reserved_keys); $ri++)
               if ($array[$field] == $reserved_keys[$ri]) $reserved_violation = 1;

            if ($reserved_violation != 1) {
               if (is_array($array[$field])) {
                  for ($z = 0; $z < count($array[$field]); $z++)
                     $content .= $field.SEPARATOR.$array[$field][$z].NEWLINE;
               } else
                  $content .= $field.SEPARATOR.$array[$field].NEWLINE;
            }
         }
      }
	  
      while (list($key, $val) = each($array)) {
         $reserved_violation = 0;
         for ($ri = 0; $ri < count($reserved_keys); $ri++)
            if ($key == $reserved_keys[$ri]) $reserved_violation = 1;

         for ($ri = 0; $ri < count($sort); $ri++)
            if ($key == $sort[$ri]) $reserved_violation = 1;

         // prepare content
         if ($reserved_violation != 1) {
            if (is_array($val)) {
               for ($z = 0; $z < count($val); $z++)
                  $content .= ucwords(str_replace("_", " ", $key)).SEPARATOR.$val[$z].NEWLINE;
            } else {
               $content .= ucwords(str_replace("_", " ", $key)).SEPARATOR.$val.NEWLINE;
			}
         }
      }
   }
   return $content;
}

// mail the content we figure out in the following steps
function mail_it($content, $subject, $from_address, $recipient) {
	global $cc, $bcc;
	
	$ob = "----=_OuterBoundary_000";
	$ib = "----=_InnerBoundery_001";
	
	$headers  = "MIME-Version: 1.0\n"; 
	$headers .= "From: " . $from_address . "\n"; 
	$headers .= "Reply-To: " . $from_address . "\n";
	if ($cc) {
		$headers .= "Cc: " . $cc . "\n";
	}
	if ($bcc) {
		$headers .= "Bcc: " . $bcc . "\n";
	}
	$headers .= "X-Priority: 1\n"; 
	$headers .= "X-Mailer: PHP" . phpversion() . "\n"; 
	$headers .= "Content-Type: multipart/mixed;\n\tboundary=\"" . $ob . "\"\n";
	
		  
	$message  = "This is a multi-part message in MIME format.\n";
	$message .= "\n--".$ob."\n";
	$message .= "Content-Type: multipart/alternative;\n\tboundary=\"" . $ib . "\"\n\n";
	$message .= "\n--" . $ib . "\n";
	$message .= "Content-Type: text/plain;\n\tcharset=\"iso-8859-1\"\n";
	$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
	$message .= $content . "\n\n";
	$message .= "\n--" . $ib . "--\n";
	
	// If there were any files uploaded, attach them
	foreach ($_FILES as $attachment) {
		$fp = fopen($attachment['tmp_name'],  "r");
		$attachment_chunk = fread($fp, filesize($attachment['tmp_name']));
		$attachment_chunk = base64_encode($attachment_chunk);
		$attachment_chunk = chunk_split($attachment_chunk);
		
		$message .= "\n--".$ob."\n";
		$message .= "Content-Type: " . $attachment['type'] . ";\n\tname=\"" . $attachment['name'] . "\"\n";
		$message .= "Content-Transfer-Encoding: base64\n";
		$message .= "Content-Disposition: attachment;\n\tfilename=\"" . $attachment['name'] . "\"\n\n";
		$message .= $attachment_chunk;
		$message .= "\n\n";
	}
	$message .= "\n--" . $ob . "--\n";
	
	mail($recipient, $subject, $message, $headers);
}

// take in the body building arguments and build the body tag for page display
function build_body($title, $bgcolor, $text_color, $link_color) {
	if ($title) {
		echo "<title>$title</title>\n";
	}
	$bgcolor = "#FFFFFF";
	$text_color = "#000000";
	$link_color = "#0000FF";
	echo "<body bgcolor=\"$bgcolor\" text=\"$text_color\" link=\"$link_color\">\n\n";
}
?>