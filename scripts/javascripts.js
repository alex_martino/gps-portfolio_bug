function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<!-- Create Pop-up Window -->
<!-- Begin
function openWindow(mypage, myname, w, h, scroll, resizable) {
	var winl = (screen.width - w) / 2;
	var wint = (screen.height - h) / 2;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',resizable='+resizable+',scrollbars='+scroll+',status=no,toolbar=no';
	win = window.open(mypage, myname, winprops)
}
//  End -->

<!-- Get the location of an object and move a layer onto it -- >
<!-- Begin
function seofindx(obj) {
	var curleft = 0;
	if(obj.offsetParent) {
		while(1) {
		  curleft += obj.offsetLeft;
			if(!obj.offsetParent) {
				break;
			}
		  obj = obj.offsetParent;
		}
	} else if(obj.x) {
		curleft += obj.x;
	}
	
	return curleft;
}

function seofindy(obj) {
	var curtop = 0;
	if(obj.offsetParent) {
		while(1) {
			curtop += obj.offsetTop;
			if(!obj.offsetParent) {
				break;
			}
			obj = obj.offsetParent;
		}
	} else if(obj.y) {
		curtop += obj.y;
	}
	
	return curtop;
}

function seomv() {
	var newx = seofindx(document.getElementById('seoanchor'));
	var newy = seofindy(document.getElementById('seoanchor'));
	document.getElementById('seodiv').style.left = newx + 'px';
	document.getElementById('seodiv').style.top = newy + 'px';
}
//  End -->

<!-- Check form for required fields and submit -->
<!-- Begin
function checkform(of) {
	// Test if DOM is available and there is an element called required
	if(!document.getElementById || !document.createTextNode){return;}
	if(!document.getElementById('required')){return;}

	// Define error messages and split the required fields
	var error = false;
	var errorClass = 'error_field'
	var errorMsg = 'One or more required fields are empty or invalid';
	var reqfields = document.getElementById('required').value.split(',');

	// remove classes from the required fields
	for(var i = 0; i < reqfields.length; i++) {
		var f = document.getElementById(reqfields[i]);
		if(!f){continue;}
		f.className = '';
	}
	
	// loop over required fields
	for(var i = 0; i < reqfields.length; i++) {
		// check if required field is there
		var f = document.getElementById(reqfields[i]);
		
		if(!f){continue;}
		
		// test if the required field has an error, 
		// according to its type
		switch(f.type.toLowerCase()) {
			case 'text':
				if(f.value == '' && f.id != 'email' && f.id != 'email_address'){cf_adderr(f); error = true;}							
				if((f.id == 'email' || f.id == 'email_address') && !cf_isEmailAddr(f.value)){cf_adderr(f); error = true;}							
			break;
			case 'textarea':
				if(f.value == ''){cf_adderr(f); error = true;}							
			break;
			case 'checkbox':
				if(!f.checked){cf_adderr(f); error = true;}							
			break;
			case 'select-one':
				if(!f.selectedIndex && f.selectedIndex == 0){cf_adderr(f); error = true;}							
			break;
		}
	}
	
	if (error == true) {
		alert(errorMsg);
		return false;
	} else {
		return true;
	}

	/* Tool methods */
	function cf_adderr(o) {
		// Switch the focus to the first invalid field
		if (error == false) {
			f.focus();
		}
		
		o.className = errorClass;
	}
	
	function cf_isEmailAddr(str) {
		return str.match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/);
	}
}

function submitform(form_name, useCaptcha) {
	var form = document.getElementById(form_name);
	
	if (checkform(form) == false) {
		return;
	}
	
	if (useCaptcha == true) {
		checkCaptcha(form);
	} else {
		form.submit();
	}
}

function checkCaptcha(form) {
	var connection = new Ext.data.Connection().request({
		url: '/includes/captcha/ajax_response.php',
		method: 'POST',
		params: {
			'recaptcha_response_field': Ext.get('recaptcha_response_field').getValue(),
			'recaptcha_challenge_field': Ext.get('recaptcha_challenge_field').getValue()
		},
		success: function(o) {
			var response = Ext.util.JSON.decode(o.responseText);
			
			if (response.success == true) {
				form.submit();
			} else {
				alert('Verification code incorrect, please try again.');
				Recaptcha.reload();
			}
		}
	});
}
//  End -->

<!-- Capitolize First Letters On All Word -->
<!-- Begin
function UpperCaseWords(object) {
	var index;
	var tmpStr = object.value;
	var tmpChar;
	var preString;
	var postString;
	var strlen = tmpStr.length;
	
	if (strlen > 0)  {
		for (index = 0; index < strlen; index++)  {
			if (index == 0)  {
				tmpChar = tmpStr.substring(0, 1).toUpperCase();
				postString = tmpStr.substring(1, strlen);
				tmpStr = tmpChar + postString;
			} else {
				tmpChar = tmpStr.substring(index, index + 1);
				if (tmpChar == " " && index < (strlen - 1))  {
					tmpChar = tmpStr.substring(index + 1, index + 2).toUpperCase();
					preString = tmpStr.substring(0, index + 1);
					postString = tmpStr.substring(index + 2, strlen);
					tmpStr = preString + tmpChar + postString;
				}
			}
		}
	}
	
	object.value = tmpStr;
}
//  End -->

<!-- Capitolize All Letters On Word -->
<!-- Begin
function UpperCase(object) {
	var tmpStr = object.value;
	
	tmpStr = tmpStr.toUpperCase();
	object.value = tmpStr;
}
//  End -->

<!-- Format Phone Number -->
<!-- Begin
function FormatPhoneNumber(object) {
	var tmpStr = object.value;
	
	// First just check if the number is long enough or if they are just messing around
	if (tmpStr != '' && tmpStr.length < 7) {
		alert("Invalid Phone Number Format ex. 555-555-5555");
		object.focus();
		object.select();
		stop();
	}
	
	// Now really do some checking
	tmpStr = tmpStr.replace(/\./g, "");
	tmpStr = tmpStr.replace(/-/g, "");
	tmpStr = tmpStr.replace(/[^0-9]/g, "");
	
	if (tmpStr != "") {
		if (tmpStr.length == 10) {
			object.value = tmpStr.substr(0, 3) + "-" + tmpStr.substr(3, 3) + "-" + tmpStr.substr(6, 4);
		} else {
			alert("Invalid Phone Number Format ex. 555-555-5555");
			object.focus();
			object.select();
		}
	}
}
//  End -->

<!-- Format Email Address -->
<!-- Begin
function FormatEmailAddress(object) {
	var tmpStr = object.value;
	
	if (tmpStr.match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/)) {
	} else {
		alert("Invalid Email Address Format ex. user@domain.com");
		object.focus();
		object.select();
	}
}
//  End -->