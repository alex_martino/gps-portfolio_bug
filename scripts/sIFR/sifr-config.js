var arial = {
    src: '/scripts/sIFR/fonts/arial.swf'
};

var avenir = {
    src: '/scripts/sIFR/fonts/avenir.swf'
};

sIFR.activate(arial, avenir);

// Setup some misc options
sIFR.useStyleCheck = true;
sIFR.fitExactly = true;
sIFR.forceClear = true;
sIFR.repaintOnResize = true;

// To get ratios for each font use the following code
//sIFR.debug.ratios({src: '/scripts/sIFR/fonts/p22_grenville.swf', selector: 'h1' });

/* Replace h2 tags */
sIFR.replace(arial, {
	selector: 'h2',
	css: [
		'.sIFR-root { color: #000000; font-size: 14px; }'
	],
	forceSingleLine: true,
	transparent: true
});

/* Replace top menu links */
sIFR.replace(avenir, {
	selector: '#top_menu_container .menu_item',
	css: [
		'.sIFR-root { background-color: #8E8E91; color: #FFFFFF; font-size: 11px; }',
		'a { text-decoration: none; }',
		'a:link { color: #FFFFFF; }',
		'a:hover { color: #d9e2fc; }'
	],
	forceSingleLine: true,
	transparent: true,
	offsetTop: 3,
	filters: {
		DropShadow: {
			color: '#6c5948',
			angle: 120,
			distance: 1,
			strength: 2,
			knockout: false
		}
	}
});

/* Replace side menu links */
sIFR.replace(arial, {
	selector: '#side_menu_container div',
	css: [
		'.sIFR-root { background-color: #8E8E91; color: #FFFFFF; font-size: 11px; }',
		'a { text-decoration: none; }',
		'a:link { color: #FFFFFF; }',
		'a:hover { color: #091c5a; }'
	],
	forceSingleLine: true,
	transparent: true,
	offsetTop: 2
});