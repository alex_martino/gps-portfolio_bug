<div><img src="/content_files/headers/payments.gif" width="800" height="90">

<% if $smarty.get.pay_success %>
<div id="successbar">
<img src="/content_files/images/icons/notification.png" style="vertical-align: text-bottom; width: 18px; height: 15px">

Invoice successfully paid - thank you!

</div>
<% /if %>


</div>
<script>
function invoice_open(id) {
	if (id != -1) {
		window.open("https://www.gps100.com/includes/payments/invoice_details.php?id="+id,"invoice","width=900, height=600, left="+((screen.width/2)-450)+",top="+((screen.height/2)-330));
	}
	else {
    	$( "#non_portal_dues_explanation" ).dialog({
    		resizable: false
    	});
	}
}
function pay_outstanding() {
boxes = document.getElementsByName("do_pay").length;
pay_ids = "";
for (i = 0; i < boxes; i++) {
if (document.getElementsByName("do_pay")[i].checked) {
pay_ids = pay_ids + document.getElementsByName("do_pay")[i].value + ",";
}
}
if (pay_ids.length > 0) {
window.location="https://www.gps100.com/portal/payments/pay_invoice.php?id="+pay_ids;
}
else {
alert("Please select an invoice to pay by ticking the checkbox next to the invoice.");
}
}
</script>

<div>
<h2>Invoices</h2>
<% if ($active_payments_count > 0) %>
<table class="admin_table" style="width: 670px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 110px">Invoice Date</th>
<th style="width: 110px">Due Date</th>
<th style="width: 350px">Description</th>
<th style="width: 60px">Amount</th>
<th style="width: 80px">Pay</th>
</tr>
<% foreach from=$payments.active item=i %>
<tr class="row">
<td onClick="invoice_open('<% $i.id %>');"><% $i.created|date_format:"%d %b %Y" %></td>
<td onClick="invoice_open('<% $i.id %>');"><% $i.due_date|date_format:"%d %b %Y" %></td>
<td onClick="invoice_open('<% $i.id %>');"><% if ($i.annual_dues_flag) %>Annual dues: <% /if %><% $i.description %></td>
<td onClick="invoice_open('<% $i.id %>');">$<% $i.amount %></td>
<td style="cursor: default"><input type="checkbox" name="do_pay" value="<% $i.id %>" style="margin-top: 1px" checked></td>
</tr>
<% /foreach %>
</table>
<BR>
<span class="button default strong"><input type="button" onClick="pay_outstanding();" value="Make Payment"></span>

<!--<b>GPS Payments are currently unavailable</b> - sorry for the inconvenience. We'll update you when they become available again.-->

<% else %>
You have no active invoices.
<% /if %>
<div style="clear:both">
<BR>&nbsp;
</div>

<h2>Annual Dues</h2>
<table>
	<tr>
		<td>Dues cap:</td>
		<td>$<% $payments.dues_cap_amount|number_format:2:".":"," %></td>
	</tr>
	<tr>
		<td>Dues paid:</td>
		<td>$<% $payments.total_annual_paid|number_format:2:".":"," %> (thank you!)</td>
	</tr>
	<tr>
		<td>Remaining:</td>
		<td>$<% $payments.total_annual_dues_remaining|number_format:2:".":"," %></td>
	</tr>
</table>

<BR>

<h2>Past Payments</h2>
<% if ($past_payments_count > 0) %>

<table class="admin_table" style="width: 670px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 110px">Invoice Date</th>
<th style="width: 110px">Due Date</th>
<th style="width: 350px">Description</th>
<th style="width: 60px">Amount</th>
<th style="width: 80px">Status</th>
</tr>
<% foreach from=$payments.past item=i %>
<tr class="row">
<td onClick="invoice_open('<% $i.id %>');">
	<% if $i.created|count_characters < 1 %>
		-
	<% else %>
		<% $i.created|date_format:"%d %b %Y" %>
	<% /if %>
</td>
<td onClick="invoice_open('<% $i.id %>');">
	<% if $i.due_date|count_characters < 1 %>
		-
	<% else %>
		<% $i.due_date|date_format:"%d %b %Y" %>
	<% /if %>
</td>
<td onClick="invoice_open('<% $i.id %>');"><% if ($i.annual_dues_flag) %>Annual dues: <% /if %><% $i.description %></td>
<td onClick="invoice_open('<% $i.id %>');">$<% $i.amount %></td>
<td onClick="invoice_open('<% $i.id %>');"><% $i.status|capitalize %></td>
</tr>
<% /foreach %>
</table>


<div id="non_portal_dues_explanation" title="Dues paid via PayPal" style="display: none">
These are dues that were paid via PayPal, before we started using the Portal for collecting dues.<BR><BR>
Please contact payments@gps100.com if you believe this total amount is incorrect.
</div>

<% else %>
You have not made any payments yet.
<% /if %>

</div>