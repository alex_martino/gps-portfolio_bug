<div><img src="/content_files/headers/vote.gif" width="800" height="90"></div>
<div>
<% if ($add_success) %>
<B>Survey successfully added!</b><BR><BR>
<% /if %>
<% if $previous %>
<h2>Past Surveys</h2>
<p><i>The below surveys are no longer active.</i></p>
<% else %>
<h2>Current Surveys</h2>
<% /if %>
<% if ($user_can_add_survey && $previous != 1) %>
<div style="width:100%; text-align: center">
<span class="button default strong"><input type="button" value="Add Survey" class="submit" id="addbutton" onClick="showform(); return false;" /></span>
</div>
<% /if %>

<% if !$nosurveys %>

<% foreach from=$surveys item=i %>
<% assign var=myid value=$i.id %>
<% assign var=catid value=$i.cat_id-1 %>
<div style="margin-top:10px">

<form class="vote" method="post" action="index.php?pollid=<% $i.id %>">
<fieldset>
<legend><% $categories.$catid.name %></legend>

<% if ($already_voted.$myid || $previous == 1) %>
<div style="float: right; font-size: 11px; color: #15428b">
<% $difftext.$myid %>
</div>
<p class="question">
<% $i.question %>
<% if ($user_can_download_survey) %>
<a href="/portal/admin/surveys.php?download=<% $myid %>"><img src="/content_files/images/icons/download.gif" style="width: 15px; height: 15px"></a>
<% /if %>
</p>

<div style="float: left; line-height: 1.6">
<% foreach from=$vresults.$myid.option item=y key=z %>
<% $y.name %><BR>
<% /foreach %>
</div>
<div style="float: left; margin-left: 10px">
<% foreach from=$vresults.$myid.option item=y key=z %>
<% if $y.total > 0 %>
<div style="height: 15px; width: <% $y.width %>px; background: <% $y.color %>; color:<% $y.fcolor %>; margin: 3px 10px 4px 0; text-align: right"><div style="margin-right: 2px"><% $y.percentage %>%</div></div>
<% /if %>
<% /foreach %>
</div>
<div style="clear:both">
<BR>
<% if ($vresults.$myid.expiry < 604800) %>
<div style="float:right">
	<span class="button default strong"><input type="button" value="+1 Day" class="button" onClick="extend_survey('<% $myid %>',1);" /></span>
	<span class="button default strong"><input type="button" value="+1 Week" class="button" onClick="extend_survey('<% $myid %>',7);" /></span>
</div>
<% /if %>
Total Votes: <% $vresults.$myid.total_votes %>
</div>
<% else %>
<div style="float: right; font-size: 11px; color: #15428b">
<% $difftext.$myid %>
</div>
<p class="question">
<% $i.question %>
</p>
<% if ($i.multiple != 1) %>
<p>
<% foreach from=$options.$myid item=j key=m %>
<input type="radio" name="vote" value="<% $m %>" /> <% $j.name %><br />
<% /foreach %>
	</p>
<% else %>
<p>
<% foreach from=$options.$myid item=j key=m %>
<input type="checkbox" name="vote[]" value="<% $m %>" /> <% $j.name %><br />
<% /foreach %>
	</p>
<% /if %>

	<p>
<span class="button default strong"><input type="submit" value="Vote" class="submit" /></span>
	</p>
<% /if %>

	</fieldset>

	</form>
</div>
<% /foreach %>
<% elseif $previous %>
<p>There are no old surveys yet.</p>
<% else %>
<p>There are no active surveys.</p>
<% /if %>



<% if ($user_can_add_survey) %>
<div style="clear:both">&nbsp;</div>
    <div id="page_screen">

        &nbsp;
    </div>


<div id="addform" style="display:none; border-radius:15px; -moz-border-radius: 15px; height: 340px">
<% if $error %>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><% $errormsg %></div>
<% /if %>
<form name="addform" action="index.php?addsurvey=1" method="POST">
<div class="network_block_edit">
<div class="dataform">
<div class="data_title_r">
Question<BR>
Category<BR>
Deadline Date<BR>
Deadline Time
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="question" class="edittextw" value="" MAXLENGTH=255><BR>
<select name="category">
<option value="0">** Please Select **</option>
<% foreach from=$categories item=i %>
<option value="<% $i.id %>"><% $i.name %></option>
<% /foreach %>
</select><BR>
<select name="deadline_month">
<% foreach from=$months item=i %>
<option value="<% $i.0 %>"><% $i.1 %></option>
<% /foreach %>
</select>
<select name="deadline_day">
<% foreach from=$days item=i %>
<option value="<% $i %>"><% $i %></option>
<% /foreach %>
</select>
<select name="deadline_year">
<% foreach from=$years item=i %>
<option value="<% $i %>"><% $i %></option>
<% /foreach %>
</select>
<BR>
<select name="deadline_hour">
<% foreach from=$hours item=i %>
<option value="<% $i %>"><% $i %></option>
<% /foreach %>
</select>
:
<select name="deadline_minute">
<% foreach from=$minutes item=i %>
<option value="<% $i %>"><% $i %></option>
<% /foreach %>
</select>
EST
</div>
<BR>
<hr class="editline">
<div class="data_title_r">
Response 1<BR>
Response 2<BR>
Response 3<BR>
Response 4<BR>
Response 5
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="response1" class="edittextw" value="<% $post.response1 %>" MAXLENGTH=50><BR>
<input type="text" name="response2" class="edittextw" value="<% $post.response2 %>" MAXLENGTH=50><BR>
<input type="text" name="response3" class="edittextw" value="<% $post.response3 %>" MAXLENGTH=50><BR>
<input type="text" name="response4" class="edittextw" value="<% $post.response4 %>" MAXLENGTH=50><BR>
<input type="text" name="response5" class="edittextw" value="<% $post.response5 %>" MAXLENGTH=50>
</div>
<hr class="editline">
<div class="data_title_r">
Type
</div>
<div style="float:left" class="data_data_edit">
<select name="type" style="margin-bottom: 5px" onChange="update_explain();">
<option value="radio">One-response only</option>
<option value="checkbox">Multiple responses</option>
</select>
<span id="type_explain" style="font-size:11px">I.e. user can select just one of the above responses</span>
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>
</div>

<script>
document.addform.deadline_month.value = "<% $today.month %>";
document.addform.deadline_day.value = "<% $today.day %>";
document.addform.deadline_hour.value = "19";
document.addform.deadline_minute.value = "00";
<% if $error %>
showform();
<% /if %>
function extend_survey(id,length) {
	if (length == 1) { length_str = "1 day"; }
	else if (length == 7) { length_str = "1 week"; }
	if (confirm("Are you sure you want to extend the vote by "+length_str+"?")) {
		window.location = "./?extend_survey="+id+"&extend="+length;
	}
}
</script>

<% /if %>

</div>
