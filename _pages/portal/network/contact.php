<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>
<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>

<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<a href="#" onClick="document.do_edit.submit(); return false;">Save</a> |
<a href="contact.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="contact.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>

</div>

<% if ($is_editing) %>

<form name="do_edit" action="contact.php?id=<% $profile_id %>&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Gmail Address</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">

			<input type="text" name="gmail_address" value="<% $profile.gmail_address %>" id="gmail_address" style="width: 250px" />
<BR>
		<p class="note" style="margin-top: 3px">This is useful for when we need to share Google Docs</p>

<% if $errormsg.gmail %>
<div class="error" style="margin-left: 0"><% $errormsg.gmail %></div>
<% /if %>

</div>

</div>

<BR><BR>


		</fieldset>


		<fieldset>
		<legend>Contact Number</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<select name="contact_number_country">
<option value="225_1">United States (1)</option>
<option value="224_44">United Kingdom (44)</option>
<% foreach from=$countries item=i key=key %>
<option value="<% $i.id %>_<% $i.country_code %>" <% if ($profile.contact_number_country_id == $i.id) %>SELECTED<% /if %>><% $i.country_name %> (<% $i.country_code %>)</option>
<% /foreach %>
</select>
			<input type="text" name="contact_number" value="<% $profile.contact_number %>" id="contact_number" />

<% if $errormsg.number %>
<BR><BR><div class="error" style="margin-left: 0"><% $errormsg.number %></div>
<% /if %>

</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Skype Username</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" name="skype" value="<% $profile.skype %>" id="skype" />

<% if $errormsg.skype %>
<BR><BR><div class="error" style="margin-left: 0"><% $errormsg.skype %></div>
<% /if %>

</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Email Redirect (Not editable)</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" value="<% $profile.email_address %>" style="width: 250px" readonly />
<BR>
		<p class="note" style="margin-top: 3px">To update where this redirects, please contact the VP of IR</p>
</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Mailing Address</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<textarea name="address" id="mailing_address" style="width: 400px; height: 70px"><% $profile.address %></textarea>
<BR><BR>
			<input type="checkbox" name="address_private" value="1"<% if ($profile.address_private) %> checked<% /if %>> Make address confidential (UM & Board only)
</div>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Alternative Email</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">

			<input type="text" name="alternative_email_1" value="<% $profile.alternative_email_1 %>" id="alternative_email_1" style="width: 250px" />
<BR>
		<p class="note" style="margin-top: 3px">If you would like to be able to send in articles from another email address, add the email here.</p>

<% if $errormsg.alternative_email_1 %>
<div class="error" style="margin-left: 0"><% $errormsg.alternative_email_1 %></div>
<% /if %>

</div>

</div>

<BR><BR>


		</fieldset>

</div>

</form>

<% else %>

<div class="network_data">

<% if $profile.email_address %>
<div id="sectors" class="network_block_top">
<div style="float:left" class="data_title">
Email
</div>
<div style="float:left" class="data_data">
<% $profile.email_address %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $profile.gmail_address %>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Gmail
</div>
<div style="float:left" class="data_data">
<% $profile.gmail_address %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $profile.contact_number %>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Contact Number
</div>
<div style="float:left" class="data_data">
<% if ($profile.valid_us_number) %>
<% $profile.valid_us_number %>
<% else %>
<% $profile.contact_number_country_code %> <% $profile.contact_number %>
<% /if %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $profile.skype %>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Skype
</div>
<div style="float:left" class="data_data">
<% $profile.skype %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $profile.address && !$profile.address_private %>
<div id="address" class="network_block">
<div style="float:left" class="data_title">
Mailing Address
</div>
<div style="float:left" class="data_data">
<% $profile.address|replace:"\n":"<BR>" %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $can_edit %>
<div id="sectors" class="network_block">
<div style="float:left" class="data_title">
Alternative Email
</div>
<div style="float:left" class="data_data">
Only you can see this email address - <a href="contact.php?id=<% $profile_id %>&edit=1">view it</a>.
</div>
</div>
<BR><BR>
<% /if %>

</div>

<% /if %>

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
