<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>
<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>

<% if $is_editing %>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('hometowns', {unique: true, plugins: {autocomplete: {}}});
<% foreach from= $tags.hometown item=i key=ttype %>
t1.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_hometown.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();	
			});

</script>
<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple',
		editor_selector: 'mceEditor'
	});
</script>
<% /if %>

<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<span id="savelink">Please wait...</span> |
<a href="overview.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="overview.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% if $profile.id == '102' %>Lord <% /if %><% if $profile.id == '96' %>Lady <% /if %><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" id="profile_img" src="<% $profile.current_photo %><% if $smarty.get.profile_update %>?<% $smarty.now %><% /if %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>" />

<% if $profile.school_photo %>
<script>
function time_back() {
$('profile_img').src='<% $profile.school_photo %>?<% $smarty.now %>';
document.getElementById("time_back").style.display="none";
document.getElementById("time_forward").style.display="block";
return false;
}
function time_forward() {
$('profile_img').src='<% $profile.current_photo %>';
document.getElementById("time_forward").style.display="none";
document.getElementById("time_back").style.display="block";
return false;
}

</script>
<div style="width: 100%; text-align: center; margin: 5px 0 0 0" id="time_back"><a href="#" onClick="time_back();">Time Travel</a></div>
<div style="width: 100%; text-align: center; margin: 5px 0 0 0; display: none" id="time_forward"><a href="#" onClick="time_forward();">Return to the Future</a></div>
<% /if %>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>

</div>

<% if ($is_editing) %>

<% if ($error) %>
<div class="error"><% $errormsg %></div>
<% /if %>

<form name="do_edit" action="overview.php?id=<% $profile_id %>&edit=1" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">

		<fieldset>
		<legend>Hometowns</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
			<input type="text" name="hometowns" value="" id="hometowns" />
<BR>
		<p class="note" style="margin-top: 3px">Anywhere you call home.. E.g. London, UK or New York, NY</p>
</div>
</div>

<BR><BR>

</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Public Biography</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="biography" name="biography" style="width: 500px; height: 200px" class="mceEditor"><% $profile.biography %></textarea>
<BR>
		<p class="note" style="margin-top: 3px">Your biography is visible on your <a href="/membership/member.php?id=<% $profile.id %>">public profile</a>.</p>
</div>
</div>

<BR><BR>

</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Photos</legend>
		<p class="note">Ideally images should be around 200px in width and in 'portrait' layout.</p>
		<p style="float: left;">Current Photo:<br />
			<input type="file" id="current_photo" name="current_photo" value="" style="width: 230px;" />
		</p>
		<p style="float: right;">School Photo:<br />
			<input type="file" id="school_photo" name="school_photo" value="" style="width: 230px;" />
		</p>
		<br style="clear: both;" />
		<div style="float: left; width: 230px;">
			<% if strlen($profile.current_photo) > 0 %>
			<img src="<% $profile.current_photo %>" width="150" alt="" />
			<div style="text-align:center; width:150px; padding-top: 6px">Delete? <input type="checkbox" name="current_photo_del" value=1></div><% /if %>
		</div>
		<div style="float: right; width: 230px;">
			<% if strlen($profile.school_photo) > 0 %>
			<img src="<% $profile.school_photo %>" width="150" alt="" />
			<div style="text-align:center; width:150px; padding-top: 6px">Delete? <input type="checkbox" name="current_school_del" value=1></div><% /if %>
		</div>
		<br style="clear: both;" />
		</fieldset>

</div>

<div class="network_data" style="width: 580px; float: right; margin-right: 5px">

		<fieldset>
		<legend>Birth Date</legend>

<div style="float:left">
		<div class="form_friends" style="float: left; margin-left: 15px">
<select name="birthday_day">
<option value=0 SELECTED>Day</option>
<% foreach from=$days item=i %>
<option value="<% $i %>" <% if $profile.birth_date|date_format:"%d" == $i && $profile.birth_date != "0000-00-00" %> SELECTED<% /if %>><% $i %></option>
<% /foreach %>
</select>

<select name="birthday_month">
<option value=0 SELECTED>Month</option>
<% foreach from=$months item=i %>
<option value="<% $i.0 %>" <% if $profile.birth_date|date_format:"%m" == $i.0 && $profile.birth_date != "0000-00-00" %> SELECTED<% /if %>><% $i.1 %></option>
<% /foreach %>
</select>

<select name="birthday_year">
<option value=0 SELECTED>Year</option>
<% foreach from=$years item=i %>
<option value="<% $i %>" <% if $profile.birth_date|date_format:"%Y" == $i && $profile.birth_date != "0000-00-00" %> SELECTED<% /if %>><% $i %></option>
<% /foreach %>
</select>
</div>
</div>

<BR><BR>

</fieldset>

</div>
</form>

<% else %>

<div class="network_data">

<% if ($profile.id == 173) %>
<div id="relationship_status" class="network_block_top">
<div style="float:left" class="data_title">
Relationship Status
</div>
<div style="float:left" class="data_data">
In a secret relationship with <a href="http://www.gps100.com/portal/network/view/overview.php?id=143">Raina 'Roofie' Gandhi</a>
</div>
</div>

<% elseif ($profile.id == 143) %>
<div id="relationship_status" class="network_block_top">
<div style="float:left" class="data_title">
Relationship Status
</div>
<div style="float:left" class="data_data">
In a secret relationship with <a href="http://www.gps100.com/portal/network/view/overview.php?id=173">Thomas 'Stud' Gorman</a>
</div>
</div>

<% else %>

<div id="position" class="network_block_top">
<div style="float:left" class="data_title">
Position
</div>
<div style="float:left" class="data_data">
<% $profile.title %>
</div>
</div>

<% /if %>

<BR>
<div id="university" class="network_block">
<div style="float:left" class="data_title">
University
</div>
<div style="float:left" class="data_data">
<a href="/portal/network/search.php?s=1&uni=<% $profile.ug_school_id %>"><% $profile.uni_fname %></a> | <a href="/portal/network/search.php?s=1&year=<% $profile.graduation_year %>"><% $profile.graduation_year %></a>
</div>
</div>

<% if $profile.current_employer %>
<BR><BR>
<div id="employer" class="network_block">
<div style="float:left" class="data_title">
Employer
</div>
<div style="float:left" class="data_data">
<% $profile.current_employer %>
</div>
</div>
<% /if %>

<% if $tags.hometown %>
<BR><BR>
<div id="hometowns" class="network_block">
<div style="float:left" class="data_title">
Hometown<% if ($tags.hometown|@count > 1)%>s<% /if %>
</div>
<div style="float:left" class="data_data">
<% foreach from=$tags.hometown item=i key=key %>
<span class="button red small"><a href="../search.php?qs=1&ttype=hometown&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<% /if %>

<% if $profile.birth_date != "0000-00-00" %>
<BR><BR>
<div id="Birthday" class="network_block">
<div style="float:left" class="data_title">
Birthday
</div>
<div style="float:left" class="data_data">
<% if $profile.birth_date|date_format:"%Y" != "0" %>
<% $profile.birth_date|date_format:"%B %e, %Y" %>
<% else %>
<% $profile.birth_date|date_format:"%B %e" %>
<% /if %>
</div>
</div>
<BR><BR>
<% /if %>

</div>

<% /if %>

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
