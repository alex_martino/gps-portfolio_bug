<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>
<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>
<% if $is_editing %>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('investors', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.investor item=i key=ttype %>
t1.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_investor.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t2 = new TextboxList('philosophers', {unique: true, plugins: {autocomplete: {}}});

<% foreach from = $tags.philosopher item=i key=ttype %>
t2.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t2.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_philosopher.php', onSuccess: function(r){
					t2.plugins['autocomplete'].setValues(r);
					t2.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t3 = new TextboxList('books', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.book item=i key=ttype %>
t3.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t3.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_book.php', onSuccess: function(r){
					t3.plugins['autocomplete'].setValues(r);
					t3.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t4 = new TextboxList('movies', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.movie item=i key=ttype %>
t4.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t4.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_movie.php', onSuccess: function(r){
					t4.plugins['autocomplete'].setValues(r);
					t4.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t5 = new TextboxList('television', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.television item=i key=ttype %>
t5.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t5.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_television.php', onSuccess: function(r){
					t5.plugins['autocomplete'].setValues(r);
					t5.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();
			
								
			});

function checkApostrophe(field) {
alert(field);
}

</script>

<% /if %>
<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<span id="savelink">Please wait...</span> |
<a href="interests.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="interests.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>
</div>

<div style="float: right; width: 610px">

<% if $error %>
<div class="error"><% $errormsg %></div>
<% /if %>

<% if ($is_editing) %>

<form name="do_edit" action="interests.php?id=<% $profile_id %>&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Investors</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="investors" value="" id="investors" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Philosophers</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="philosophers" value="" id="philosophers" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Books</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="books" value="" id="books" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Movies</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="movies" value="" id="movies" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Television</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="television" value="" id="television" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

</form>

		
<% else %>

<div class="network_data">

<% if $tags.investor %>
<div id="investors" class="network_block_top">
<div style="float:left" class="data_title">
Investors
</div>
<div style="float:left" class="data_data">
<% foreach from=$tags.investor item=i key=key %>
<span class="button black small"><a href="../search.php?qs=1&ttype=investor&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<% /if %>
<% if $tags.philosopher %>
<div id="philosophers" class="network_block">
<div style="float:left" class="data_title">
Philosophers
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.philosopher item=i key=ttype %>
<span class="button red small"><a href="../search.php?qs=1&ttype=philosopher&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<% /if %>
<% if $tags.book %>
<div id="books" class="network_block">
<div style="float:left" class="data_title">
Books
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.book item=i key=ttype %>
<span class="button blue small"><a href="../search.php?qs=1&ttype=book&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<% /if %>
<% if $tags.movie %>
<div id="movies" class="network_block">
<div style="float:left" class="data_title">
Movies
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.movie item=i key=ttype %>
<span class="button green small"><a href="../search.php?qs=1&ttype=movie&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<div style="line-height:1px">&nbsp;</div>
<% /if %>
<% if $tags.television %>
<div id="television" class="network_block">
<div style="float:left" class="data_title">
Television
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.television item=i key=ttype %>
<span class="button black small"><a href="../search.php?qs=1&ttype=television&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<BR><BR>
<% /if %>


</div>

<% /if %>

</div> <!--end float right -->

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
