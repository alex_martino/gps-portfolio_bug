<div><img src="/content_files/headers/network.gif" width="800" height="90">

<% if $not_viewable %>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<% else %>

<div id="network_t_menu">
  <ul>
<% foreach from=$network_t_menu item=i %>
    <li<% if $i.current == "1" %> id="current"<% /if %>><a href="<% $i.url %>?id=<% $profile.id %>"><% $i.name %></a></li>
<% /foreach %>
  </ul>
</div>
</div>

<div style="margin-top: 25px">

<% if $can_edit %>
<div class="editlink">
<% if $is_editing %>
<span id="savelink">Please wait...</span> |
<a href="activities.php?id=<% $profile_id %>">Cancel</a>
<% else %>
<a href="activities.php?id=<% $profile_id %>&edit=1">Edit</a>
<% /if %>
</div>
<% /if %>

<h2><% $profile.first_name %> <% $profile.last_name %></h2>

<% if $is_editing %>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('hobbies', {unique: true, plugins: {autocomplete: {}}});
<% foreach from = $tags.hobby item=i key=ttype %>
t1.add('<% $i.tag|capitalize %>');
<% /foreach %>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_hobbies.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();

			});

</script>
<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple',
		editor_selector: 'mceEditor'
	});
</script>
<% /if %>

<div style="float:left" style="width: 150px">

<% if $display_photo %>

<img name="profile_img" src="<% $profile.current_photo %>" width="150" alt="Photo of <% $profile.first_name %> <% $profile.last_name %>"
<% if $profile.school_photo %>
<%* onmouseover="this.src='<% $profile.school_photo %>'" onmouseout="this.src='<% $profile.current_photo %>'" *%>
<% /if %>
/>

<% else %>

<img src="/images/male.png" width="150" alt="Photo missing" />

<% /if %>

</div>

<% if ($is_editing) %>

<form name="do_edit" action="activities.php?id=<% $profile_id %>&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">

		<fieldset>
		<legend>Philanthropy</legend>

<textarea id="philanthropy" name="philanthropy" style="width: 100%" class="mceEditor"><% $profile.philanthropy %></textarea>

</fieldset>

		<fieldset>
		<legend>Entrepreneurship</legend>

<textarea id="entrepreneurship" name="entrepreneurship" style="width: 100%" class="mceEditor"><% $profile.entrepreneurship %></textarea>

</fieldset>

		<fieldset>
		<legend>Hobbies</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="hobbies" value="" id="hobbies" />

</div>
</div>

</fieldset>

</div>

</form>

<% else %>

<div class="network_data">

<% if $profile.philanthropy|strip_tags %>
<div id="philanthropy" class="network_block_top">
<div style="float:left" class="data_title">
Philanthropy
</div>
<div style="float:left" class="data_data">
<% $profile.philanthropy %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $profile.entrepreneurship %>
<div id="entrepreneurship" class="network_block">
<div style="float:left" class="data_title">
Entrepreneurship
</div>
<div style="float:left" class="data_data">
<% $profile.entrepreneurship %>
</div>
</div>
<BR><BR>
<% /if %>

<% if $tags.hobby %>
<div id="hobbies" class="network_block">
<div style="float:left" class="data_title">
Hobbies
</div>
<div style="float:left" class="data_data">
<% foreach from = $tags.hobby item=i key=ttype %>
<span class="button blue small"><a href="../search.php?qs=1&ttype=hobby&tag=<% $i.tag %>"><% $i.tag|capitalize %></a></span>
<% /foreach %>
</div>
</div>
<BR><BR>
<% /if %>


</div>

<% /if %>

<div style="clear:both">&nbsp;</div>

<% /if %>

</div>
