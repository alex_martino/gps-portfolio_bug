<div><img src="/content_files/headers/network.gif" width="800" height="90"></div>
<div>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>

		<div id="map" style="width: 760px; height: 400px;"></div>
  <style>
	.map_box { float: left; margin: 0 5px 0 0; }
	.map_container a { text-decoration: none }
  </style>
  <script type="text/javascript">
    var locations = [
	<% foreach from=$members_array item=i name=addresses %>
		['<div class="map_container"><div class="map_box"><img src="/content_files/member_photos/<% $i.id %>-cropped.jpg"></div><b><% $i.first_name %> <% $i.last_name %></b><BR><% $i.undergraduate_school_s %> <% $i.graduation_year %><div style="clear: both; text-align: center; padding: 12px 0 0 0"><a href="mailto: <% $i.email_address|escape:'quotes' %>">Email</a><% if $i.contact_number %> | <a href="tel:+<% $i.contact_number_country_code %><% $i.contact_number %>">Call</a><% /if %> | <a href="/portal/network/view/contact.php?id=<% $i.id %>">Address</a></div></div>', <% $i.address_lat %>, <% $i.address_long %>]
		<% if not $smarty.foreach.addresses.last %>, <% /if %>
	<% /foreach %>
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: new google.maps.LatLng(40.43, -74.0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
	function share_address() {
		window.location = "map.php?share_location=1";
	}
	function check_address() {
		return true;
	}
  </script>
	<div style="float: right; margin-top: 5px">
		<i>Plotted: <% $members_array|@count %> awesome people!</i>
	</div>
  <BR>
  <b>Your Address</b><BR>
  <form name="address_form" action="map.php" method="POST" onSubmit="check_address();">
  <% if $user_details.address == '' %>
	<div style="float: right; width: 240px; border: 1px dotted red; padding: 10px">
		<b style="color: red">Small problem...</b><BR>You haven't entered your address.<BR><BR>
	</div>
  <% elseif $address_not_found %>
	<div style="float: right; width: 240px; border: 1px dotted red; padding: 10px">
		<b style="color: red">Google couldn't find you!</b><BR>Your address was unreadable by Google!
	</div>
  <% /if %>
  <% if $user_details.address_private %>
  <%*
	<div style="float: right; width: 240px; border: 1px dotted red; padding: 10px">
		<b style="color: red">Small problem...</b><BR>Your address is hidden from other GPSers.<BR><BR>
		<span class="button default strong"><input type="button" value="Share your address!" onClick="share_address();"></span>
	</div>
  *%>
  <% /if %>
  <div style="float: left">
	<textarea name="address" style="width: 200px; height: 70px; margin: 5px 0; resize: none"><% $user_details.address %></textarea><BR>
	<span class="button default strong"><input type="submit" value="Save address"></span>
  </div>
  <div style="clear:both">&nbsp;</div>
  </form>

</div>