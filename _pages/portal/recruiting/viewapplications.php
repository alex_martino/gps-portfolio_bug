<div><img src="/content_files/headers/recruiting_<% $sname %>.gif" width="800" height="90"></div>
<div>
<div class="filter_menu">
View:
<a <% if $display == "all" %> class="this_filter" <% /if %> href="viewapplications.php?display=all&cycle=<% $cycle %>">All</a> |
<a <% if $display == "P" %> class="this_filter" <% /if %> href="viewapplications.php?display=P&cycle=<% $cycle %>">Pending</a> |
<a <% if $display == "R" %> class="this_filter" <% /if %> href="viewapplications.php?display=R&cycle=<% $cycle %>">Rejected</a>
<% foreach from=$top_list item=i key=key %>
 | <a href="viewapplications.php?display=<% $i.count %>&cycle=<% $cycle %>" class="<% $i.class %>">Round <% $i.count %></a>
<% /foreach %>
 | <a <% if $display == "S" %> class="this_filter" <% /if %>
href="viewapplications.php?display=S&cycle=<% $cycle %>">Successful</a>
</div>

<% if $result_num > 0 %>
<div style="float:left" id="datalist">
<table style="border-spacing:0" class="applications">
<tr>
<th style="width: 180px">Name</th>

<% if ($f_email) %><th style="width: 90px">Email</th><% /if %>
<% if ($f_number) %><th style="width: 90px">Number</th><% /if %>
<% if ($f_date) %><th style="width: 90px">Date</th><% /if %>
<% if ($f_status) %><th style="width: 50px">Status</th><% /if %>
<% if ($f_actions) %><th style="width: 200px">Actions</th><% /if %>
</tr>

<% foreach from=$rows item=i key=key %>
<tr class="<% $i.class %>">
<td><% $i.fname %> <% $i.sname %></td>
<% if $i.email %> <td><% $i.email %></td><% /if %>
<% if $i.number %> <td><% $i.number %></td><% /if %>
<% if $i.apptime %> <td><% $i.apptime %></td><% /if %>
<% if $i.status %> <td><% $i.status %></td><% /if %>
<% if $i.actions %>
<td><input type="button" value="View" onClick="javascript:window.location.href='viewapplicant.php?id=<% $i.id %>';">
<input type="button" value="Print" onClick="javascript:CallPrint('<% $i.id %>');">
<% if $can_respond_to_these %>
<input type="button" value="Respond" onClick="javascript:window.location.href='respond.php?id=<% $i.id %>'">
<% /if %>
</td>
<% /if %>
</tr>
<% /foreach %>

</table>
</div>
<% /if %>
<br style="clear:both">
</div>