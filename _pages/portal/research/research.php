<div><img src="/content_files/headers/research.gif" width="800" height="90"></div>
<div>

<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple',
		editor_selector: 'mceEditor'
	});
</script>
<% if $research_item && !$smarty.get.edit && $item_details.deleted != 1 %>
<div class="editlink">
<a href="research.php?sector=<% $smarty.get.sector %>&item=<% $smarty.get.item %>&edit=1">Edit</a>
<% if $can_delete %>
| <a href="research.php?sector=<% $smarty.get.sector %>&item=<% $smarty.get.item %>&delete=1" onClick="return confirm('Are you sure you want to delete this company?');">Delete</a>
<% /if %>
</div>
<% /if %>

<% if $research_item && !$smarty.get.edit && $item_details.deleted == 1 && $can_restore == 1 %>
<div class="editlink">
<a href="research.php?sector=<% $smarty.get.sector %>&item=<% $smarty.get.item %>&restore=1">Restore</a>
</div>
<% /if %>

<% if $research_item && $smarty.get.edit && $item_details.deleted != 1 %>
<div class="editlink">
<a href="#" onClick="document.edit_item.submit(); return false;">Save</a> | <a href="research.php?sector=<% $smarty.get.sector %>&item=<% $smarty.get.item %>">Cancel</a>
</div>
<% /if %>

<h2><% $research_cat %></h2>

<% if $sectorid && !$research_item %><p><% $research_description %></p>

<BR><h2>Current Pipeline</h2>

<div style="width:100%; text-align: center; margin: 10px 0">
<span class="button default strong"><input type="button" value="Add Company" class="submit" onClick="showform(); document.addform.type.value = 'pipeline'; update_explain(); document.addform.sector.value = '<% $sectorid %>'; document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_pipeline').innerHTML; document.getElementById('userlabel').innerHTML = 'Person Researching'; return false;" /></span>
</div>

<% if $display_pipeline %>

<center>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Person Researching</th>
</tr>

<% foreach from=$pipeline item=i %>
<tr class="row" onClick="window.location.href='research.php?sector=<% $smarty.get.sector %>&item=<% $i.id %>';">
<td><% $i.entry_date|date_format:"%m/%d/%y" %></td>
<td><% $i.company_name %>
<td><% $i.ticker %>
<td><% $i.sname %>
<td><% $i.first_name %> <% $i.last_name %></td>
</tr>
<% /foreach %>

</table>
</center>

<% else %>
<p>There is no research currently in the pipeline in this sector.</p>

<% /if %>

<% /if %>

<% if $research_item < 1 && $display_wishlist == 1 %>

<% if $sectorid %><BR><h2>PM's Wishlist</h2><% /if %>

<div style="width:100%; text-align: center; margin: 10px 0">
<span class="button default strong"><input type="button" value="Add Company" class="submit" onClick="showform(); document.addform.type.value = 'wishlist'; update_explain(); document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML; document.getElementById('userlabel').innerHTML = 'User'; document.addform.sector.value = '<% $sectorid %>'; return false;" /></span>
</div>

<center>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Suggested By</th>
</tr>

<% foreach from=$wishlist item=i %>
<tr class="row" onClick="window.location.href='research.php?sector=<% $smarty.get.sector %>&item=<% $i.id %>';">
<td><% $i.entry_date|date_format:"%m/%d/%y" %></td>
<td><% $i.company_name %>
<td><% $i.ticker %>
<td><% $i.sname %>
<td><% $i.first_name %> <% $i.last_name %></td>
</tr>
<% /foreach %>

</table>
</center>
</div>

<% else %>

<% /if %>

<% if $research_item && $item_details.deleted != 1 && $smarty.get.edit != 1 %>
<div style="width: 570px; float: left">
<b>Description</b><BR>
<% $item_details.description %>
<BR><BR>
<b>Why Interesting?</b><BR>
<% $item_details.why_interesting %>
<BR><BR>
<b>Other Information</b><BR>
<% $item_details.other_information %>
</div>
<div style="width: 171px; float: left; border: 1px dotted black; margin-left: 10px; padding: 2px">
<div style="text-align: center; margin-bottom: 3px"><b>Financial Data</b></div>
<div style="width: 90px; float: left; margin-left: 5px">
Entry Price:<BR>
Current Price:<BR>
Price Change:
</div>
<div style="width: 76px; float: left">
$<% $item_details.entry_price %><BR>
$<% $item_details.current_price %><BR>
<% $item_details.price_change %>%<BR>
</div>
</div>
<% /if %>

<% if $research_item && $item_details.deleted != 1 && $smarty.get.edit == 1 %>
<form name="edit_item" action="research.php?sector=<% $smarty.get.sector %>&item=<% $smarty.get.item %>&edit_form=1" method="POST">

<div class="network_data" style="margin-left: 0">

		<fieldset>
		<legend>Description</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="description" name="description" style="width: 705px" class="mceEditor">
<% $item_details.description %>
</textarea>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Why is it interesting?</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="why_interesting" name="why_interesting" style="width: 705px" class="mceEditor">
<% $item_details.why_interesting %>
</textarea>
</div>

<BR><BR>

</fieldset>


		<fieldset>
		<legend>Other Information</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="other_information" name="other_information" style="width: 705px" class="mceEditor">
<% $item_details.other_information %>
</textarea>
</div>

<BR><BR>

</fieldset>


		<fieldset>
		<legend>Ticker</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<input type="text" name="ticker" value="<% $item_details.ticker %>" style="width: 80px" maxlength=8>
</div>

<BR><BR>

</fieldset>

</form>
</div>
<% /if %>

<% if $research_item && $item_details.deleted == 1 %>
<p>This item has been deleted. If you would like it restored please contact an admin.</p>
<% /if %>

<div style="clear:both">&nbsp;</div>
    <div id="page_screen">

        &nbsp;
    </div>

<style>
.data_title_r { width: 130px }
</style>
<div id="addform" style="display:none; border-radius:15px; -moz-border-radius: 15px; height: 640px">
<% if $error %>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><% $errormsg %></div>
<% /if %>
<form name="addform" action="research.php?wishlist=<% $smarty.get.wishlist %>&sector=<% $smarty.get.sector %>&addcompany=1" method="POST">
<div class="network_block_edit">
<div class="dataform">
<div class="data_title_r">
Company Name<BR>
Sector<BR>
Ticker
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="company_name" class="edittextw" value="<% $smarty.post.company_name %>" MAXLENGTH=255><BR>
<select name="sector" style="margin-bottom: 3px">
<option value="0">** Please Select **</option>
<% foreach from=$sectors item=i %>
<option value="<% $i.id %>"><% $i.fname %></option>
<% /foreach %>
</select>
<BR>
<input type="text" name="ticker" value="<% $smarty.post.ticker %>" style="width: 60px; margin-bottom: 10px" MAXLENGTH=8>
<span style="font-size:11px">I.e. This should match the ticker used by Google Finance</span>
</div>
<BR>
<hr class="editline">
<div class="data_title_r">
Description
</div>
<div style="float:left" class="data_data_edit">
<textarea style="width: 420px" name="description" class="mceEditor"><% $smarty.post.description %>
</textarea>
</div>
<div class="data_title_r">
Why is it interesting?
</div>
<div style="float:left" class="data_data_edit">
<textarea style="width: 420px" name="why_interesting" class="mceEditor"><% $smarty.post.why_interesting %>
</textarea>
</div>
<div class="data_title_r">
Other Information
</div>
<div style="float:left; margin-bottom: 10px" class="data_data_edit">
<textarea style="width: 420px" name="other_information" class="mceEditor"><% $smarty.post.other_information %>
</textarea>
</div>
<hr class="editline">
<div class="data_title_r">
Type<BR>
<div style="padding-top: 3px" id="userlabel">Person Researching</div>
</div>
<div style="float:left" class="data_data_edit">
<select name="type" onChange="update_explain();" style="margin-bottom: 3px">
<option value="pipeline">Pipeline</option>
<option value="wishlist">Wishlist</option>
</select>
<span id="type_explain" style="font-size:11px">I.e. This is a suggestion for future research</span>
<BR>
<div id="userdiv">
<select name="user" style="margin-bottom: 5px">
<option value="0">** Please Select **</option>
<% foreach from=$users item=i %>
<option value="<% $i.id %>"><% $i.name %></option>
<% /foreach %>
</select>
</div>
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>
</div>

<div id="userdiv_wishlist" style="display:none">
<input type="text" name="user" value="<% $smarty.session.user_first_name %> <% $smarty.session.user_last_name %>" readonly style="border:0; background: #ededed; margin-top: 2px">
</div>

<div id="userdiv_pipeline" style="display:none">
<select name="user" style="margin-bottom: 5px" id="userdrop2">
<option value="0">** Please Select **</option>
<% foreach from=$users item=i %>
<option value="<% $i.id %>"<% if $i.id == $smarty.session.user_id %> SELECTED<% /if %>><% $i.name %></option>
<% /foreach %>
</select>
</div>

<script>
document.addform.user.value = "<% $smarty.session.user_id %>";
document.getElementById('userdrop2').value = "<% $smarty.session.user_id %>";
<% if !$sectorid %>
document.addform.type.value = "wishlist";
document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML;
<% /if %>
<% if $smarty.post.sector %>
document.addform.sector.value = '<% $smarty.post.sector %>';
<% /if %>
<% if $smarty.post.user %>
document.addform.user.value = "<% $smarty.post.user %>";
document.getElementById('userdrop2').value = "<% $smarty.post.user %>";
<% /if %>
<% if $smarty.post.type == 'wishlist' %>
document.addform.type.value = "wishlist"; document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML; document.getElementById('userlabel').innerHTML = "User";
<% /if %>
<% if $error %>
showform();
<% /if %>
update_explain();
</script>

</div>
