<div><img src="/content_files/headers/research.gif" width="800" height="90"></div>
<div>
<p>Here you can find the current research pipeline. Use the menu on the left to browse by sector.</p>
<BR>
<h2>Current Research: Most recent companies</h2>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Person Researching</th>
</tr>
<% foreach from=$recently_added item=i %>
<tr class="row" onClick="window.location.href='research.php?sector=<% $i.sector_id %>&item=<% $i.id %>';">
<td><% $i.entry_date|date_format:"%m/%d/%y" %></td>
<td><% $i.company_name %>
<td><% $i.ticker %>
<td><% $i.sname %>
<td><% $i.first_name %> <% $i.last_name %></td>
</tr>
<% /foreach %>
</table>
</div>