<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings: Admin List</h2>

<% if $add_success == 1 %>

<b>Congratulations! Admin successfully added.</b><BR><BR>

<% /if %>

<% if $error > 0 %>
<div style="color:navy">
<% $errormsg %>
</div>
<BR>
<% /if %>

<script>
			window.addEvent('load', function(){

				// Autocomplete with poll the server as you type
				var t1 = new TextboxList('add_admin', {unique: true, max: 1, plugins: {autocomplete: {
					minLength: 1,
					queryRemote: true,
					remote: {url: '/includes/autocomplete_members.php?admin=1'}
				}}});
				//t1.add('John Doe').add('Jane Roe');


			});
</script>

<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>First Name</th>
<th>Last Name</th>
<th><img src="/content_files/images/bin.gif" style="width:14px; height: 14px"></th>
</tr>
<% foreach from=$admin_list item=i key=id %>
<tr>
<td><% $i.id %></td>
<td><% $i.first_name %></td>
<td><% $i.last_name %></td>
<% if ($i.id != '103') %>
<td><a href="admin_list.php?del=<% $i.id %>"><img src="/content_files/images/delete.png" style="width: 12px; height: 12px; margin-top: 2px" onClick="javascript:return confirm('Are you sure you wish to remove this user\'s administrative priviledges?');"></a></td>
<% else %>
<td></td>
<% /if %>
</tr>
<% /foreach %>
</table>
<BR>
<h2>Add Admin</h2>
<form name="add_admin" action="admin_list.php" method="POST">
<div class="form_friends">
<div style="float:left"><input type="text" name="add_admin" value="" id="add_admin" /></div>
<div style="float:left; margin: 0 0 0 5px; padding-top: 2px"><span class="button default strong"><input type="submit" value="Add Admin"></span></div>
</div>
</form>

<div style="clear:both">&nbsp;</div>

</div>