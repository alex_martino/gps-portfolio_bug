<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<% if $edit_success == 1 %>

Congratulations! User successfully edited.<BR><BR>

<a href="user_edit.php">Edit another</a>

<% else %>

<h2>Edit Users</h2>

<b>Note:</b> You should use the "<a href="loginas.php">Login As</a>" feature to edit user's profiles - making edits below can damage the integrity of the database.<BR><BR>


<iframe src="/includes/user_edit.php" style="width: 750px; height: 600px; border: 0"></iframe>




<% /if %>

</div>