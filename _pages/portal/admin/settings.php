<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings</h2>

<style>
ol.steps {
	margin: 20px 0;
	background: navy;
	padding: 0 0 0 35px; /*--Distance between the order numbers--*/
	border: 1px solid #111;
}
ol.steps li {
	margin: 0;
	padding: 15px 15px;
	color: #ffffff;
	font-size: 1.7em;
	font-weight: bold;
       /*--The bevel look is styled with various colors in the border properties below--*/
	border-top: 1px solid #000;
	border-bottom: 1px solid #353535;
	border-right: 1px solid #333;
	border-left: 1px solid #151515;
	background: #dedede;
}
ol.steps li.first { border-top: 1px solid #353535; }
ol.steps li.last { border-bottom: none; }
ol.steps li h2 {
	font-size: 0.9em;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px dashed #333;
	color: navy;
}
ol.steps li p {
	color: #000000;
	font-size: 0.7em;
	font-weight: normal;
	line-height: 1.6em;
}
</style>
<ol class="steps">
	<li>
		<h2><a href="admin_list.php">Admin List</a></h2>
		<p>Add or remove administrator priviledges</p>
	</li>
	<li>
		<h2><a href="industries.php">Industries</a></h2>
		<p>Add or edit employment industries</p>
	</li>
	<li>
		<h2><a href="recruiting_timings.php">Recruiting Timings</a></h2>
		<p>Amend the timings for GPS recruiting</p>
	<li>
		<h2><a href="universities.php">Universities</a></h2>
		<p>Add/edit/delete the GPS Universities</p>
	</li>
</ol>
<script type="text/javascript">
	$(document).ready(function() {
		$("ol.steps li:first").addClass("first");
		$("ol.steps li:last").addClass("last");
	});
</script>
</div>