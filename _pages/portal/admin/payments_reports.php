<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<div>
<script>
function generatereport() {
due_cat = document.generate_report.due_cat.value;
uni = document.generate_report.university.value;
who = document.generate_report.who.value;
theurl = encodeURI("payments_reports.php?download=1&due_cat="+due_cat+"&uni="+uni+"&who="+who);
window.location=theurl;
}
</script>
<h2>Reports</h2>
<% if $error > 0 %>
<div style="color:navy">
<% $errormsg %>
</div>
<BR>
<% /if %>

<div style="float: left">

<form name="generate_report">

<select name="due_cat">
  <% foreach from=$due_cat item=i %>
    <option value="<% $i %>"><% $i %></option>
  <% /foreach %>
</select>

<select name="university">
  <option value="all">All pods</option>
  <% foreach from=$universities item=i %>
    <option value="<% $i.ID %>"><% $i.SNAME %></option>
  <% /foreach %>
</select>

<select name="who">
  <option value="all">All GPSers</option>
  <option value="analyst">Analysts</option>
  <option value="member">Members</option>
  <option value="alumni">Alumni</option>
</select>

</div>

<div style="margin: 1px 0 0 10px; float: left">
  <span class="button default medium strong"><input type="button" value="Generate" onClick="generatereport();"></span>
</div>

</form>

</div>