<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<script>
function invoice_open(id) {
window.open("https://www.gps100.com/includes/payments/invoice_details.php?id="+id,"invoice","width=900, height=600, left="+((screen.width/2)-450)+",top="+((screen.height/2)-330));
}
</script>
<div>
<h2>Invoices</h2>
<% if $error > 0 %>
<div style="color:navy">
<% $errormsg %>
</div>
<BR>
<% /if %>

<table class="admin_table" style="width: 100%; border: 1px solid black">

<tr style="border-bottom: 1px solid black">

<th style="width: 20px">ID</th>
<th>User</th>
<th>Description</th>
<th>Amount</th>
<th style="width: 100px">Created</th>
<th>Updated</th>
<th>Status</th>
</tr>

<% foreach from=$invoices key=k item=i %>

<tr class="row" onClick="invoice_open('<% $i.id %>');">
<td><% $i.id %></td>
<td><% $i.name|truncate:30:"...":true %></td>
<td><% if ($i.annual_dues_flag) %>Annual dues: <% /if %><% $i.description %></td>
<td><% $i.amount %></td>
<td><% $i.created|date_format:"%d/%m/%y" %></td>
<td><% $i.updated|date_format:"%d/%m/%y" %></td>
<td><b><% $i.status|capitalize %></b></td>
</tr>


<% /foreach %>

</table>

</div>