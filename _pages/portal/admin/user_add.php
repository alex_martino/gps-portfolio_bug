<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<% if $add_success == 1 %>

Congratulations! New User Added.<BR><BR>

<a href="user_add.php">Add another</a>

<% else %>

<p>Fill out this form to add a user:</p>
<% if $error > 0 %>
<div style="color:navy">
<% $errormsg %>
</div>
<BR>
<% /if %>
<form name="add_user" method="POST" action="user_add.php">
<div style="float:left; width: 85px; line-height: 1.6">
First Name:<BR>
Last Name:<BR>
Email:<BR>
Gender:<BR>
Year:<BR>
Status:<BR>
University:<BR><BR>
<input type="submit" value="Make User">
</div>
<div style="float:left; width: 500px; line-height: 1.6">
<input type="text" name="first_name" id="first_name" value="<% $fname %>"><BR>
<input type="text" name="last_name" id="last_name" value="<% $sname %>"><BR>
<input type="text" name="email" id="email" value="<% $email %>" style="width: 180px"> (I.e. tshannon@gps100.com)<BR>
<select name="gender">
<option value="1">Male</option>
<option value="0">Female</option>
</select><BR>
<select name="year">
<% foreach from=$years item=i %>
<option value="<% $i %>" <% if ($smarty.post.year == $i) %> SELECTED<% /if %>><% $i %></option>
<% /foreach %>
</select><BR>
<select name="status" id="status">
<option value="analyst" <% if $status == "analyst" %>SELECTED<% /if %>>Analyst</option>
<option value="member" <% if $status == "member" %>SELECTED<% /if %>>Member</option>
<option value="alumni" <% if $status == "alumni" %>SELECTED<% /if %>>Alumni</option>
</select><BR>
<select name="university" id="university">
<% foreach from=$rowdetails item=i key=key %>
<option value="<% $i.uni_id %>" <% if $i.selected == 1 %>SELECTED<% /if %>><% $i.uni_sname %></option>
<% /foreach %>
</select>
</div>
<input type="hidden" name="submitted" value="1">
</form>

<% /if %>

</div>