<div><img src="/content_files/headers/portfolio.gif" width="800" height="90">

</div>
<div>
<script>
  $(document).ready(function() {
  
portfolio = "<% $portfolio_json %>";

portfolio_json = JSON.parse(portfolio);

portfolio_json_clean = portfolio_json['stock'];

var tbl_body = "";

  var j = new Array();

  for (var i = 0; i < portfolio_json_clean.length; i++) {
    j[i] = portfolio_json_clean[i];
  }

  console.log(j)

  $.each(j, function() {
          var tbl_row = "";
          $.each(this, function(k , v) {
              tbl_row += "<td>"+v+"</td>";
          })
          tbl_body += "<tr>"+tbl_row+"</tr>";                 
      })

      $("#target_table_id").html(tbl_body);

  });
</script>
  
  <link href="/css/footable-0.1.css" rel="stylesheet" type="text/css">
    <link href="/css/footable.sortable-0.1.css" rel="stylesheet" type="text/css">

    <script src="/scripts/footable-0.1.js" type="text/javascript"></script>
    <script src="/scripts/footable.sortable.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(function() {
        $('table').footable();
      });
    </script>
    <style>
      .test-class { background-color: #5BFF82; }
    </style>

	<table class="footable">
      <thead>
        <tr>
          <th>Ticker</th>
          <th>Name</th>
          <th data-type="numeric">Shares Held</th>
		  <th data-hide="phone,tablet,desktop">Valuation Date</th>
		  <th data-hide="phone,tablet">Current Share Price</th>
          <th data-hide="phone,tablet">Market Value</th>
		  <th data-hide="">% of Portfolio</th>
        </tr>
      </thead>
      <tbody id="target_table_id">
        
      </tbody>
    </table>


</div>