<div><img src="/content_files/headers/annual_reports.gif" alt="" width="800" height="90" /></div>
<div>Please click the links below to view the Global Platinum Securities annual reports.</div>

<div style="text-align: center; width:550px; margin: 0 auto">

<div class="ar_img">
<a href="/content_files/Annual Report 2013.pdf">
<img src="/content_files/images/AR2013.jpg" alt="" /><br /><br />
2013</a><br /> (3.5MB PDF)
</div>

<div class="ar_img">
<a href="/content_files/Annual Report 2012.pdf">
<img src="/content_files/images/AR2012.jpg" alt="" /><br /><br />
2012</a><br /> (9.6MB PDF)
</div>

<div class="ar_img">
<a href="/content_files/Annual Report 2011.pdf">
<img src="/content_files/images/AR2011.jpg" alt="" /><br /><br />
2011</a><br /> (6MB PDF)
</div>

<div style="clear:both">&nbsp;</div>

<div class="ar_img">
<a href="/content_files/Annual Report 2010.pdf">
<img src="/content_files/images/AR2010.jpg" alt="" /><br /><br />
2010</a><br /> (11MB PDF)
</div>

<div class="ar_img">
<a href="/content_files/Annual Report 2009.pdf">
<img src="/content_files/images/AR2009.jpg" alt="" /><br /><br />
2009</a><br /> (3.6MB PDF)
</div>

<div class="ar_img">
<a href="/content_files/Annual Report 2008.pdf">
<img src="/content_files/images/AR2008.jpg" alt="" /><br /><br />
2008</a><br /> (4.6MB PDF)
</div>

<div style="clear:both">&nbsp;</div>

<style>
.ar_img { float: left; width: 143px; padding: 0 15px }
.ar_img img { width: 133px; height: 173px; border: 0 }
</style>

</div>