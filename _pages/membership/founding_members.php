<div><img src="/content_files/headers/founding_members.gif" width="800" height="90"></div>
<div>
	<h2>Global Platinum Securities Founding Student Members</h2>
	<% foreach from=$founding_student_members item=member %>
		<div style="float: left; height: 62px; margin-bottom: 15px; width: 253px;">
			<% if $member.biography != '' %><a href="/membership/member.php?member_type=2&amp;id=<% $member.id %>"><% /if %><strong><% $member.first_name %> <% $member.last_name %></strong><% if $member.biography != '' %></a><% /if %><br />
			<% if $member.gps_position_held != '' %><strong><% $member.gps_position_held %></strong><br /><% /if %>
			<% if $member.current_employer != '' %><% $member.current_employer %><br /><% /if %>
			<% $member.undergraduate_school %>
		</div>
	<% /foreach %>
	<div style="clear: both;"></div>
	
	<h2>Global Platinum Securities Chapter Founders</h2>
	<% foreach from=$founding_chapter_members item=member %>
		<div style="float: left; height: 38px; margin-bottom: 15px; width: 253px;">
			<% if $member.biography != '' %><a href="/membership/member.php?member_type=2&amp;id=<% $member.id %>"><% /if %><strong><% $member.first_name %> <% $member.last_name %></strong><% if $member.biography != '' %></a><% /if %><br />
			<% if $member.current_employer != '' %><% $member.current_employer %><br /><% /if %>
			<% $member.undergraduate_school %>
		</div>
	<% /foreach %>
	<div style="clear: both;"></div>
</div>