<div><img src="/content_files/headers/members.gif" alt="" width="800" height="90"></div>
<div>

<% if $member.hidden != 1 %>
<% if $can_edit %>
<div class="editlink">
<a href="/portal/network/view/overview.php?id=<% $member.id %>&edit=1">Edit Biography</a>
</div>
<% /if %>
	<% if $member.founding_member eq 1 || $member.chapter_founder eq 1 %>
		<h2>Global Platinum Securities Founding Member</h2>
	<% elseif $member.alumni eq 1 %>
		<h2>GPS Alumni - Class of <% $member.graduation_year %></h2>
	<% elseif $member.member eq 1 %>
		<h2>Global Platinum Securities Member</h2>
	<% elseif $member.advisory_board eq 1 %>
		<h2>Global Platinum Securities Advisory Board Member</h2>
	<% elseif $member.upper_management eq 1 %>
		<h2>Global Platinum Securities Upper Management Member</h2>
	<% elseif $member.board_manager eq 1 %>
		<h2>Global Platinum Securities Board Manager Member</h2>
	<% /if %>
	<% if $member.current_photo != '' && $logged_in == 1 %>
		<div style="float: left; margin-bottom: 5px; margin-right: 10px;"><img src="<% $member.current_photo %>" width="150" alt="" /></div>
	<% /if %>
	<p><strong><span style="font-size: 16px;"><% $member.first_name %> <% $member.last_name %></span><br />
		<% if $member.gps_position_held != '' %><% $member.gps_position_held %><br /><% /if %>
		<% $member.undergraduate_school %></strong></p>
	<% $member.biography|replace:'<p>':'<p style="text-align:justify">' %>
	<%*
	<% if $smarty.session.member_id != '' %>
		<% if $member.student_photo != '' %>
			<div style="float: left; margin-bottom: 5px; margin-right: 10px;"><img src="<% $member.student_photo %>" width="150" alt="" /></div>
		<% /if %>	
		<h2>Personal Information</h2>
        
		<% if $member.email_address != '' %><p><strong>Email Address:</strong> <a href="mailto:<% $member.email_address %>"><% $member.email_address %></a></p><% /if %>
		<% if $member.birth_date != '' %><p><strong>Birth Date:</strong> <% $member.birth_date %></p><% /if %>
		<% if $member.location != '' %><p><strong>Location:</strong> <% $member.location %></p><% /if %>
		<% if $member.hometown != '' %><p><strong>Hometown:</strong> <% $member.hometown %></p><% /if %>
		<% if $member.hobbies != '' %><p><strong>Hobbies:</strong> <% $member.hobbies %></p><% /if %>
		
		<h2>School & Professional Information</h2>
		<% if $member.undergraduate_school != '' %><p><strong>Undergraduate School:</strong> <% $member.undergraduate_school %></p><% /if %>
		<% if $member.undergraduate_major != '' %><p><strong>Undergraduate Major:</strong> <% $member.undergraduate_major %></p><% /if %>
		<% if $member.graduation_year != '' %><p><strong>Graduation Year:</strong> <% $member.graduation_year %></p><% /if %>
		<% if $member.graduate_school != '' %><p><strong>Graduation School:</strong> <% $member.graduate_school %></p><% /if %>
		<% if $member.professional_designations != '' %><p><strong>Professional Designations:</strong> <% $member.professional_designations %></p><% /if %>
		<% if $member.current_employer != '' %><p><strong>Current Employer:</strong> <% $member.current_employer %></p><% /if %>
		<% if $member.previous_employers != '' %><p><strong>Previous Employers:</strong> <% $member.previous_employers %></p><% /if %>
		
		<h2>GPS Information</h2>
		<% if $member.gps_position_held != '' %><p><strong>GPS Position Held:</strong> <% $member.gps_position_held %></p><% /if %>
		<% if $member.favorite_gps_memory != '' %><p><strong>Favorite GPS Memory:</strong> <% $member.favorite_gps_memory %></p><% /if %>
		<% if $member.analyst_stock_pitch != '' %><p><strong>Analyst Stock Pitch:</strong> <% $member.analyst_stock_pitch %></p><% /if %>
		<% if $member.alumni_mentor != '' %><p><strong>Alumni Mentor:</strong> <% $member.alumni_mentor %></p><% /if %>
		<% if $member.member_advocate != '' %><p><strong>Member Advocate:</strong> <% $member.member_advocate  %></p><% /if %>
	<% /if %>
*%>

<% else %>

<h2>Confidential</h2>

<p>This member profile is private.</p>

<% /if %>
</div>
