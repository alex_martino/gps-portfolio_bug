<div><img src="/content_files/headers/membership.gif" width="800" height="90"></div>
<div>
	<div align="center"><img src="/content_files/images/gps_map_new.png" width="511" height="378" alt="" /></div><br />
	<h2>Global Platinum Securities Upper Management</h2>
	<% foreach from=$upper_management item=member %>
		<div style="float: left; height: 38px; margin-bottom: 15px; width: 253px;">
			<% if $member.biography != '' %><a href="/membership/member.php?id=<% $member.id %>"><% /if %><strong><% $member.first_name %> <% $member.last_name %></strong><% if $member.biography != '' %></a><% /if %><br />
			<% if $member.gps_position_held != '' %><strong><% $member.gps_position_held %></strong><br /><% /if %>
			<% $member.undergraduate_school %>
		</div>
	<% /foreach %>
	<div style="clear: both;"></div>

	<h2>Global Platinum Securities Current Members</h2>
	<% foreach from=$current_members item=member %>
		<div style="float: left; height: 38px; margin-bottom: 15px; width: 253px;">
			<% if $member.biography != '' %><a href="/membership/member.php?id=<% $member.id %>"><% /if %><strong><% $member.first_name %> <% $member.last_name %></strong><% if $member.biography != '' %></a><% /if %><br />
			<% $member.undergraduate_school %>
		</div>
	<% /foreach %>
	<div style="clear: both;"></div>
	

</div>
