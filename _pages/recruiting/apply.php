<div><img src="/content_files/headers/recruiting.gif" alt="" width="800" height="90" /></div>
<div>
<h1>Apply Online
<% if $cycles != 'closed' %>
- Stage 1/4</h1>

<% if $error != 0 %>
<BR>
<% $errormsg %>
<% /if %>
<BR />
<form name="app1" action="<% $phpself %>" method="post">
<div style="float: left; line-height:1.5">
First Name:<BR/>
Surname:<BR/>
Email:<BR/>
Contact number:<BR/>
University:<BR/>
<% if isset($deadline_f) %>
Deadline:
<% /if %>
</div>

<div style="float: left; padding-left: 10px; line-height:1.5" class="app1">
<input type="hidden" name="submitapp1" value="1" />
<input type="text" name="fname" value="<% $smarty.request.fname %>" /><BR/>
<input type="text" name="sname" value="<% $smarty.request.sname %>" /><BR/>
<input type="text" name="email" value="<% $smarty.request.email %>" /><BR/>
<input type="text" name="number" value="<% $smarty.request.number %>" /><BR/>
<select name="university" onChange="window.location.href='apply.php?uni='+this.options[this.selectedIndex].value+'&fname='
+document.app1.fname.value+'&sname='+document.app1.sname.value+'&email='+document.app1.email.value+'&number='+
document.app1.number.value">
<option value="0">Please Select</option>
<% html_options output=$options values=$values selected=$uni_s%>
</select>
<% if isset($deadline_f) %>
<BR>
<% $deadline_f %>
(Eastern Time)
<% /if %>

</div>
<br style="clear:left" /><BR/>
<script>
//if(document.app1.university.value != 0) {
document.write('<p class="btxt">');
document.write('<a href="#" onclick="javascript:document.app1.submit();">Continue to next stage</a>');
document.write('</p>');
//}
</script>
<noscript>
Please use a JavaScript enabled browser to submit your application.
</noscript>
</form>

<% else %>
</h1>
We are not currently accepting applications. Please check back later.
<% /if %>

</div>