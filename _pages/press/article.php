<div><img src="/content_files/headers/press.gif" width="800" height="90"></div>
<div>
	<h2><% $press_release.title %></h2>
	<p><strong><% $press_release.date|date_format:"%m-%d-%Y" %></strong></p>
	<% $press_release.body %>
</div>