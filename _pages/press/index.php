<div><img src="/content_files/headers/press.gif" width="800" height="90"></div>
<div>
	<% if $press_releases != false %>
		<% foreach from=$press_releases item=press_release %>
			<p><a href="/press/article.php?id=<% $press_release.id %>"><strong><% $press_release.date|date_format:"%m-%d-%Y" %> - <% $press_release.title %></strong></a><br />
			<% $press_release.body|strip_tags|truncate:300 %></p>
		<% /foreach %>
	<% else %>
		<p>There are no press releases at this time. Please check back soon.</p>
	<% /if %>
</div>