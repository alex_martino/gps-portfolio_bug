<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>
<% $page_title %>
</title>

<link rel="stylesheet" type="text/css" href="/scripts/ext-2.0/resources/css/reset.css">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
<link rel="stylesheet" type="text/css" href="/css/portal.css">
<link rel="stylesheet" type="text/css" href="/css/button.css">

<style>
#header, #article { width: 100% }
#header { position: absolute; top: 0; height: 70px; z-index: 1; background: #ffffff; border-bottom: 4px solid #002868 }
#article_frame { width: 100%; border: 0; margin: 74px 0 0 0; display: none; background: #FFF; }
#recommended_by { position: absolute; top: 0; left: 0; margin: 10px; z-index: 1 }
#recommended_by .photo { float: left }
#recommended_by .text { float: left; margin: 0 0 0 10px; font-size: 10px; line-height: 1.6; color: #000000 }
#recommended_by .text b, #recommended_by .text i  { font-size: 12px; line-height: 1.1; color: #002868 }
#rate_article { float: left; text-align: center; margin: 0 auto; width: 100% }
#rate_div { margin: 20px 0 0 0 }
#login_box { float: none; width: 265px; margin: 15px auto 0 auto; text-align: left }
#login_box input[type="text"] { width: 120px }
#login_box input[type="password"] { width: 70px }
#login_details { position: absolute; top: 0; right: 0; float: right; margin: 10px 10px 0 0 ; z-index: 1 }
#login_details .photo { float: right }
#login_details .text { float: left; margin: 0 10px 0 0; font-size: 10px; line-height: 1.6; color: #000000 }
#login_details .text b, #recommended_by .text i  { font-size: 12px; line-height: 1.1; color: #002868 }
#fallback { position: absolute; left: 0; margin: 10px; z-index: 1 }
#fallback .text { float: left; margin: 0 0 0 10px; font-size: 10px; line-height: 1.6; color: #000000 }
#loading { margin: 100px 0 0 0; text-align: center; font-size: 18px; color: #ffffff }
body { color: #002868 }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function setIframeHeight(iframeName) {
		  //var iframeWin = window.frames[iframeName];
		  var iframeEl = document.getElementById? document.getElementById(iframeName): document.all? document.all[iframeName]: null;
		  if (iframeEl) {
		  iframeEl.style.height = "auto"; // helps resize (for some) if new doc shorter than previous
		  //var docHt = getDocHeight(iframeWin.document);
		  // need to add to height to be sure it will all show
		  var h = alertSize();
		  var new_h = (h-74);
		  iframeEl.style.height = new_h + "px";
		  //alertSize();
		  }
		}

		function alertSize() {
		  var myHeight = 0;
		  if( typeof( window.innerWidth ) == 'number' ) {
		    //Non-IE
		    myHeight = window.innerHeight;
		  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		    //IE 6+ in 'standards compliant mode'
		    myHeight = document.documentElement.clientHeight;
		  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		    //IE 4 compatible
		    myHeight = document.body.clientHeight;
		  }
		  //window.alert( 'Height = ' + myHeight );
		  return myHeight;
		}

function pageloaded() {
setIframeHeight('article_frame');
$("#loader").hide();
$("#article_frame").show();
}

function login() {
    document.getElementById('login_form').submit();
}

function rate_article() {

	rating = document.getElementById("article_rating_value").value;
	
	
		$("#rate_div").html("Please wait...");
		$.get(
			"read.php?rate_article=1&token=<% $smarty.get.token %>&rating="+rating,
			{language: "php", version: 5},
			function(responseText){
					$("#rate_div").css("padding-top","5px");			
				if (responseText == "success") {
					$("#rate_div").html("Thank you for rating the article!");
				}
				else if (responseText == "fail") {
					$("#rate_div").html("There was a problem rating this article. Perhaps you need to login again.");
				}
				else if (responseText == "error_already_rated") {
					$("#rate_div").html("You've already rated this article!");
				}
				else {
					$("#rate_div").html("Error:"+responseText);
				}
			},
			"html"
		);
}

</script>


</head>
<body onload="pageloaded();" onresize="setIframeHeight('article_frame');" style="overflow:hidden;">

<div id="header">

<div id="recommended_by">

 <div class="photo">
  <img src="https://www.gps100.com/content_files/member_photos/<% $article.user_id %>-cropped.jpg" alt="Profile photo">
 </div>

 <div class="text">
  Recommended by<br>
  <b><% $article.first_name %> <% $article.last_name %></b><BR>
  <i><% $article.school_name %> <% $article.graduation_year %></i>
 </div>
 
</div>

<div id="rate_article">

  <% if $logged_in %>
	<div id="rate_div">
	<% if !$article.already_rated %>
    <select id="article_rating_value" onChange="rate_article();">
    <option value="0">** Rate Article **</option>
    <option value="1">Poor</option>
    <option value="3">Average</option>
    <option value="5">Excellent</option>
	</select>
	<% else %>
	<div style="padding-top: 5px">
	You've already rated this article!
	</div>
	<% /if %>
	</div>

  <% else %>

	<div id="login_box">
	  <form id="login_form" action="https://www.gps100.com/members/login.php" method="post">
		<div style="float: left; width: 265px; padding-bottom: 3px">
		  <div style="float:right">
		    <input type="checkbox" name="remember_me" value="1" style="width:10px;vertical-align: middle; margin-bottom: 3px"> Auto
		  </div>
		  <strong>Login to rate &amp; comment</strong>
		</div>
		
		<div style="float: left; clear:left; margin-right: 5px;"><input type="text" id="email_address" name="email_address" value="" placeholder="@gps100.com"></div>
		<div style="float: left; margin-right: 5px;"><input type="password" id="password" name="password" value=""></div>
		<div style="float: left;"><a href="#" onClick="login();"><img id="login_button" src="/content_files/images/buttons/login_out.gif" style="border: 0; width: 57px; height: 17px" alt="Login button"></a></div>
		<input type="hidden" id="action" name="action" value="login">
	  </form>
	</div>

  <% /if %>

</div>

<% if $logged_in %>
<div id="login_details">
 
 <div class="photo">
  <img src="https://www.gps100.com/content_files/member_photos/<% $smarty.session.user_id %>-cropped.jpg" alt="Profile photo">
 </div>

 <div class="text">
  Logged in as<br>
  <b><% $smarty.session.user_first_name|substr:0:1 %> <% $smarty.session.user_last_name %></b><BR>
  <i><a href="/members/login.php?action=logout">Logout</a></i>
 </div>
</div>
<% /if %>

</div>

<div id="article_holder">
<iframe id="article_frame" src="<% $article.url %>"></iframe>
</div>

<div id="loading">
  <div style="margin: 0 0 5px 0; font-size: 14px; font-weight: bold">Loading article...</div>
  <div>
   <img src="https://www.gps100.com/images/loading2.gif" alt="Loading">
  </div>
</div>

</body>
</html>
