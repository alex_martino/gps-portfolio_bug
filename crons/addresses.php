<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//first check where we are
$sql = "SELECT value FROM settings WHERE setting = 'address_lookup_marker' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_row($result);
$marker = $row[0];

//now count number of addresses
$sql = "SELECT id, first_name, last_name, email_address, address FROM members WHERE disabled = 0 AND address <> ''";
$result = mysql_query($sql) or die(mysql_error());
$address_count = mysql_num_rows($result);

if ($marker > $address_count) { $marker=0; }

$marker_stop = $marker+20;

//load up users with an address
$sql = "SELECT id, first_name, last_name, email_address, address FROM members WHERE disabled = 0 AND address <> '' LIMIT ".$marker.",".$marker_stop;
$result = mysql_query($sql) or die(mysql_error());
$do_count = mysql_num_rows($result);

while ($row=mysql_fetch_array($result)) { //start loop

	$url = "http://maps.google.com/maps/geo?q=".urlencode($row['address'])."&output=csv&key=AIzaSyDuG7ufZ1rLsxu_KstE-A21mhsDgEEimsY";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER,0); //Change this to a 1 to return headers
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
//	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
	$data = curl_exec($ch);
	
	//Check our Response code to ensure success
	if (substr($data,0,3) == "200"){
		$data = explode(",",$data);
			$precision = $data[1];
			$latitude = $data[2];
			$longitude = $data[3];
			if (is_numeric($latitude) && is_numeric($longitude)) {
				$sql = "UPDATE members SET address_lat_long = POINT(".$latitude.", ".$longitude.") WHERE id = '".$row['id']."' LIMIT 1";
				$result2 = mysql_query($sql) or die(mysql_error());
			}
	}
	else {
		//do nothing
	}

} //end loop
curl_close($ch);

if ($do_count %20 == 0) { //check if divisible by 20
	$new_marker = $marker_stop;
}
else {
	$new_marker = 0;
}
$sql = "UPDATE settings SET value =  '".$new_marker."' WHERE setting = 'address_lookup_marker'";
$result = mysql_query($sql);
?>