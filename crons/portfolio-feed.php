<?php
//This runs once per hour - to check for Bivio portfolio changes and update our database

//include DB connection
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//set parameters
$url = "https://www.bivio.com/gps100/accounting/investments";
$loginurl = "https://www.bivio.com/pub/login";
$username = "bwigoder@gps100.com";
$password = "bivio4M3";
$cookiefile = tempnam("/tmp", "cookies"); 

session_start();

		//get cookie set
		$ch = curl_init ();
		curl_setopt($ch, CURLOPT_URL, $loginurl);
		curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent=Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
		curl_setopt ($ch, CURLOPT_REFERER, $loginurl); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);  
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
		$data = curl_exec($ch);
		$errors = curl_error($ch);
		$details = curl_getinfo($ch); 
		
		// Displaying debugging info
		//var_dump($data);
		//var_dump($errors);
		//var_dump($details);

		
		//now cookie is set, log in
		curl_setopt($ch, CURLOPT_URL, $loginurl);
		curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent=Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
		curl_setopt ($ch, CURLOPT_REFERER, $url); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);  
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);  
		$postfields = 'tz=-180&v=2&c=aNg__&f2='.urlencode($username).'&f0='.urlencode($password).'&f3=Secure+Login';
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$body = curl_exec($ch);
		$errors = curl_error($ch);
		$details = curl_getinfo($ch); 

		//now fetch the data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent=Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
		curl_setopt ($ch, CURLOPT_REFERER, $loginurl); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);  
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
		$data = curl_exec($ch);
		$errors = curl_error($ch);
		$details = curl_getinfo($ch); 
		
		//Remove irrelevant data
		$startpos = strpos($data,'<table class="s_investments s_table">');
		$data = substr($data,$startpos);
		$endpos = strpos($data,'</table>');
		$data = substr($data,0,$endpos);
		
		//include parser
		require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/simple_html_dom.php");

		$table = array();
		$html = str_get_html($data);
		foreach($html->find('tr') as $row) {
			$name = $row->find('td',0);
			$name = strip_tags($name);
				//split name into name and ticker
				preg_match('#\((.*?)\)#', $name, $match);
				if (count($match)>0) {
					$ticker = $match[1];
					$name = trim(substr($name,0,strpos($name,"(")));
					$shares_held = $row->find('td',1);
					$shares_held = trim(strip_tags($shares_held));
				$valuation_date = $row->find('td',2);
				$valuation_date = trim(strip_tags($valuation_date));
					//lets fix the format
					$timestamp = strtotime($valuation_date);
					$valuation_date = date("Y-m-d",$timestamp);
				$price_per_share = $row->find('td',3);
				$price_per_share = trim(strip_tags($price_per_share));
				$market_value = $row->find('td',4);
				$market_value = trim(strip_tags($market_value));
					//remove commas
					$shares_held = str_replace(',','',$shares_held);
					$price_per_share = str_replace(',','',$price_per_share);
					$market_value = str_replace(',','',$market_value);
					$percent_of_portfolio = str_replace(',','',$percent_of_portfolio);
				$percent_of_portfolio = $row->find('td',5);
				$percent_of_portfolio = strip_tags($percent_of_portfolio);
					//remove the percent sign + whitespace
					$percent_of_portfolio = substr_replace($percent_of_portfolio,"",-1);
					$percent_of_portfolio = str_replace("&nbsp;","",$percent_of_portfolio);
				}
			if(strlen($name)>0) {
				$table[] = array($ticker,$name,$shares_held,$valuation_date,$price_per_share,$market_value,$percent_of_portfolio);
			}
		}
		
		print_r($table);
		
		//Only update the file & DB if we get the stock data (if less than 5 rows, assume an error)
		if (count($table) < 5) { die("error fetching data: table too small"); }

		//empty portfolio table
		mysql_query("truncate TABLE portfolio");
		
		//let's put it to the DB
		foreach($table as $row) {
			$query = 'insert into portfolio values ("'.$row[0].'","'.$row[1].'","'.$row[2].'","'.$row[3].'","'.$row[4].'","'.$row[5].'","'.$row[6].'")';
			$result = mysql_query($query) or die(mysql_error());
		}
		
		//now let's output it as a format we can use
		
		//xml
			$config['table_name'] = "stock";
			$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			$root_element = "portfolio"; //portfolio
			$xml .= "<$root_element>";
			
			//select all items in table
			$sql = "SELECT * FROM ".$root_element;
 
			if (!$result = mysql_query($sql))
			die("Query failed.");

			if(mysql_num_rows($result)>0)
			{
			while($result_array = mysql_fetch_assoc($result))
			{
				$xml .= "<".$config['table_name'].">";

				//loop through each key,value pair in row
				foreach($result_array as $key => $value)
				{
					//$key holds the table column name
					$xml .= "<$key>";
			
					//embed the SQL data in a CDATA element to avoid XML entity issues
					$xml .= $value; 
 
					//and close the element
					$xml .= "</$key>";
				}
 
				$xml.="</".$config['table_name'].">";
			}
			}

			//close the root element
			$xml .= "</$root_element>";
			
			//write the file
			$myFile = $_SERVER['DOCUMENT_ROOT'] . "/api/files/portfolio.xml";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $xml);
			fclose($fh);
		
?>