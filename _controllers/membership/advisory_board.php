<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('members');
$members = $db_connection->select_array("advisory_board = 1", 'last_name');

// Assign variables
$smarty->assign('members', (is_array($members) ? $members : false));
?>