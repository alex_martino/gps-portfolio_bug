<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('members');
$board_of_managers = $db_connection->select_array("board_manager = 1", 'weight DESC, last_name');

// Assign variables
$smarty->assign('board_of_managers', (is_array($board_of_managers) ? $board_of_managers : false));
?>