<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('members');
$upper_management = $db_connection->select_array("upper_management = 1 AND hidden = 0 AND disabled = 0", 'weight DESC, last_name');
$current_members = $db_connection->select_array("member = 1 AND upper_management = 0 AND hidden = 0 AND disabled = 0", 'weight DESC, last_name');


// Assign variables
$smarty->assign('upper_management', (is_array($upper_management) ? $upper_management : false));
$smarty->assign('current_members', (is_array($current_members) ? $current_members : false));
?>