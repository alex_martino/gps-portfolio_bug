<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('members');
$founding_members = $db_connection->select_array("founding_member = 1", 'weight DESC, last_name');
$chapter_founders = $db_connection->select_array("chapter_founder = 1", 'weight DESC, last_name');

// Assign variables
$smarty->assign('founding_student_members', (is_array($founding_members) ? $founding_members : false));
$smarty->assign('founding_chapter_members', (is_array($chapter_founders) ? $chapter_founders : false));
?>