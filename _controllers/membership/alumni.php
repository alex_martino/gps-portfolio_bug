<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('members');
$results = $db_connection->select_array("alumni = 1 AND hidden = 0 AND disabled = 0", 'graduation_year DESC, last_name');

foreach ($results as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}

// Assign variables
$smarty->assign('years', $years);
?>