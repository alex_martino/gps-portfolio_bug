<?php
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

//valid ID
$fetch_id = $_REQUEST['id'];
if (!is_numeric($fetch_id)) { die ("Fatal error"); }
$fetch_id = mysql_real_escape_string($fetch_id);

//check if your profile
$can_edit = 0;
if ($logged_in) {
	if ($_SESSION['user_id'] == $fetch_id) { $can_edit = 1; }
}
$smarty->assign('can_edit', $can_edit);

// Retrieve member info
$db_connection = new db_connection('members');
$results = $db_connection->select_array("id = '" . $fetch_id . "'");

// Assign a page title for this page
$title = SITE_NAME . ($url ? " :: " : "") . ucwords(str_replace("-", " / ", str_replace("_", " ", str_replace("/", " :: ", str_replace(".php", "", str_replace("/index.php", "", $url)))))) . ' :: ' . $results[0]['first_name'] . ' ' . $results[0]['last_name'];

// Assign variables
$smarty->assign('member', $results[0]);
?>