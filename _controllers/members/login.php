<?
//first move to https
if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

//check if force logout because account was disabled
if ($_GET['disabled_in_session']) {
unset ($_SESSION['member_id']);
$smarty->assign('error_message', 'Your account has been disabled. If you believe this is in error please contact an administrator.');
}

//check if logged in
if ($_GET['action'] != "logout") {
if (isset($_SESSION['member_id'])) { header("Location: /portal/"); }
}

// Include main config file
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include database class
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Check for referring page
if (isset($_GET['referrer'])) { $referrer = $_GET['referrer']; }

// Check for ID
$refer_id = $_GET['id'];

if (is_numeric($refer_id)) {
$referrer = $referrer."&id=".$refer_id;
}

$db_connection = new db_connection('members');

switch ($_REQUEST['action']) {
	default:
		break;
	case "login" :
		if ($_POST['email_address'] && $_POST['password']) {

			// First do some sanitation
			$_POST['email_address'] = str_replace('"', '', $_POST['email_address']);
			$_POST['email_address'] = str_replace("'", "", $_POST['email_address']);
			$_POST['password'] = str_replace('"', '', $_POST['password']);
			$_POST['password'] = str_replace("'", "", $_POST['password']);

			// Check to make sure the username and password are valid

				$results = $db_connection->select_array("email_address = '" . mysql_real_escape_string($_POST['email_address']) . "' AND pass_enc_sec = sha1('" . sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8')");

				if (is_array($results) && $results[0]['disabled'] == 1) {
				$smarty->assign('error_message', 'Login Failure: Account disabled.');
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login_fail_disabled','".$results[0]['id']."','".date("Y-m-d H:i:s")."','".$referrer."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());
				}

				elseif (is_array($results) && $results[0]['password'] == "update_required") {
				$smarty->assign('update_required', 'true');

				function rand_string( $length ) {
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	

				$size = strlen( $chars );
					for( $i = 0; $i < $length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}

					return $str;
				}

				$c_code = rand_string(16);
				$c_time = date("Y-m-d H-i-s");
				$c_email = $_POST['email_address'];

				mysql_query("INSERT INTO pass_reset (gps_email,code,time) VALUES ('$c_email','$c_code','$c_time') ON DUPLICATE KEY UPDATE code = '$c_code'") or die(mysql_error());

				//now email code
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = $c_email;
				$subject = "Action Required: GPS Password Reset";
				$body = "Dear GPSer,\n\nPlease click the following link to reset your password: http://www.".$domain."/members/passreset.php?code=".$c_code."&email=".$c_email;
				$body .= "\n\nMany thanks,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $emailsuccess=1;
				 }
				
				
				}
			

			// Now login the user
			elseif (is_array($results)) {
				// Setup session variables
				if (!session_is_registered('member_id')) {
					session_register('member_id');
				}
				$_SESSION['member_id'] = $results[0]['id'];
				$_SESSION['user_id'] = $results[0]['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $results[0]['first_name'];
				$_SESSION['user_last_name'] = $results[0]['last_name'];
				$_SESSION['user_ug_school_id'] = $results[0]['ug_school_id'];
				$_SESSION['user_is_analyst'] = $results[0]['analyst'];
				$_SESSION['user_is_member'] = $results[0]['member'];
				$_SESSION['user_is_um'] = $results[0]['upper_management'];
				$_SESSION['user_is_board'] = $results[0]['board_manager'];
				$_SESSION['user_is_alumni'] = $results[0]['alumni'];
				$_SESSION['user_is_admin'] = $results[0]['admin'];
				$_SESSION['user_gps_position_held'] = $results[0]['gps_position_held'];

				// Documents System Variables
 				if($results[0]['analyst']){ $_SESSION['id_group'] = 1; }
				if($results[0]['member']){ $_SESSION['id_group'] = 2; }
				if($results[0]['alumni']){ $_SESSION['id_group'] = 3; }
				if($results[0]['board_manager']){ $_SESSION['id_subgroup'] = 1; }
				if($results[0]['upper_management']){ $_SESSION['id_subgroup'] = 2; }
				if($results[0]['board_manager'] && $results[0]['upper_management']) { $_SESSION['id_subgroup'] = '1,2'; }
				$_SESSION['id_user'] = $results[0]['id'];
				$_SESSION['id_admin'] = $results[0]['admin'];
				$_SESSION['email'] = $results[0]['email_address'];
				$_SESSION['username'] = $results[0]['email_address'];
				$_SESSION['userfolder']='';

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$referrer."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());


				// Check for remember me, if checked set remember me cookie
				if ($_POST['remember_me']) {
				$cookie_path = "/";
				$cookie_timeout = 60 * 60 * 24 * 30; 
				$user_nohash = $_POST['email_address'];
				$pass_hash = sha1(sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8");
				setcookie ('rememberme', 'r_email_address='.$user_nohash.'&r_hash='.$pass_hash, time() + $cookie_timeout, "/", '.gps100.com');
				}
				
				if (session_is_registered('member_id')) {
					if (strlen($referrer)>0) {
						header('Location:'.$referrer);
					}
					elseif (stristr($_SERVER['HTTP_REFERER'], '/members/login.php')) {
						header('Location: /portal/');
					} else {
						header('Location: ' . $_SERVER['HTTP_REFERER']);
					}
					exit();
				} else {
					$smarty->assign('error_message', 'Login Failure: Session not registered.');
				}
			} else {
				$smarty->assign('error_message', 'Login Failure: Username/Password not correct.');
				// check if attempt to log into admin account, if yes email the super-admin
				$sql = "SELECT * FROM members WHERE email_address='".$_POST['email_address']."' AND admin = 1 LIMIT 1";
				$result = mysql_query($sql);
				$row = mysql_fetch_array($result);
				if (mysql_num_rows($result) > 0) {

				//now email super admin
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@".$domain.">";
				$to = "bwigoder@gps100.com";
				$subject = "GPS Portal Notification: Admin login failure";
				$body = "Dear Ben,\n\nA user failed to log into an admin account.\n\n";
				$body .= "Attempted email: ".$_POST['email_address']."\nAttempted Password: ";
				if ($setting_under_attack == 1) {
				$body .= $_POST['password'];
				}
				else {
				$body .= "[obscured]";
				}
				$body .= "\n\nDate: ".date('Y-m-d')."\nTime: ".date('H:i:s')."\nIP Address: ".$_SERVER['REMOTE_ADDR'];
				$body .= "\n\nMany thanks,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@".$domain;
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $emailsuccess=1;
				 }

				}

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login_fail','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".mysql_real_escape_string($_POST['email_address'])."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());
			}
		} else {
			$smarty->assign('error_message', 'Login Failure: You must enter a username and password.');
		}
		break;
	case "logout" :
		// Logout the user
		unset($_SESSION['member_id']);
		unset($_SESSION['admin_override']);
		
		// Document Storage variables
		unset($_SESSION['id_group']);
		unset($_SESSION['id_subgroup']);
		unset($_SESSION['id_user']);
		unset($_SESSION['email']);
		unset($_SESSION['username']);
		unset($_SESSION['userfolder']);

		// delete remember me cookie
		setcookie ('rememberme', "", time() - 3600, '/', '.gps100.com');

		$smarty->assign('error_message', 'You have been logged out.');
		break;
}
		$smarty->assign('session_expired', $_GET['session_expired']);
		$smarty->assign('referrer', $referrer);
?>