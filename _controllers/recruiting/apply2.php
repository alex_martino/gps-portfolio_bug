<?php
session_start();
$error = 0;

//Initial checks
  if (!isset($_SESSION['fname']) || !isset($_COOKIE['fname'])) { header('Location: sessionexpired.php'); }
  if (!is_numeric($_SESSION['uni']) || !is_numeric($_COOKIE['uni'])) { header('Location: sessionexpired.php'); }

$uni_id = $_SESSION['uni'];

require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
$sql = "SELECT * FROM cycles WHERE UNI_ID = '$uni_id' ORDER BY DEADLINE DESC LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$coverlimit = $row['COVERLETTER'];
$_SESSION['coverlimit'] = $coverlimit;

if (!is_numeric($coverlimit)) { header("Location: apply.php"); }

//retrieve stored cover letter
$coverletter = $_SESSION['coverletter'];
$coverletter = str_replace("<br>","",$coverletter);

if (isset($_POST['coverletter'])) {

$coverletter = stripslashes($_POST['coverletter']);

function count_words($str)
{
$words = 0;
$str = eregi_replace(" +", " ", $str);
$array = explode(" ", $str);
for($i=0;$i < count($array);$i++)
{
if (eregi("[0-9A-Za-z�-��-��-�]", $array[$i]))
$words++;
}
return $words;
}


if (empty($coverletter)) {
$error++;
$errormsg = "Please enter your cover letter before continuing.";
}

if ($coverlimit > 0) {
if (count_words($coverletter) > $coverlimit) {
$error++;
$errormsg = "Word limit exceeded.";
}
}


//begin processing data
if ($error == 0) {
$_SESSION['coverletter'] = $coverletter;
header('Location: apply3.php');
}

}

//assign error data
$smarty->assign('error',$error);
$smarty->assign('errormsg',$errormsg);
$smarty->assign('cycles',$cycles);

$smarty->assign('coverletter',$coverletter);
$smarty->assign('coverlimit',$coverlimit);

?>