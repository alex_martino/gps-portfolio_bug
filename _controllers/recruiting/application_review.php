<?php
session_start();
$error = 0;

//Gather information
  //Check the first name is set
  if (!isset($_SESSION['fname']) && !isset($_COOKIE['fname'])) { header('Location: sessionexpired.php'); }
  if (!is_numeric($_SESSION['uni']) && !is_numeric($_COOKIE['uni'])) { header('Location: sessionexpired.php'); }
  //Gather variables from session
    $fname = $_SESSION['fname'];
    $sname = $_SESSION['sname'];
    $email = $_SESSION['email'];
    $number = $_SESSION['number'];
    $uni = $_SESSION['uni'];
    $cycle = $_SESSION['cycle'];
  //if firstname variable is empty set all variables to cookie values
  if (empty($_SESSION['fname'])) {
    $fname = $_COOKIE['fname'];
    $sname = $_COOKIE['sname'];
    $email = $_COOKIE['email'];
    $number = $_COOKIE['number'];
    $uni = $_SESSION['uni'];
    $cycle = $_SESSION['cycle'];
  }
  //Find university name
    require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
    $sql = "SELECT FNAME FROM universities WHERE ID = ".$uni." LIMIT 1";
    $result=mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_array($result);
    $uni_fname = $row['FNAME'];
  //Retrieve CV location
    if (!isset($_SESSION['filestamp']) || !isset($_SESSION['extension'])) { header("Location: apply4.php"); }
    $filestamp = $_SESSION['filestamp'];
    $extension = $_SESSION['extension'];
  //Retrieve Cover Letter
    if ($_SESSION['coverlimit'] > 0) {
    $coverletter = $_SESSION['coverletter'];
    $coverletter = str_replace("\n","<br>",$coverletter);
    }

  //Fetch backup info
   require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
   $sql = "SELECT EMAIL FROM cycles WHERE ID = '$cycle' LIMIT 1";
   $result=mysql_query($sql) or die(mysql_error());
   $row = mysql_fetch_array($result);
   $backup_email = $row['EMAIL'];

//Complete application
if ($_GET['submit'] == 1) {

//Now run full security checks, insert into DB and store CV
  //Check all variables are correct
$valid_fname = preg_match('/^[a-zA-Z\-]+$/', $fname);
if ($valid_fname == 0) { $error++; $errormsg .= "Invalid characters entered in first name.<BR>"; }
$valid_sname = preg_match('/^[a-zA-Z\-\s]+$/', $sname);
if ($valid_sname == 0) { $error++; $errormsg .= "Invalid characters entered in surname.<BR>"; }
include("email_validate.php");
if (!check_email_address($email)) { $error++; $errormsg .= "Invalid email entered.<BR>"; }
$valid_number = preg_match('/^[0-9\s]+$/', $number);
if ($valid_number == 0) { $error++; $errormsg .= "Please only enter numbers in the phone number field.<BR>"; }
if (!is_numeric($uni)) { die("Fatal error."); }
if (!is_numeric($cycle)) { die("Fatal error."); }
//Check cycle is still open for submissions
$sql = "SELECT DEADLINE FROM cycles WHERE ID = '$cycle' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
function convert_datetime($str) {
list($date, $time) = explode(' ', $str);
list($year, $month, $day) = explode('-', $date);
list($hour, $minute, $second) = explode(':', $time);
$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
return $timestamp;
}
$deadline = convert_datetime($row['DEADLINE']);
$current_time = time();
if ($current_time > $deadline) { $error++; $errormsg .= "Deadline has passed."; }

if ($_SESSION['coverlimit'] > 0) { $coverletter = stripslashes($coverletter); }
if (!is_numeric($filestamp)) { die("Fatal error."); }
//File extension check
if (strtolower($extension) != 'doc' && strtolower($extension) != 'docx' && strtolower($extension) != 'pdf') {
$error++; $errormsg = "CV is an invalid document type. Must be a MS Word file or PDF.";
}


if ($error < 1) {
  //Insert into DB
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
$sql = "INSERT INTO applications (FNAME, SNAME, EMAIL, NUMBER, UNI_ID, CYCLE, COVERLETTER, FILESTAMP, EXTENSION)
VALUES ('$fname','$sname','$email','$number','$uni','$cycle','$coverletter','$filestamp','$extension')";
mysql_query($sql) or die(mysql_error());

  //Store CV
rename("./recruiting/cvs/temp/".$filestamp.".".$extension, "./recruiting/cvs/".$cycle."/".$fname."_".$sname."_".$filestamp.".".$extension);

  //Delete temp cv row in DB
mysql_query("DELETE FROM tempcvs WHERE filestamp='$filestamp'");

  //Email user

//Load up template
$letter_id = 1; //Email confirmation ID
$sql = "SELECT CONTENT FROM letters WHERE ID = '$letter_id'";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$content = $row['CONTENT'];
$content = str_replace('[applicant]',$fname,$content);

//Find Uni Short Name
$sql = "SELECT SNAME FROM universities WHERE ID = '$uni'";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$uni_sname = $row['SNAME'];
$content = str_replace('[uni_sname]',$uni_sname,$content);

require_once "Mail.php";

$from = "GPS Recruiting <recruiting@".$domain.">";
$to = $email;
$subject = "GPS Application Confirmation";
$body = $content;

$host = "localhost";
$username = "recruiting@".$domain;
$password = "101dalmations";

$headers = array ('From' => $from,
  'To' => $to,
  'Subject' => $subject);
$smtp = Mail::factory('smtp',
  array ('host' => $host,
    'auth' => true,
    'username' => $username,
    'password' => $password));

$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
  $error++; $errormsg = "Email Error";
 } else {
  $emailsuccess=1;

//no error - send to backup address

if (strlen($backup_email) > 0) { //check for backup email
$fileatt = "./recruiting/cvs/".$cycle."/"; // Path to the file
$fileatt .= $fname."_".$sname."_".$filestamp.".".$extension; // Filename that will be used for the file as the attachment

require_once('Mail\mime.php');
    $subject_detailed = $fname." ".$sname."'s Application";
    $coverletter_e = str_replace("<br>","\n",$coverletter);

    $message = new Mail_mime();
    $text = "First Name: ".$fname."\nSurname: ".$sname."\nEmail: ".$email."\nNumber: ".$number."\n\nCover Letter: ".$coverletter_e;
    $html = "";

    $message->setTXTBody($text);
    $message->setHTMLBody($html);
    $message->addAttachment($fileatt); 
    $body = $message->get();
    $extraheaders = array("From"=>$from, "Subject"=>$subject_detailed);
    $headers = $message->headers($extraheaders);

$smtp = Mail::factory('smtp',
  array ('host' => $host,
    'auth' => true,
    'username' => $username,
    'password' => $password));

$mail = $smtp->send($backup_email, $headers, $body);

} //end check for backup email


 } //end email success

if ($error < 1) { //check for errors

  //add action
//add action
$f_datetime = date('Y-m-d H:i:s');
$f_type = "app_submit";
$f_name = substr($fname,0,1)." ".$sname;
$sql = "INSERT INTO actions (time, uni_id, action, detail) VALUES ('$f_datetime', '$uni', '$f_type', '$f_name')";
$result = mysql_query($sql) or die(mysql_error());

  //Clear all stored data
session_destroy();
$expiry = time()-60*60*6;
setcookie('fname', '', $expiry);
setcookie('sname', '', $expiry);
setcookie('email', '', $expiry);
setcookie('number', '', $expiry);
setcookie('cycle', '', $expiry);
setcookie('uni', '', $expiry);

  //Redirect to thank you page
header('Location: applicationcomplete.php');

} //end check for errors

}

else { //If error takes place
header('Location: sessionexpired.php');
}

}

$smarty->assign('error',$error);
$smarty->assign('errormsg',$errormsg);
$smarty->assign('cycles',$cycles);

$smarty->assign('coverlimit',$_SESSION['coverlimit']);

$smarty->assign('fname',$fname);
$smarty->assign('sname',$sname);
$smarty->assign('email',$email);
$smarty->assign('number',$number);
$smarty->assign('uni_fname',$uni_fname);
$smarty->assign('coverletter',$coverletter);

$smarty->assign('filestamp',$filestamp);
$smarty->assign('extension',$extension)


?>