<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Retrieve news
$db_connection = new db_connection('_sa_news_admin');
$press_releases = $db_connection->select_array(false, 'date DESC, weight');

// Assign variables
$smarty->assign('press_releases', (is_array($press_releases) ? $press_releases : false));
?>