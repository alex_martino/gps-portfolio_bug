<?php
$i = 0;
function convert_datetime($str) {
list($date, $time) = explode(' ', $str);
list($year, $month, $day) = explode('-', $date);
list($hour, $minute, $second) = explode(':', $time);
$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
return $timestamp;
}
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$display = $_GET['display'];
$cycle = $_GET['cycle'];

if (!is_numeric($cycle)) { die(); }

if (empty($display)) { $display = 'all'; }

//get uni id
$sql = "SELECT * FROM cycles WHERE ID = '$cycle' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$UNI_ID = $row['UNI_ID'];
$rounds_num = $row['ROUNDS'];

//get uni short name
$sql = "SELECT * FROM universities WHERE ID = '$UNI_ID' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$sname = $row['SNAME'];

//Check for entries
$sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' AND STATUS = '$display' LIMIT 1";
if ($display == 'all') { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' LIMIT 1"; }
$result = mysql_query($sql) or die(mysql_error());
$result_num = mysql_num_rows($result);

//Filters
$f_status = 1;
$f_actions = 1;

$f_email = $_GET['f_email'];
$f_number = $_GET['f_number'];
$f_date = $_GET['f_date'];
if (isset($_GET['f_status'])) { $f_status = $_GET['f_status']; }
if (isset($_GET['f_actions'])) { $f_actions = $_GET['f_actions']; }

//For export
if ($f_email) { $exp_filters .= "&email=1"; }
if ($f_number) { $exp_filters .= "&number=1"; }
//if ($f_date) { $exp_filters .= "&date=1"; }
if ($f_status) { $exp_filters .= "&status=1"; }

$exp_filters .= "&cycle=".$cycle;

$r = $rounds_num;
$i = 1;
while ($i <= $r) {
$top_list[$i]['count'] = $i;
if ($display == $i) { $top_list[$i]['class'] = "this_filter"; }
$i++;
}

if ($display == 'P') { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' AND STATUS = 'P'"; }
elseif ($display == 'R') { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' AND STATUS = 'R'"; }
elseif ($display == 'S') { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' AND STATUS = 'S'"; }
elseif (is_numeric($display)) { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle' AND STATUS = '$display'"; }
else { $sql = "SELECT * FROM applications WHERE CYCLE = '$cycle'"; }
$result=mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array($result)) {
$i++;
if ($i&1) { $rows[$i]['class'] = "data"; }
else { $rows[$i]['class'] = "data2"; }
$rows[$i]['fname'] = $row['FNAME'];
$rows[$i]['sname'] = $row['SNAME'];
if ($f_email) { $rows[$i]['email'] = $row['EMAIL']; }
if ($f_number) { $rows[$i]['number'] = $row['NUMBER']; }
if ($f_date) {
$apptime = date("d M Y ",$row['FILESTAMP']);
 $rows[$i]['apptime'] = $apptime;
 }
if ($f_status) {  $rows[$i]['status'] = $row['STATUS']; }
if ($f_actions) { $rows[$i]['actions'] = 1; }

}

if ($can_app_respond_all || ($can_app_respond && $_SESSION['user_ug_school_id'] == $UNI_ID)) { $can_respond_to_these = 1; }

$smarty->assign('display', $display);
$smarty->assign('sname', $sname);
$smarty->assign('cycle', $cycle);
$smarty->assign('top_list', $top_list);
$smarty->assign('result_num', $result_num);

$smarty->assign('f_email', $f_email);
$smarty->assign('f_number', $f_number);
$smarty->assign('f_date', $f_date);
$smarty->assign('f_status', $f_status);
$smarty->assign('f_actions', $f_actions);

$smarty->assign('rows', $rows);

$smarty->assign('can_respond_to_these', $can_respond_to_these);
?>