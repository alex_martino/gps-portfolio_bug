<?php

$now = date("Y-m-d H:i:s");
$ip = $_SERVER['REMOTE_ADDR'];

//process resume delete
if ($_GET['delete'] && is_numeric($_GET['id'])) {

$delid = $_GET['id'];

if ($delid == $_SESSION['user_id']) { //check user is deleting their own resume

$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/portal/resumes/".$delid.".pdf";

if(file_exists($targetPath)) {
unlink($targetPath);

$sql = "UPDATE members SET resume_uploaded = '0', resume_date='', resume_problem = '0' WHERE id = '$delid'";
$result = mysql_query($sql);
$sql = "INSERT INTO logs (type, uid, time, ip_address) VALUES ('resume_delete','$delid','$now','$ip')";
$result = mysql_query($sql);

} //end check file exists

} //end check user deleting own resume

} //end resume delete


//process resume download
elseif ($_GET['download'] && is_numeric($_GET['id'])) {

if (($_GET['id'] == $_SESSION['user_id']) || $_SESSION['user_is_admin']) {

if( headers_sent() ) { die('Headers Sent'); }

$fullPath = $_SERVER['DOCUMENT_ROOT'] . "/portal/resumes/" . $_GET['id'] . ".pdf";

  // Required for some browsers
  if(ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off'); 

if( file_exists($fullPath) ){ 

//get extra data
$uid = $_GET['id'];
$sql = "SELECT first_name, last_name, resume_date, resume_problem FROM members WHERE id='$uid' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_array($result)) {
$data = $row;
}

$name = $data['first_name'] . " " . $data['last_name'] . " (" . date("M j, Y",strtotime($data['resume_date'])). ").pdf";

// Parse Info / Get Extension
    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);
   
    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: application/pdf");
    header("Content-Disposition: attachment; filename=\"".$name."\";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$fsize);
    ob_clean();
    flush();
    readfile( $fullPath );
    $sql = "INSERT INTO logs (type, uid, data, time, ip_address) VALUES ('resume_download','".$_SESSION['user_id']."','$uid','$now','$ip')";
    $result = mysql_query($sql);
    die();

  } else
    die('File Not Found'); 

} //end check downloading own resume

} //end resume download

$sql = "SELECT resume_uploaded, resume_date, resume_problem FROM members WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_array($result)) {
$user_has_resume = $row['resume_uploaded'];
$resume_upload_date = $row['resume_date'];
$resume_has_problem = $row['resume_problem'];
}

define('PDF_MAGIC', "\x25\x50\x44\x46\x2D");
function is_pdf($filename) {
    return (file_get_contents($filename, false, null, 0, strlen(PDF_MAGIC)) === PDF_MAGIC) ? true : false;
}

if ($user_has_resume && !$resume_has_problem) {
if (!is_pdf($_SERVER['DOCUMENT_ROOT']."/portal/resumes/".$_SESSION['user_id'].".pdf")) {
$resume_has_problem = 1;
$userid = $_SESSION['user_id'];
$sql = "UPDATE members SET resume_problem = '1' WHERE id = '$userid'";
$result = mysql_query($sql);
}
}

if(!$resume_has_problem && $user_has_resume) {

// check resume is readable
// just require TCPDF instead of FPDF
//require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/tcpdf.php');
//currently use fpdp - below

require_once($_SERVER['DOCUMENT_ROOT'].'/includes/fpdf/fpdf.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/fpdi/individual_fpdi.php');

class concat_pdf extends FPDI {

    var $files = array();

    function concat() {
            $pagecount = $this->setSourceFile($_SERVER['DOCUMENT_ROOT']."/portal/resumes/".$_SESSION['user_id'].".pdf");
            for ($i = 1; $i <= $pagecount; $i++) {
                 $tplidx = $this->ImportPage($i);
                 $s = $this->getTemplatesize($tplidx);
                 $this->AddPage('P', array($s['w'], $s['h']));
                 $this->useTemplate($tplidx);
            }

    }

}

$pdf =& new concat_pdf();
$pdf->concat();

}

$smarty->assign('user_has_resume', $user_has_resume);
$smarty->assign('resume_upload_date', $resume_upload_date);
$smarty->assign('resume_has_problem', $resume_has_problem);

?>