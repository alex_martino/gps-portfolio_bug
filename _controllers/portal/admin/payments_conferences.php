<?php
//Add a conference
if ($_GET['add_conference'] == 1) {
	//pick up the variables
	$price['analyst'] = $_POST['analyst_price'];
	$price['member'] = $_POST['member_price'];
	$price['alumni'] = $_POST['alumni_price'];
	$attendees = $_POST['conference_user'];
	$conference = $_POST['conference'];
	
	//Fix the array (just get the user ids)
	$attendees_arr = explode("_",$attendees);
	$attendees_arr = array_filter($attendees_arr, "is_numeric");
	
	//And make a string again
	$attendees_str = implode(",",$attendees_arr);
	
	//Grab the data and associate fees with correct attendees
	$sql = "SELECT id, analyst, member, alumni FROM members WHERE id IN(".$attendees_str.")";
	$result = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_array($result)) {
		if ($row['analyst'] == 1) {
			$pay[] = array('uid'=>$row['id'],'price'=>$price['analyst']);
		}
		if ($row['member'] == 1) {
			$pay[] = array('uid'=>$row['id'],'price'=>$price['member']);
		}
		if ($row['alumni'] == 1) {
			$pay[] = array('uid'=>$row['id'],'price'=>$price['alumni']);
		}		
	}
	
	//now prepare to insert the payments (invoices)
	$sql = "INSERT INTO payments (user_id, description, amount, due_date, status, created, updated) VALUES";
	$description = $conference;
	$now = date("Y-m-d H:i:s");
	$due_date = date("Y-m-d", strtotime('+1 week'));
	$year = date("Y");
	foreach ($pay as $key=>$value) {
		$sql .= " ('".$value['uid']."','".$description."','".$value['price']."','".$due_date."','unpaid','$now','$now'),";
	}
	
	//tidy sql
	$sql = rtrim($sql, ',');
	$sql = $sql . ";";
	
	//now load the invoices
	$result = mysql_query($sql) or die(mysql_error());
	
	//We need to add email notification here!
	
	//now redirect
	header("Location: payments_conferences.php");
}

//Get the conferences
unset($conference);
$sql = 'SELECT DISTINCT(description) FROM payments WHERE annual_dues_flag != 1 AND description != "Test"';
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
	$conference[] = $row[0];
}

foreach ($conference as $value) {
	//get unpaid
	$sql = 'SELECT p.status, sum(p.amount) as total FROM payments p JOIN members m on m.id = p.user_id WHERE p.description = "'.$value.'" AND p.status = "unpaid"';
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$unpaid = $row[1];
	if (count($unpaid) < 1) { $unpaid = "0"; }
	
	//get paid
	$sql = 'SELECT p.status, sum(p.amount) as total FROM payments p JOIN members m on m.id = p.user_id WHERE p.description = "'.$value.'" AND p.status = "paid"';
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$paid = $row[1];
	if (count($paid) < 1) { $paid = "0"; }
	
	//get overdue
	$sql = 'SELECT p.status, sum(p.amount) as total FROM payments p JOIN members m on m.id = p.user_id WHERE p.description = "'.$value.'" AND p.status = "overdue"';
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$overdue = $row[1];
	if (count($overdue) < 1) { $overdue = "0"; }
	
	//get first invoice
	$sql2 = 'SELECT created FROM payments WHERE description = "'.$value.'" ORDER BY created ASC LIMIT 1';
	$result2 = mysql_query($sql2);
	if (mysql_num_rows($result2) > 0) {
		$row2 = mysql_fetch_row($result2);
		$first_invoice = date("Y-m-d",strtotime($row2[0]));
	}
	
	//get last payment
	$sql3 = 'SELECT updated FROM payments WHERE description = "'.$value.'" AND status = "paid" ORDER BY updated DESC LIMIT 1';
	$result3 = mysql_query($sql3);
	if (mysql_num_rows($result3) > 0) {
		$row3 = mysql_fetch_row($result3);
		$last_payment = date("Y-m-d",strtotime($row3[0]));
	}
	else {
		$last_payment = "N/A";
	}
	
	$conference_list[] = array('name'=>$value,'paid'=>$paid,'unpaid'=>$unpaid,'overdue'=>$overdue,'first_invoice'=>$first_invoice,'last_payment'=>$last_payment);
	
}
	
if (mysql_num_rows($result) < 1) {
	$no_conferences = 1;
}

//Create a list of possible conferences to add
$start_year = date("Y")-1; //last year
$end_year = date("Y"); //this year

//Generate a list of all the years so far
for ($x=$start_year; $x<=$end_year; $x++) {
	$years[] = $x;
}

foreach ($years as $value) {
	$addconf_list[] = $value . " (Spring)";
	$addconf_list[] = $value . " (Summer)";
	$addconf_list[] = $value . " (Fall)";
}

//Reverse order
rsort($addconf_list);

//now remove any conferences that have already been billed
foreach ($addconf_list as $key => $value) {
	foreach ($conference_list as $value2) {
		if ($value2['name'] == $value) {
			unset ($addconf_list[$key]);
		}
	}
}

$smarty -> assign('addconf_list', $addconf_list);
$smarty -> assign('conference', $conference);
$smarty -> assign('conference_list', $conference_list);
$smarty -> assign('no_conferences', $no_conferences);
?>