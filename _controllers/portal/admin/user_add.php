<?php
function createRandomPassword() {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

if ($_POST['submitted']) { //form submitted
$error = 0;
$fname = ucwords($_POST['first_name']);
$sname = ucwords($_POST['last_name']);
$email = $_POST['email'];
$year = $_POST['year'];
$status = $_POST['status'];
$gender = $_POST['gender'];
$university = $_POST['university'];

$valid_fname = preg_match('/^[a-zA-Z\-]+$/', $fname);

if ($valid_fname == 0) { $error++; $errormsg .= "Invalid characters entered in first name.<BR>"; }
$valid_sname = preg_match('/^[a-zA-Z\-\s]+$/', $sname);

if ($valid_sname == 0) { $error++; $errormsg .= "Invalid characters entered in surname.<BR>"; }

require_once("email_validate.php");
if (!check_email_address($email)) { $error++; $errormsg .= "Invalid email entered.<BR>"; } else {
$pos = strpos($email, "@gps100.com");

  if ($pos === false) { $error++; $errormsg .= "Email must be @gps100.com"; }

  else {
	
	include($_SERVER['DOCUMENT_ROOT']."/includes/xmlapi.php");

	$ip = $_SERVER['SERVER_ADDR'];
	//echo $ip;

	//get credentials
	include($_SERVER['DOCUMENT_ROOT']."/config_admin.php");

	$user = $cpanel_username;
	$password = $cpanel_password;

	$xmlapi = new xmlapi($ip);
	$xmlapi->set_port(2083);
	$xmlapi->password_auth($user, $password);
	$xmlapi->set_debug(0);
	$xmlapi->set_output("array");
	
	print $xmlapi->api2_query($account, "Email", "listpopswithdisk" );

	$result = $xmlapi->api2_query($user, "Email", "listforwards");
	$cp_res_emailfwds = $result['data'];

	function array_searchRecursive( $needle, $haystack, $strict=false, $path=array() )
	{
	    if( !is_array($haystack) ) {
	        return false;
	    }
 
	    foreach( $haystack as $key => $val ) {
	        if( is_array($val) && $subPath = array_searchRecursive($needle, $val, $strict, $path) ) {
	            $path = array_merge($path, array($key), $subPath);
	            return $path;
	        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
	            $path[] = $key;
	            return $path;
	        }
	    }
	    return false;
	}

	//check forward does not already exist
	if (!array_searchRecursive($email, $cp_res_emailfwds)) {
		$error = 1; $errormsg .= "Email forward not found. Please create an email forward before adding the user.";
	}

	//check user does not already exist
	else {

		$sql = "SELECT email_address FROM members WHERE email_address = '".$email."' LIMIT 1";
		$result = mysql_query($sql) or mysql_error();
		$num = mysql_num_rows($result);
		if ($num > 0) {
			$error = 1; $errormsg .= "User already exists with this email address.";
		}

	}

  }


}


if ($error < 1) {
$sql = "SELECT email_address FROM members WHERE email_address = '".$email."'";
$result=mysql_query($sql) or die(mysql_error());
$num_rows = mysql_num_rows($result);
if ($num_rows > 0) { $error++; $errormsg .= "Email address already in use"; }
}
$valid_number = preg_match('/^[0-9\s]+$/', $number);
if (!is_numeric($university.$year)) { die("Fatal error."); }
if ($status == "analyst") { $is_analyst = 1; }
elseif ($status == "member") { $is_member = 1; }
elseif ($status == "alumni") { $is_alumni = 1; }
else { die("Fatal error."); }

// check length of fields
if (strlen($fname) > 150) { $error++; $errormsg .= "Maximum length of a first name is 150 characters.<BR>"; }
if (strlen($sname) > 150) { $error++; $errormsg .= "Maximum length of a last name is 150 characters.<BR>"; }
if (strlen($email) > 255) { $error++; $errormsg .= "Maximum length of an email is 255 characters.<BR>"; }

$sql = "SELECT id FROM members WHERE first_name = '$fname' AND last_name = '$sname' LIMIT 1";
$result=mysql_query($sql) or die(mysql_error());
$num_rows = mysql_num_rows($result);
if ($num_rows > 0) { $error++; $errormsg .= "An account already exists for ".$fname." ".$sname."."; }


if ($error < 1) { //add user
$password = createRandomPassword();
$enc_pass = sha1(sha1($password)."3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8");

//find university name
    $sql = "SELECT FNAME FROM universities WHERE ID = ".$university." LIMIT 1";
    $result=mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_array($result);
    $uni_fname = $row['FNAME'];

$sql = "INSERT INTO members (first_name, last_name, email_address, password, pass_enc_sec, undergraduate_school, ug_school_id, analyst, member, alumni, graduation_year, gender_male)
VALUES ('$fname', '$sname', '$email', 'update_required', '$enc_pass', '$uni_fname', '$university', '$is_analyst', '$is_member', '$is_alumni', '$year', '$gender')";
$result = mysql_query($sql) or die(mysql_error());

// Log user add permanently
$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('admin_user_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$fname . " " . $sname ."','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql) or die("There was a problem logging the admin add. Please contact an admin.");


				//now email new user
				require_once "Mail.php";
				
				$from = "GPS Admin <admin@gps100.com>";
				$to = $email;
				$subject = "Welcome to the GPS Portal";
				$body = "Dear ".$fname.",\n\nI am excited to introduce you to the GPS Portal, something new to you, as well as GPS members and alumni. We have been developing the Portal over the past few years as a means for our members to connect with the worldwide GPS community.";
				$body .= "\n\nThe GPS portal allows you to easily search amongst analysts, members and alumni so you can reach out to those who share similar interests, skills or hobbies. While the GPS Portal is still in its infancy, you may use it for networking, GM and sector chats, as well as voting after buy and sell presentations.";
				$body .= "\n\nI encourage you to fill out your online profile as completely as possible - and be sure to add a picture!";
				$body .= "\n\nTo login visit http://www.gps100.com";
				$body .= "\n\nUsername: ".$email."\nPassword: ".$password;
				$body .= "\n\nWhen you log in for the first time you will be prompted to change your password.";
				$body .= "\n\nBest wishes,\nBenjamin Wigoder";

				$host = "localhost";
				$username = "admin@gps100.com";
				$password = "101dalmations";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				 } else {
				  $emailsuccess=1;
				 }
unset($fname);unset($sname);unset($email);unset($status);unset($university); $add_success=1;


}

}

if (empty($_POST['year'])) { $_POST['year'] = date("Y")+3; }

	$year = 2005;
	for ($i = $year; $i <= date("Y")+5; ++$i) {
			$years[] = $year; ++$year;
	}
	rsort($years);


$sql = "SELECT * FROM universities ORDER BY SNAME";
$result=mysql_query($sql) or die(mysql_error());
$i = 0;
while($row = mysql_fetch_array($result)) {
$selected = 0; if($row['ID'] == $university) { $selected = 1; }

$rowdetails[$i]["uni_id"] = $row['ID'];
$rowdetails[$i]["uni_sname"] = $row['SNAME'];
$rowdetails[$i]["uni_fname"] = $row['FNAME'];
$rowdetails[$i]["selected"] = $selected;
$i++;
}


$smarty->assign('rowdetails', $rowdetails);
$smarty->assign('errormsg', $errormsg);
$smarty->assign('error', $error);

$smarty->assign('fname', $fname);
$smarty->assign('sname', $sname);
$smarty->assign('email', $email);
$smarty->assign('status', $status);
$smarty->assign('gender', $gender);

$smarty->assign('years', $years);
$smarty->assign('add_success', $add_success);
?>