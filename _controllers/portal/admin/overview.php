<?php
$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ./"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;
}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//Include this early as referenced by image script
$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

// Include image manipulation script
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/simpleimage.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/FaceDetector.php");

//include nav nav menu
$page = "OVERVIEW"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

if ($can_edit && $_POST['submitted']) {
//edit form submitted
$do_hometowns = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['hometowns']))));
$do_biography = mysql_real_escape_string($_POST['biography']);
$do_current_photo = mysql_real_escape_string($do_current_photo);
$do_school_photo = mysql_real_escape_string($do_school_photo);
$do_current_photo_del = mysql_real_escape_string($_POST['current_photo_del']);
$do_school_photo_del = mysql_real_escape_string($_POST['current_school_del']);
$do_birthday_day = mysql_real_escape_string($_POST['birthday_day']);
$do_birthday_month = mysql_real_escape_string($_POST['birthday_month']);
$do_birthday_year = mysql_real_escape_string($_POST['birthday_year']);
if (!is_numeric($do_birthday_day.$do_birthday_month.$do_birthday_year)) { die("Fatal error."); }
$do_birthday = $do_birthday_year . "-" . $do_birthday_month . "-" . $do_birthday_day;

$filename = $_FILES['current_photo']['name'];
$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); // Get the extension from the filename.
$allowed_filetypes = array('.jpg','.gif','.png'); // These will be the types of file that will pass the validation.

		// Upload users photos
		if ($_FILES['current_photo']['size'] != '' && in_array($ext,$allowed_filetypes)) {

		$size = $_FILES['current_photo']['size']/1024;

			$do_current_photo = '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg';
			if (move_uploaded_file($_FILES['current_photo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $do_current_photo) == false) {
				$smarty->assign('error_message', 'Error: Could not upload ' . $_FILES['current_photo']['name'] . '. Save Failed.');
				break;
			}
			else {

list($l_width, $l_height, $l_type, $l_attr) = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/content_files/member_photos/' . $_SESSION['user_id'] . '-current' . '.jpg');

			if ($size > 250 || $l_width > 300) { //if size is too big, face recognition will crash, so resize now (sacrifices quality for cropped image)

			//resize image
			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg');
			   $image->resizeToWidth(150);
			   $image->save($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg');
			}

			//generate crop
$detector = new FaceDetector();
$detector->scan($_SERVER['DOCUMENT_ROOT'] . '/content_files/member_photos/' . $_SESSION['user_id'] . '-current' . '.jpg');
$faces = $detector->getFaces();
foreach($faces as $face) {
$x = $face['x'];
$y = $face['y'];
$width = $face['width'];
$height = $face['height'];
}

			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current' . '.jpg');
if (is_numeric($width)) {
			   $image->crop($width,$height,$x,$y);
}
			   $image->resizeToHeight(80);
			   $image->resizeToWidth(50);
			   $image->crop(50, 50, 0, 0);
			   $image->resize(50, 50);
			   $image->save($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-cropped' . '.jpg');

			//resize image
			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg');
			   $image->resizeToWidth(150);
			   $image->save($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg');

			//resize image for thumbnail
			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-current.jpg');
			   $image->resizeToHeight(50);
			   $image->save($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . $_SESSION['user_id'] . '-thumb.jpg');


			}
		}
		
		if ($_FILES['school_photo']['tmp_name'] != '') {
			$do_school_photo = '/content_files/member_photos/' . $_SESSION['user_id'] . '-school' . '.jpg';
			if (move_uploaded_file($_FILES['school_photo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $do_school_photo) == false) {
				$smarty->assign('error_message', 'Error: Could not upload ' . $_FILES['school_photo']['name'] . '. Save Failed.');
				break;
			}
		}

		if($_FILES['current_photo']['size'] != '' && !in_array($ext,$allowed_filetypes)) {
		$error++; $errormsg .= "Image must be .jpg, .gif or .png!";
		}

if ($error < 1) {
//first delete all the hometown tags which match for this user
$sql = "DELETE FROM tags WHERE uid = '$profile_id' AND ttype = 'hometown'";
$result = mysql_query($sql) or die(mysql_error());
//now add the tags
foreach ($do_hometowns as $i) {
if(strlen($i) > 3) {
$sql = "INSERT INTO tags (uid,ttype,tag) VALUES ('$profile_id','hometown','$i')";
$result = mysql_query($sql) or die(mysql_error());
}

//check for photos
if (strlen($do_current_photo) < 1) { $do_current_photo = $profile['current_photo']; }
if (strlen($do_school_photo) < 1) { $do_school_photo = $profile['school_photo']; }

if ($do_current_photo_del) { $do_current_photo = ''; }
if ($do_school_photo_del) { $do_school_photo = ''; }

//now add biography, photos & birthday
$sql = "UPDATE members SET biography = '$do_biography', current_photo = '$do_current_photo', school_photo = '$do_school_photo', birth_date = '$do_birthday' WHERE id = '$profile_id'";
$result = mysql_query($sql) or die(mysql_error());

}

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','overview','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location:overview.php?id=$profile_id");

} //end if no error

} //end if form submitted and editing priviledges

//get tags (hometown)
$sql = "SELECT tag FROM tags WHERE uid = '$profile_id' AND ttype = 'hometown' ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hometown'][]['tag'] = $row[0];

load_unis(); //loads universities into university array
$profile['uni_sname'] = $university[$profile['ug_school_id']]['sname'];
$profile['uni_fname'] = $university[$profile['ug_school_id']]['fname'];

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}

if ($not_viewable != 1) {

//load up jobs - code now redundant, used to pick out the first listed job
//$sql = "SELECT * FROM jobs WHERE uid = '$profile_id' ORDER BY end DESC, start DESC, employer ASC";
//$result = mysql_query($sql) or die(mysql_error());
//while ($row = mysql_fetch_assoc($result))
//    $jobs[] = $row;
//    $profile['employer'] = $jobs[0]['employer'];

if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }

//calculate graduation past/present
$this_year = date("Y");
$this_month = date("n");
$grad_year = $profile['graduation_year'];
if ($grad_year < $this_year) { $graduated = 1; }
if ($grad_year > $this_year) { $graduating = 1; }
if ($grad_year == $this_year) {
if ($this_month > 7) { $graduated = 1; } else { $graduating = 1; }
}

if ($graduated == 1) { $grad_text = "Graduated:"; } else { $grad_text = "Graduating:"; }


$title = "Member";
if ($profile['analyst']) { $title = "Analyst";}
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$profile['title'] = $title;

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;

	for($day=1; $day <= 31; ++$day) {
		if ($day <= 9) { $day = "0" . $day; }
		$days[] = $day;
	}

		$months[] = array("01","January");
		$months[] = array("02","February");
		$months[] = array("03","March");
		$months[] = array("04","April");
		$months[] = array("05","May");
		$months[] = array("06","June");
		$months[] = array("07","July");
		$months[] = array("08","August");
		$months[] = array("09","September");
		$months[] = array("10","October");
		$months[] = array("11","November");
		$months[] = array("12","December");

	$year = 1980;
	for ($i = $year; $i <= date("Y")-15; ++$i) {
			$years[] = $year; ++$year;
	}
	rsort($years);

$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('tags', $tags);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);
$smarty->assign('grad_text', $grad_text);

$smarty->assign('days', $days);
$smarty->assign('months', $months);
$smarty->assign('years', $years);


$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

}

$smarty->assign('not_viewable', $not_viewable);
?>