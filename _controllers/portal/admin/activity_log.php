<?php

$smarty->compile_check = true;

// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

if (!isset($_GET['popup'])) {

$req_string = "";

//check if filters applied
if($_GET['submitted']) {
//do filters first
   //transfer fields from JS to PHP
$fields = array();
$fieldsi = array();
if(!is_array($_GET['r'])) { header("Location: activity_log.php"); }
foreach ($_GET['r'] as $key => $value) {
$fields[$key] = 1;
$fieldsi[] = $key;
$req_string .= "&r[".$key."]=".$value;
$req_string .= "&submitted=1";
}


}

$valid_fields = array("admin_add", "admin_delete", "admin_loginas", "admin_user_add", "db_download", "db_edit", "db_search", "user_login", "user_login_fail", "profile_update", "user_pw_change",
"user_pw_reset_req", "user_login_remember_me", "survey_add", "survey_download", "survey_vote", "research_add", "research_delete", "research_update", "dms_file_delete", "dms_file_download", "dms_file_upload",
"dms_folder_create", "dms_folder_delete", "resume_download", "resume_search", "resume_upload");

	$tbl_name="logs";		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$query = "SELECT COUNT(*) as num FROM $tbl_name WHERE 1=0";

	//do filters
	$rquery = $query;
	foreach($valid_fields as $key => $value) {
	if (isset($fields[$value])) { $query .= " OR type = '". $value ."'"; }
	} //end foreach

	if ($rquery == $query) { $query = "SELECT COUNT(*) as num FROM $tbl_name"; }

	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = "activity_log.php"; 		//your file name  (the name of this file)
	$limit = 10; 								//how many items to show per page
	$page = $_GET['page'];
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$rootsql = "SELECT * FROM $tbl_name WHERE 1=0";


	//do filters
	$sql = $rootsql;
	foreach($valid_fields as $key => $value) {
	if (isset($fields[$value])) { $sql .= " OR type = '". $value ."'"; }
	} //end foreach

	if ($sql == $rootsql) { $sql = "SELECT * FROM $tbl_name"; }


	$sql .= " ORDER BY time DESC LIMIT $start, $limit";
	$result = mysql_query($sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	//include pagination script (variables defined above)
	include($_SERVER['DOCUMENT_ROOT'] . "/includes/pagination.php");	
	
		while($row = mysql_fetch_array($result))
		{
	
$this_id = $row['uid'];
$sql2 = "SELECT first_name,last_name FROM members WHERE id ='$this_id' LIMIT 1";
$result2=mysql_query($sql2);
while($row2=mysql_fetch_array($result2)) {
$row['user'] = $row2['first_name']." ".$row2['last_name'];
}

$logs[] = $row;
	
		}

$smarty->assign('pagination', $pagination);
$smarty->assign('logs', $logs);

} //end if popup

elseif (is_numeric($_GET['popup'])) {
$log_id = $_GET['popup'];

$sql = "SELECT * FROM logs WHERE id = '$log_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$popup = $row;
$this_id = $popup['uid'];
$sql2 = "SELECT first_name,last_name FROM members WHERE id ='$this_id' LIMIT 1";
$result2=mysql_query($sql2);
while($row2=mysql_fetch_array($result2)) {
$popup['user'] = $row2['first_name']." ".$row2['last_name'];
}

// if database edit fetch all data
$this_time = $popup['time'];
$sql = "SELECT * FROM logs_pme WHERE updated = '$this_time'";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$db_edit_fields[] = $row;
}

// if admin_loginas
if ($popup['type'] == "admin_loginas") {
$sql = "SELECT first_name, last_name FROM members WHERE id = '".$popup['data']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$popup['uname'] = $row[0]." ".$row[1];
}

//if dms_file_delete or dms_file_download or dms_file_upload
if ($popup['type'] == "dms_file_delete" || $popup['type'] == "dms_file_download" || $popup['type'] == "dms_file_upload") {
$sql = "SELECT name FROM dms_file WHERE id_file = '".$popup['data']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$popup['file_name'] = $row[0];
}

//if dms_folder_create or dms_folder_delete
if ($popup['type'] == "dms_folder_create" || $popup['type'] == "dms_folder_delete") {
$sql = "SELECT name FROM dms_folder WHERE id_folder = '".$popup['data']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$popup['folder_name'] = $row[0];
}

//if survey_add or survey_download or survey_vote
if ($popup['type'] == "survey_add" || $popup['type'] == "survey_download" || $popup['type'] == "survey_vote") {
$sql = "SELECT question FROM vote_questions WHERE id = '".$popup['data']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$popup['survey_name'] = $row[0];
}

}

$smarty -> assign('popup', $popup);
$smarty -> assign('db_edit_fields', $db_edit_fields);

}

$smarty -> assign('fieldsi', $fieldsi);

?>