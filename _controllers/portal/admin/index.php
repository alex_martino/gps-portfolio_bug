<?php
//iniatialise variables
$error = array();

//handle mailing list password request
if (isset($_GET['fl'])) {
	$name = mysql_real_escape_string($_GET['listname']);
	$sql = "SELECT * FROM mailman_lists WHERE name = '$name' LIMIT 1";
	$result = mysql_query($sql);

	while($row = mysql_fetch_array($result)) {

	//log the request
	$l_sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mailing_list_access','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','" . $name ."','".$_SERVER['REMOTE_ADDR']."')";
	$l_result = mysql_query($l_sql) or die(mysql_error());

	die ($row['password']);
	}
}

include($_SERVER['DOCUMENT_ROOT']."/includes/xmlapi.php");

$ip = $_SERVER['SERVER_ADDR'];

//get credentials
include($_SERVER['DOCUMENT_ROOT']."/config_admin.php");

$user = $cpanel_username;
$password = $cpanel_password;

$xmlapi = new xmlapi($ip);
$xmlapi->set_port(2083);
$xmlapi->password_auth($user, $password);
$xmlapi->set_debug(0);
$xmlapi->set_output("array");

//delete email forward
if (isset($_GET['del_fwd'])) {
$del_gps_email = mysql_real_escape_string($_GET['del_gps_email']);
$del_dest_email = mysql_real_escape_string($_GET['del_dest_email']);
$result = $xmlapi->api1_query($user, "Email", "delforward", array($del_gps_email."=".$del_dest_email));
//log the email forward deletion
$data = $del_gps_email . " => " . $del_dest_email;
$l_sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('email_forward_delete','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','" . $data ."','".$_SERVER['REMOTE_ADDR']."')";
$l_result = mysql_query($l_sql) or die(mysql_error());
header("Location: index.php?successmsg=1&message=email_fwd_deleted");
die();
}

//Get Email Forwards
$result = $xmlapi->api2_query($user, "Email", "listforwards");
$cp_res_emailfwds = $result['data'];

$fwd_count = count($cp_res_emailfwds);

//Get Mailing Lists
$result = $xmlapi->api2_query($user, "Email", "listlists");
$mailinglists_list = $result['data'];

$lists_count = count($mailinglists_list);

//handle email forward add check request
if (isset($_GET['ef'])) { //start AJAX processing

 if (isset($_GET['gps_email'])) {

 $gps_email = $_GET['gps_email'];
 $pos = strpos($gps_email, "@gps100.com");
 if (!$pos) { $gps_email = $gps_email . "@gps100.com"; }
 include($_SERVER['DOCUMENT_ROOT']."/includes/email_validate.php");
 if (check_email_address($gps_email)) {

$i = 0;
while ($i < $fwd_count) {
if($cp_res_emailfwds[$i]['dest'] == $gps_email) {
die("FORWARD_EXISTS");
}
$i++;
}

$i = 0;
while ($i < $lists_count) {
if($mailinglists_list[$i]['list'] == $gps_email) {
die("MAILING_LIST_EXISTS");
}
$i++;
}

 $gps_email = substr($gps_email, 0, -11);
 die($gps_email);
 }
 else {
 die("EMAIL_INVALID");
 }


 }

elseif (isset($_GET['dest_email'])) { //start dest email processing

$dest_email = $_GET['dest_email'];

 include($_SERVER['DOCUMENT_ROOT']."/includes/email_validate.php");
 if (check_email_address($dest_email)) {
 die($dest_email);
}

else {
die("EMAIL_INVALID");
}

} //end dest email processing

} //end AJAX processing

//Add email forward process
if (isset($_POST['gps_email'])) {
$gps_email = $_POST['gps_email'];
$dest_email = $_POST['email_dest'];
$result = $xmlapi->api2_query($user, "Email", "addforward", array(domain=>"gps100.com", email=>$gps_email, fwdopt=>"fwd", fwdemail=>$dest_email) );
//log the email forward add
$data = $gps_email . " => " . $dest_email;
$l_sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('email_forward_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','" . $data ."','".$_SERVER['REMOTE_ADDR']."')";
$l_result = mysql_query($l_sql) or die(mysql_error());
header("Location: index.php?successmsg=1&message=email_fwd_added");
die();
} //end email forward process

//handle mailing list add check request
if (isset($_GET['ml'])) { //start AJAX processing

 if (isset($_GET['list_name'])) {

 $list_name = $_GET['list_name'];
 $pos = strpos($list_name, "@gps100.com");
 if (!$pos) { $list_name = $list_name . "@gps100.com"; }
 include($_SERVER['DOCUMENT_ROOT']."/includes/email_validate.php");
 if (check_email_address($list_name)) {

$i = 0;
while ($i < $lists_count) {
if($mailinglists_list[$i]['list'] == $list_name) {
die("MAILING_LIST_EXISTS");
}
$i++;
}

$i = 0;
while ($i < $fwd_count) {
if($cp_res_emailfwds[$i]['dest'] == $list_name) {
die("FORWARD_EXISTS");
}
$i++;
}

 $list_name = substr($list_name, 0, -11);
 die($list_name);
 }
 else {
 die("LIST_NAME_INVALID");
 }


 }

elseif (isset($_GET['list_password'])) { //start password processing

$list_password = $_GET['list_password'];

if (ctype_alnum($list_password)) { die($list_password); }

else {
die("PASSWORD_INVALID");
}

} //end dest email processing

} //end AJAX processing

//Add mailing_list process
if (isset($_POST['list_name'])) {
$list_name = $_POST['list_name'];
$list_password = $_POST['list_password'];

 $pos = strpos($list_name, "@gps100.com");
 if (!$pos) { $list_name = $list_name . "@gps100.com"; }
 $list_name = substr($list_name, 0, -11);

//Create the list
$result = $xmlapi->api1_query($user, "Email", "addlist", array($list_name, $list_password, "gps100.com") );

//Set the default settings
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/members/list";
//$cookie_new = tempnam("/tmp","MKUP");
$cookie_new = "/tmp/cookie.txt";

//Set the default settings - log in to Mailman
$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
          'adminpw' => $list_password
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

$cookie_status = array();
$cookie_contents = file_get_contents($cookie_new);

curl_close($ch);

function get_csrf_token($url,$cookie_new) {
//Get the CSRF token

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);
	  
		$doc = new DOMDocument;
		if ( !$doc->loadhtml($postResult) ) {
			echo 'something went wrong';
		}
		else {
			$xpath = new DOMXpath($doc);
			foreach($xpath->query('//form//input[@name="csrf_token"]') as $eInput) {
				$csrf_token = $eInput->getAttribute('value');
			}
			return $csrf_token;
		}
}
	  
//Set the default settings - general settings
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/general";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'owner' => "admin@gps100.com",
          'subject_prefix' => '',
          'send_reminders' => '0',
          'send_welcome_msg' => '0',
          'send_goodbye_msg' => '0',
          'administrivia' => '0',
          'max_message_size' => '0',
          'admin_member_chunksize' => '100',
          'max_days_to_hold' => '90',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);


//Set the default settings - non digest option
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/nondigest";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'msg_footer' => "",
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);


//Set the default settings - digest option
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/digest";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'digestable' => '0',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Set the default settings - privacy options - subscription rules
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/privacy/subscribing";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'advertised' => '0',
          'subscribe_policy' => '1',
          'private_roster' => '2',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Set the default settings - privacy options - sender filters
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/privacy/sender";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'generic_nonmember_action' => '0',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Set the default settings - privacy options - recipient filters
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/privacy/recipient";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'require_explicit_destination' => '0',
          'max_num_recipients' => '0',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Set the default settings - bounce processing
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/bounce";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'bounce_processing' => '0',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Set the default settings - archive
$url = "http://gps100.com/mailman/admin/".$list_name."_gps100.com/archive";

$csrf_token = get_csrf_token($url,$cookie_new);

$ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'csrf_token' => $csrf_token,
          'archive' => '0',
          'archive_private' => '1',
          'submit' => "submit"
      )));
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_new);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_new);
      curl_setopt($ch, CURLOPT_HEADER, 1);
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
      $postResult = curl_exec($ch);

//Add list pw to db
$i_sql = "INSERT INTO mailman_lists (name, password) VALUES ('".$list_name."', '".$list_password."')";
$i_result = mysql_query($i_sql) or die(mysql_error());

//log the mailing list add
$data = $list_name . " (" . $list_password . ")";
$l_sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mailing_list_add','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','" . $data ."','".$_SERVER['REMOTE_ADDR']."')";
$l_result = mysql_query($l_sql) or die(mysql_error());
header("Location: index.php?successmsg=1&message=mailing_list_added");
die();
} //end mailing_list process

//Check Mailman version
$urldata = file_get_contents("http://gps100.com/mailman/admin");
$versionpos = strpos($urldata, "version");
$version = substr($urldata,$versionpos+8,6);
$version_major = substr($version,0,1);
$version_minor = substr($version,0,3);
$version_exact = substr($version,0,6);

// Check version matches exactly (alter depending on availability of a techie to upgrade script)
// Note: When Mailman 3 is released it should include an API
$mailman_curl_error = 0;
if ($version_minor != "2.1") {
	$mailman_curl_error = 1;
}


//Get Stats
$args = array('display' => 'bandwidthusage|diskusage|hostingpackage');
$result = $xmlapi->api2_query($user, "StatsBar", "stat", $args);
$result = $result['data'];
$cp_admin['hosting']['bandwidth_used'] = round($result[0]['_count'],0);
$cp_admin['hosting']['bandwidth_quota'] = $result[0]['_max'];
$cp_admin['hosting']['disk_space_used'] = round($result[1]['_count'],0);
$cp_admin['hosting']['disk_space_quota'] = $result[1]['_max'];
$cp_admin['hosting']['package_name'] = $result[2]['value'];

//if data ok
if ($cp_admin['hosting']['disk_space_quota'] > 0 && $cp_admin['hosting']['bandwidth_quota'] > 0) {

//check not close to limits
if ($cp_admin['hosting']['disk_space_used']/$cp_admin['hosting']['disk_space_quota'] > 0.9) {
$cp_admin['hosting']['disk_space_low'] = 1;
}
if ($cp_admin['hosting']['bandwidth_used']/$cp_admin['hosting']['bandwidth_quota'] > 0.9) {
$cp_admin['hosting']['bandwidth_low'] = 1;
}

}

else {
$error['hosting_details'] = 1;
}

//Get Portal Admins
$sql = "SELECT id, first_name, last_name FROM members WHERE admin = 1 ORDER BY first_name ASC";
$results = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($results)) {
$admin_list[] = $row;
}

//First Zero the login list
$sql = "SELECT id FROM members";
$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
	$login_record[$row['id']] = 0;
}

//Get Recent Logins
$login_list = array(); //initialise
$sql = "SELECT logs.time, logs.uid, members.first_name, members.last_name FROM logs RIGHT JOIN members On logs.uid = members.id WHERE logs.type = 'user_login' OR logs.type='user_login_remember_me' ORDER BY logs.time DESC LIMIT 50";
$results = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($results)) {
	if (isset($row['uid'])) {
		if ($login_record[$row['uid']] != 1) {
			$login_list[] = $row;
			$login_record[$row['uid']] = 1;
		}
	}
}

//Get Recent Surveys
$sql = "SELECT * FROM vote_questions ORDER BY deadline DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
//get total
$qid = $row['id'];
$sql = "SELECT sum(total) as total FROM vote_options WHERE qid = '$qid'";
$result2 = mysql_query($sql) or die(mysql_error());
while ($row2 = mysql_fetch_array($result2)) {
$row['total'] = $row2['total'];
}
$survey_list[] = $row;
}

//Get Recent Transcripts
$sql = "SELECT chatroom_id, DATE(FROM_UNIXTIME(sent)) date FROM arrowchat_chatroom_messages GROUP BY chatroom_id, DATE(FROM_UNIXTIME(sent)) ORDER BY date DESC LIMIT 50";
$results = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($results)) {

$date = $row['date'];
$chatroomid = $row['chatroom_id'];

//get number of messages
$sql2 = "SELECT * FROM arrowchat_chatroom_messages WHERE DATE(FROM_UNIXTIME(sent)) = '$date' AND chatroom_id = '$chatroomid'";
$results2 = mysql_query($sql2) or die(mysql_error());
$row['message_num'] = mysql_num_rows($results2);

//match up to the right sector
if ($chatroomid == 9) { $row['sector'] = 'Consumers'; $row['sector_s'] = 'consumers'; }
if ($chatroomid == 13) { $row['sector'] = 'Energy & Natural Resources'; $row['sector_s'] = 'enr'; }
if ($chatroomid == 15) { $row['sector'] = 'Financials'; $row['sector_s'] = 'financials'; }
if ($chatroomid == 17) { $row['sector'] = 'GM'; $row['sector_s'] = 'gm'; }
if ($chatroomid == 18) { $row['sector'] = 'Healthcare'; $row['sector_s'] = 'healthcare'; }
if ($chatroomid == 19) { $row['sector'] = 'Industrials'; $row['sector_s'] = 'industrials'; }
if ($chatroomid == 20) { $row['sector'] = 'Technology'; $row['sector_s'] = 'technology'; }
if (!empty($row['sector'])) { $transcript_list[] = $row; }
}

$emailfwd_list = $cp_res_emailfwds;

if (count($emailfwd_list) < 5) {
$error['emailfwd_list'] = 1;
}

//Get Recent Resumes
$sql = "SELECT id, first_name, last_name, resume_date FROM members WHERE disabled = 0 AND resume_uploaded = 1 ORDER BY resume_date DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$resume_list[] = $row;
}

//Document Stats
$sql = "SELECT COUNT(*) as file_count, SUM(file_size) as disk_usage FROM dms_file WHERE recycle_bin = 0";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$documents_file_count = $row['file_count'];
$documents_disk_usage = $row['disk_usage'];
$documents_disk_usage_rounded = round(($row['disk_usage']/1048576),2); //convert to megabytes

$sql = "SELECT COUNT(*) as file_count, SUM(file_size) as disk_usage FROM dms_file WHERE recycle_bin = 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$documents_recycled_file_count = $row['file_count'];
$documents_recycled_disk_usage = $row['disk_usage'];
$documents_recycled_disk_usage_rounded = round(($row['disk_usage']/1048576),2); //convert to megabytes

$total_disk_usage = $documents_disk_usage + $documents_recycled_disk_usage;
$documents_total_disk_usage = round(($total_disk_usage/1048576),2); //convert to megabytes

//Documents Stats (by user)
$sql = "SELECT d.id_user id, sum(d.file_size) totalbytes, m.first_name, m.last_name FROM dms_file d, members m WHERE d.id_user = m.id GROUP BY id_user ORDER BY totalbytes DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$dms_user_stats[] = $row;
}

//Document Stats (by file)
$sql = "SELECT id_file id, file_name, download_count, id_folder FROM dms_file ORDER BY download_count DESC, file_name ASC LIMIT 50";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$dms_file_stats[] = $row;
}

$smarty -> assign('admin_list', $admin_list);
$smarty -> assign('emailfwd_list', $emailfwd_list);
$smarty -> assign('mailinglists_list', $mailinglists_list);
$smarty -> assign('login_list', $login_list);
$smarty -> assign('survey_list', $survey_list);
$smarty -> assign('transcript_list', $transcript_list);
$smarty -> assign('cp_admin', $cp_admin);
$smarty -> assign('resume_list', $resume_list);
$smarty -> assign('documents_disk_usage', $documents_disk_usage_rounded);
$smarty -> assign('documents_file_count', $documents_file_count);
$smarty -> assign('documents_recycled_file_count', $documents_recycled_file_count);
$smarty -> assign('documents_recycled_disk_usage', $documents_recycled_disk_usage_rounded);
$smarty -> assign('documents_total_disk_usage', $documents_total_disk_usage);
$smarty -> assign('mailman_curl_error', $mailman_curl_error);
$smarty -> assign('dms_user_stats', $dms_user_stats);
$smarty -> assign('dms_file_stats', $dms_file_stats);

$smarty -> assign('error', $error);

?>