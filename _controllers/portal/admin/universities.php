<?php

// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$error=0;

$display = 'default';
if (isset($_GET['display'])) {
$display = $_GET['display'];
}
if (isset($_POST['addsubmitted'])) {
//University add form submitted
$display = add;

$sname = $_POST['sname'];
$fname = $_POST['fname'];

$s_valid = 0;
$f_valid = 0;
$s_valid = preg_match('/^[a-zA-Z ]+$/', $sname);
$f_valid = preg_match('/^[a-zA-Z ]+$/', $fname);
if ($s_valid == 0) {
$error++; $errormsg .= "Invalid characters entered in abbreviated name.<BR>";
}
if ($f_valid == 0) {
$error++; $errormsg .= "Invalid characters entered in full name.<BR>";
}

//Check if university already exists
$sql = "SELECT * FROM universities WHERE SNAME = '$sname'";
$result=mysql_query($sql) or die(mysql_error());
$count=mysql_num_rows($result);
if($count==1){
$error++;
$errormsg .= "A university with this abbreviated name already exists.<BR>";
}
$sql = "SELECT * FROM universities WHERE FNAME = '$fname'";
$result=mysql_query($sql);
$count=mysql_num_rows($result);
if($count==1){
$error++;
$errormsg .= "A university with this full name already exists.<BR>";
}

if ($error == 0) {
//No errors - process new university
$sql = "INSERT into universities (SNAME, FNAME) VALUES ('$sname', '$fname')";
$result=mysql_query($sql) or die(mysql_error());
$display='default';
}

}

//get university list
$query = "SELECT * FROM universities";
$result = mysql_query($query) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$universities[] = $row;
}

$smarty -> assign('universities', $universities);
$smarty -> assign('display', $display);

$smarty -> assign('error', $error);
$smarty -> assign('errormsg', $errormsg);

$smarty -> assign('sname', $sname);
$smarty -> assign('fname', $fname);

?>