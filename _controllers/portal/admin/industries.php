<?php

// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$error=0;

$display = 'default';
if (isset($_GET['display'])) {
$display = $_GET['display'];
}
if (isset($_POST['addsubmitted'])) {
//Industry add form submitted
$display = add;

$name = $_POST['name'];

$n_valid = 0;
$n_valid = preg_match('/^[a-zA-Z ]+$/', $name);
if ($n_valid == 0) {
$error++; $errormsg .= "Invalid characters entered in industry name.<BR>";
}

if (strlen($name) < 3) {
$error++; $errormsg .= "Industry name must be at least 3 characters long.<BR>";
}

if (strlen($name) > 149) {
$error++; $errormsg .= "Industry name must be less than 150 characters long.<BR>";
}

//Check if university already exists
$sql = "SELECT * FROM jobs_industries WHERE name = '$name'";
$result=mysql_query($sql) or die(mysql_error());
$count=mysql_num_rows($result);
if($count==1){
$error++;
$errormsg .= "An industry with this name already exists.<BR>";
}

if ($error == 0) {
//No errors - process new industry
$sql = "INSERT into jobs_industries (name) VALUES ('$name')";
$result=mysql_query($sql) or die(mysql_error());
$display='default';
}

}

//get university list
$query = "SELECT * FROM jobs_industries ORDER BY name ASC";
$result = mysql_query($query) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$industries[] = $row;
}

$smarty -> assign('industries', $industries);
$smarty -> assign('display', $display);

$smarty -> assign('error', $error);
$smarty -> assign('errormsg', $errormsg);

$smarty -> assign('sname', $sname);
$smarty -> assign('fname', $fname);

?>