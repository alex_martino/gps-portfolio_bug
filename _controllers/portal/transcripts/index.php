<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$now = date("Y-m-d H:i:s");
$ip = $_SERVER['REMOTE_ADDR'];
$uid = $_SESSION['user_id'];
$day = date("d");
$month = date("m");
$year = date("Y");

// Load specific message
if (is_numeric($_GET['show_message'])) {

$message_id = $_GET['show_message'];
$sql = "SELECT chatroom_id, sent FROM arrowchat_chatroom_messages WHERE id = $message_id LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
header("Location:index.php?sector=use_id&chatroom_id=".$row['chatroom_id']."&message_id=".$message_id."&date=".date('Y-m-d',$row['sent'])."#".$message_id);
}

}

// Searching Transcripts
if ($_GET['ts'] && strlen(trim(mysql_real_escape_string($_REQUEST['searchbox']))) < 2) {
$error++; $errormsg .= "Please enter a search term.<BR>";
}
elseif ($_GET['ts']) {

	$filter_sector = $_POST['filter_sector'];
	if (strlen($filter_sector) < 2) { $filter_sector = $_GET['filter_sector']; }
	$filter_sector = strtolower($filter_sector);

	if ($filter_sector == 'consumers') { $chatroomid = 9; }
	if ($filter_sector == 'enr') { $chatroomid = 13; }
	if ($filter_sector == 'financials') { $chatroomid = 15; }
	if ($filter_sector == 'gm') { $chatroomid = 17; }
	if ($filter_sector == 'healthcare') { $chatroomid = 18; }
	if ($filter_sector == 'industrials') { $chatroomid = 19; }
	if ($filter_sector == 'technology') { $chatroomid = 20; }

	//Fetch search query
	$s_query = mysql_real_escape_string($_REQUEST['searchbox']);
	$s_query = trim($s_query);
	$s_query_array = explode(" ",$s_query);

	// Log unique search queries (if query has been made today, not unique)
	$sql98 = "SELECT id FROM logs WHERE type='transcript_search' AND data='$s_query' AND DAY(time) = '$day' AND MONTH(time) = '$month' AND YEAR(time) = '$year' LIMIT 1";
	$result = mysql_query($sql98);
	if (mysql_num_rows($result) < 1) {
	$sql99 = "INSERT INTO logs (type, uid, data, time, ip_address) VALUES ('transcript_search','$uid','$s_query','$now','$ip')";
	$result = mysql_query($sql99);
	}

	$query_build = "SELECT * FROM arrowchat_chatroom_messages WHERE";
	foreach($s_query_array as $i) {
	$query_build .= " message LIKE '%$i%' OR";
	}
	$pos = strrpos($query_build,"OR");
	$query_build = substr($query_build,0,$pos-1);

	if (is_numeric($chatroomid)) {
	$query_build .= " AND chatroom_id = '$chatroomid'";
	}

	$query_build .= " ORDER BY sent DESC";


	// How many adjacent pages should be shown on each side?
	$adjacents = 3;

	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$query = str_replace("*", "COUNT(*)", $query_build);
	$query = str_replace("FROM", "as num FROM", $query);

	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages[num];

	/* Setup vars for query. */
	$targetpage = "index.php"; 	//your file name  (the name of this file)
	$limit = 10; 								//how many items to show per page
	$page = $_GET['page']; if (isset($_GET['page']) && !is_numeric($page)) { die("Fatal error 1223."); }
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
 
	/* Get data. */
	$sql = $query_build . " LIMIT $start, $limit";
	$s_results = mysql_query($sql);
	$results_count = mysql_num_rows($s_results);
 
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
 
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/

	$req_string = "&ts=1&searchbox=".$s_query;	

	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?&page=$prev".$req_string."\">&laquo; previous</a>";
		else
			$pagination.= "";	
		
		//pages
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter.$req_string\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter$req_string\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1".$req_string."\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage.$req_string\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1".$req_string."\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2".$req_string."\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter$req_string\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1.$req_string\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage.$req_string\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1".$req_string."\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2".$req_string."\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter.$req_string\">$counter</a>";					
				}
			}
		}
 
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next".$req_string."\">next &raquo;</a>";
		else
			$pagination.= "";
		$pagination.= "</div>\n";		
	}

while($row = mysql_fetch_array($s_results)) {
foreach ($s_query_array as $value) {
if ($found != 1) {
//$pos = strpos($row['message'],$value);
//if ($pos > 0) {
$row['message'] = substr($row['message'],0,75);
//$found = 1; $pos = 0;
$row['message'] = str_replace($value,"<B style='color:navy'>".$value."</B>",$row['message']);
//}
}
}
$chatroomid = $row['chatroom_id'];
if ($chatroomid == 9) { $sector = 'Consumers'; }
if ($chatroomid == 13) { $sector = 'ENR'; }
if ($chatroomid == 15) { $sector = 'Financials'; }
if ($chatroomid == 17) { $sector = 'GM'; }
if ($chatroomid == 18) { $sector = 'Healthcare'; }
if ($chatroomid == 19) { $sector = 'Industrials'; }
if ($chatroomid == 20) { $sector = 'Technology'; }
$row['sector'] = $sector;
$results_r[] = $row;
//$found = 0; $pos=0;
}


} //End Searching Transcripts

//check what we are filtering by
$sector = $_GET['sector'];
$chatroom_id_f = $_GET['chatroom_id'];

if(strlen($sector) > 1) {

if ($sector == "use_id" && is_numeric($chatroom_id_f)) {
$chatroomid = $chatroom_id_f;
if ($chatroomid == 9) { $sector = 'Consumers'; }
if ($chatroomid == 13) { $sector = 'ENR'; }
if ($chatroomid == 15) { $sector = 'Financials'; }
if ($chatroomid == 17) { $sector = 'GM'; }
if ($chatroomid == 18) { $sector = 'Healthcare'; }
if ($chatroomid == 19) { $sector = 'Industrials'; }
if ($chatroomid == 20) { $sector = 'Technology'; }
}

else {

if ($sector == 'consumers') { $chatroomid = 9; }
if ($sector == 'enr') { $chatroomid = 13; }
if ($sector == 'financials') { $chatroomid = 15; }
if ($sector == 'gm') { $chatroomid = 17; }
if ($sector == 'healthcare') { $chatroomid = 18; }
if ($sector == 'industrials') { $chatroomid = 19; }
if ($sector == 'technology') { $chatroomid = 20; }

}

//now see if we are filtering by dates
$date = $_GET['date'];

if (strlen($date) < 1) {
$sql = "SELECT DATE(FROM_UNIXTIME(sent)) date FROM arrowchat_chatroom_messages WHERE chatroom_id = '".$chatroomid."' GROUP BY DATE(FROM_UNIXTIME(sent)) ORDER BY date DESC";
$result = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($result)) {
$dates[]['date'] = $row[0];
}
}

elseif (is_numeric($chatroomid)) { //load up transcripts
$timestart = strtotime($date);
$timeend = $timestart + 86400;
$sql = "SELECT user_id, username, message, sent, id FROM arrowchat_chatroom_messages WHERE chatroom_id = '".$chatroomid."' AND sent BETWEEN '".$timestart."' AND '".$timeend."' ORDER BY sent ASC";
$result = mysql_query($sql) or die(mysql_error());
$i=0;
while ($row=mysql_fetch_array($result)) {
$messages[$i]['id'] = $row['id'];
$messages[$i]['userid'] = $row[0];
$messages[$i]['username'] = $row[1];
//$messages[$i]['message'] = $row[2];
$messages[$i]['message'] = preg_replace("@(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?������]))@", "<a href=\"$1\">$1</a>", $row[2]);

$messages[$i]['timestamp'] = $row[3];
$i++;
}
}

} //end check sector is defined

$sql = "SELECT * FROM sectors";
$result = mysql_query($sql) or die(mysql_error());
while ($row=mysql_fetch_array($result)) {
$sectors[] = $row;
}
$GM['id'] = 99; $GM['sname'] = "GM"; $GM['fname'] = "General Meeting";
$sectors[] = $GM;
function subval_sort($a,$subkey) {
	foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	asort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
}
$sectors = subval_sort($sectors,'sname');

// Assign variables
$smarty->assign('sector', $sector);
$smarty->assign('dates', $dates);
$smarty->assign('messages', $messages);
$smarty->assign('date', $date);
$smarty->assign('pagination', $pagination);

$smarty->assign('sectors', $sectors);
$smarty->assign('s_query', $s_query);
$smarty->assign('results_r', $results_r);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

$smarty->assign('results_count',$results_count);

?>