<?php

$error=0;

$profile_id = $_GET['id'];
if (!isset($_GET['id'])) { header("Location: ./"); }
if (!is_numeric($profile_id)) { die("Hack attempt"); }

//check if your profile
if ($_SESSION['member_id'] == $profile_id) { $can_edit = 1; }

//check if editing
if ($can_edit == 1 && $_GET['edit'] == 1) {
$is_editing = 1;
}

require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//include nav nav menu
$page = "CONTACT"; include ($_SERVER['DOCUMENT_ROOT'] . "/includes/network_t_menu.php"); $smarty->assign('network_t_menu', $network_t_menu);

if ($can_edit && $_POST['submitted']) {

//edit form submitted
$do_gmail_address = mysql_real_escape_string($_POST['gmail_address']);
$do_contact_number_country = explode("_", mysql_real_escape_string(str_replace("'", "", ($_POST['contact_number_country']))));
$do_contact_number = mysql_real_escape_string(str_replace("'", "", ($_POST['contact_number'])));
$do_contact_number = str_replace(" ","",$do_contact_number);
$do_contact_number = str_replace("-","",$do_contact_number);
$do_contact_number = str_replace(".","",$do_contact_number);
$do_skype = mysql_real_escape_string($_POST['skype']);
$do_address = mysql_real_escape_string($_POST['address']);
$do_address_private = mysql_real_escape_string($_POST['address_private']);
$do_alternative_email_1 = mysql_real_escape_string($_POST['alternative_email_1']);

//split country into DB ID and code
$do_contact_number_country_id = $do_contact_number_country[0];
$do_contact_number_country_code = $do_contact_number_country[1];


//check input
if (strlen($do_gmail_address) > 255) { $error++; $errormsg['gmail'] .= "Email address too long<BR>"; }
if (strlen($do_contact_number) > 50) { $error++; $errormsg['number'] .= "Contact number too long<BR>"; }
if (strlen($do_contact_number > 0) && !is_numeric($do_contact_number)) { $error++; $errormsg['number'] .= "Contact number not valid<BR>"; }
if (isset($do_contact_number_country_id) && !is_numeric($do_contact_number_country_id)) { $error++; $errormsg['number'] .= "Contact number country ID not valid<BR>"; }
if (isset($do_contact_number_country_code) && !is_numeric($do_contact_number_country_code)) { $error++; $errormsg['number'] .= "Contact number country code not valid<BR>"; }
if (strlen($do_skype) > 60) { $error++; $errormsg['skype'] .= "Skype username too long<BR>"; }
if (strlen($do_alternative_email_1) > 255) { $error++; $errormsg['alternative_email_1'] .= "Email address too long<BR>"; }
include($_SERVER['DOCUMENT_ROOT']."/includes/email_validate.php");
if (!check_email_address($do_gmail_address) && strlen($do_gmail_address) > 0) { $error++; $errormsg['gmail'] .= "Invalid email address<BR>"; }
if (!check_email_address($do_alternative_email_1) && strlen($do_alternative_email_1) > 0) { $error++; $errormsg['alternative_email_1'] .= "Invalid email address<BR>"; }

if ($error < 1) {

//now add the stuff
$sql = "UPDATE members SET gmail_address = '$do_gmail_address', contact_number_country_code = '$do_contact_number_country_code', contact_number_country_id = '$do_contact_number_country_id',
contact_number = '$do_contact_number', skype = '$do_skype', address = '$do_address', address_private = '$do_address_private', alternative_email_1 = '$do_alternative_email_1' WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

//add logs
$sql2 = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('profile_update','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','contact','".$_SERVER['REMOTE_ADDR']."')";
$result = mysql_query($sql2) or die("There was a problem logging the download. Please contact an admin.");

header("Location:contact.php?id=$profile_id");

} //end if no error

} //end if form submitted and editing priviledges


if ($is_editing) { //is editing

//load countries for phone number
$sql = "SELECT * FROM countries";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$countries[] = $row;


}

//get tags (hometown)
$sql = "SELECT tag FROM tags WHERE uid = '$profile_id' AND ttype = 'hometown' ORDER BY tag ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result))
$tags['hometown'][]['tag'] = $row[0];


$sql = "SELECT * FROM members WHERE id = '$profile_id' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$profile = mysql_fetch_array($result);

//check if account has been disabled, or elected to be hidden
if ($profile['hidden'] == 1 || $profile['disabled'] == 1) {
$not_viewable = 1;
}

if ($not_viewable != 1) {

if(strlen($profile['current_photo']) > 0) { $display_photo = 1; }

// check if US phone number, if yes, add hyhens
if ($profile['contact_number_country_id'] == '225' && strlen($profile['contact_number']) == 10) {
$profile['valid_us_number'] = "1-".substr($profile['contact_number'], 0, 3)."-".substr($profile['contact_number'], 3, 3)."-".substr($profile['contact_number'], 6, 4);
}

$title = "Member";
if ($profile['analyst']) { $title = "Analyst"; }
if ($profile['gps_position_held']) { $title = $profile['gps_position_held']; }
if ($profile['alumni']) { $title = "Alumni"; }
if ($profile['chapter_founder']) { $title = "Chapter Founder"; }
if ($profile['founding_member']) { $title = "Founder"; }
if ($profile['advisory_board']) { $title = "Advisor"; }

$title = $profile['first_name'] . " " . $profile['last_name'] . " | ". $title;


$smarty->assign('can_edit', $can_edit);
$smarty->assign('is_editing', $is_editing);
$smarty->assign('profile_id', $profile_id);

$smarty->assign('tags', $tags);
$smarty->assign('countries', $countries);

$smarty->assign('profile', $profile);
$smarty->assign('display_photo', $display_photo);
$smarty->assign('title', $title);

$smarty->assign('error', $error);
$smarty->assign('errormsg', $errormsg);

}

$smarty->assign('not_viewable', $not_viewable);

?>