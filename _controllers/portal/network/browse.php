<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

//Check for entries
$sql = "SELECT      m.id,
                    m.first_name,
                    m.last_name,
                    m.graduation_year,
                    m.current_employer,
                    m.ug_school_id,
		    m.current_photo,
                    u.FNAME AS undergraduate_school,
		    u.SNAME as undergraduate_school_s
        FROM        members m
        JOIN        universities u
        ON          m.ug_school_id = u.ID
        WHERE       NOT hidden
        AND         NOT disabled
        ORDER BY    m.graduation_year DESC,
		    m.first_name ASC";

$result = mysql_query($sql) or die(mysql_error());

while ($row = mysql_fetch_assoc($result)) {
//check for thumbnail, the second line is for backwards-compatibility with legacy set up
if (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['id']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['id']."-cropped.jpg"; }
elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg")) { $row['cropped_photo'] = "/content_files/member_photos/".$row['first_name']."-".$row['last_name']."-cropped.jpg"; }
else { $row['cropped_photo'] = "/images/male_thumb.png"; }
    $members_array[] = $row;
}

//retrieve years
foreach ($members_array as $temp) {
	if ($temp['graduation_year'] != '') {
		$years[$temp['graduation_year']][] = $temp;
	}
}


$smarty->assign('members_array', $members_array);


// Assign variables
$smarty->assign('years', $years);

?>