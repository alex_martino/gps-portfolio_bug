<?php
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

//define variables
$article_list = array();

	/* Setup vars for query. */
	$targetpage = "articles.php"; 				//the name of this file
	$limit = 10; 								//how many items to show per page
	$page = $_GET['page'];
	if(is_numeric($page))  {
		$start = ($page - 1) * $limit; 			//first item to display on this page
	}
	else {
		$start = 0;
	}

//fetch articles
$sql0 = "SELECT id, url, description, user_id, sender_name, time_received, token FROM articles a WHERE a.invalid = 0 AND sent = 1";
$sql = $sql0 .  " LIMIT " . $start . ", " . $limit;
$result = mysql_query($sql);

	//get total number of articles
	$sql_no_limit = $sql0;
	$result_no_lim = mysql_query($sql_no_limit);
	$num_rows = mysql_num_rows($result_no_lim);

while ($row = mysql_fetch_array($result)) {
	$article_list[] = $row;
}

	/* Setup page vars for display. */
	$adjacents = 3;								// How many adjacent pages should be shown on each side?
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							
	$next = $page + 1;							
	$lastpage = ceil($num_rows/$limit);			//lastpage is = total items / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	//include pagination script (variables defined above)
	include($_SERVER['DOCUMENT_ROOT'] . "/includes/pagination.php");

$smarty->assign('pagination', $pagination);
$smarty->assign('article_list', $article_list);
?>