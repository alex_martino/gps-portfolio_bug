<?
// Include site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

// Include image manipulation script
require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/simpleimage.php");

$db_connection = new db_connection('members');

switch ($_POST['action']) {
	case "update" :
		$_POST['id'] = $_SESSION['member_id'];
		
		// Upload users photos
		if ($_FILES['current_photo']['tmp_name'] != '') {
			$_POST['current_photo'] = '/content_files/member_photos/' . strtolower($_POST['first_name'] . '-' . $_POST['last_name']) . '-current' . '.jpg';
			if (move_uploaded_file($_FILES['current_photo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $_POST['current_photo']) == false) {
				$smarty->assign('error_message', 'Error: Could not upload ' . $_FILES['current_photo']['name'] . '. Save Failed.');
				break;
			}
			else { //resize image for thumbnail
			   $image = new SimpleImage();
			   $image->load($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . strtolower($_POST['first_name'] . '-' . $_POST['last_name']) . '-current' . '.jpg');
			   $image->resizeToHeight(50);
			   $image->save($_SERVER['DOCUMENT_ROOT']. '/content_files/member_photos/' . strtolower($_POST['first_name'] . '-' . $_POST['last_name']) . '-thumb' . '.jpg');
			}
		}
		
		if ($_FILES['school_photo']['tmp_name'] != '') {
			$_POST['school_photo'] = '/content_files/member_photos/' . strtolower($_POST['first_name'] . '-' . $_POST['last_name']) . '-school' . '.jpg';
			if (move_uploaded_file($_FILES['school_photo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $_POST['school_photo']) == false) {
				$smarty->assign('error_message', 'Error: Could not upload ' . $_FILES['school_photo']['name'] . '. Save Failed.');
				break;
			}
		}
		
		// Update this users info
		$db_connection->update('id', $_SESSION['member_id']);
		
		// Assign variables
		$smarty->assign('error_message', 'Save Successful!');
		break;
}

// Retrieve member info
$results = $db_connection->select_array("id = '" . $_SESSION['member_id'] . "'", 'last_name');

// Assign variables
$smarty->assign('member', $results[0]);
?>