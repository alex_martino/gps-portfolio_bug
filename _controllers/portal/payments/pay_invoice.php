<?php
$now = date("Y-m-d H:i:s");

function email_receipt($invoice_ids) {

$ids = explode(",", $invoice_ids);

foreach ($ids as $value) {

$sql = "SELECT m.first_name as first_name, m.email_address as email_address, if (p.annual_dues_flag = 1,concat('Annual dues: ',description),description) as Description, p.amount FROM payments as p JOIN members as m on m.id = p.user_id WHERE p.id = '".$value."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {


//email user their invoice
				//now email
				require_once "Mail.php";
				
				$from = "GPS Payments <payments@gps100.com>";
				$to = $row['email_address'];
				$subject = "GPS Payments Receipt - ".$row['Description'];
				$boundary = md5(date('U'));
				
				$host = "localhost";
				$username = "payments@gps100.com";
				$password = "202AlphaDog";

				$headers = array ('From' => $from,
				  'To' => $to,
				  'MIME-Version' => '1.0',
				  'Content-Type' => 'multipart/alternative; boundary='.$boundary,
				  'Subject' => $subject);
				$smtp = Mail::factory('smtp',
				  array ('host' => $host,
				    'auth' => true,
				    'username' => $username,
				    'password' => $password));

				$text="hi";
				$html="<b>Hi</b>";

				 $body = "This is a MIME encoded message."; 
 
				 $body .= "\r\n\r\n--" . $boundary . "\r\n";
				 $body .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
				 $body .= "Dear " . $row['first_name'].",\r\n\r\n";
				 $body .= "Thank you for paying your invoice for: ".$row['Description']."\r\n";
				 $body .= $row['amount']." has been charged to your card.\r\n\r\n";
				 $body .= "Thank you for paying it forward!\r\n\r\n";
				 $body .= "Sincerely,\r\n";
				 $body .= "The GPS Board";

				 $body .= "\r\n\r\n--" . $boundary . "\r\n";
				 $body .= "Content-type: text/html;charset=utf-8\r\n\r\n";
				 $body .= file_get_contents("http://www.gps100.com/includes/payments/invoice_details.php?id=".$value);

				 $body .= "\r\n\r\n--" . $boundary . "--";

				$mail = $smtp->send($to, $headers, $body);

				if (PEAR::isError($mail)) {
				  $error++; $errormsg = "Email Error";
				  error_log("Email didn't send!");
				 } else {
				  $email_success=1;
				 }

}

}

}

//Stripe config
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/stripe/lib/Stripe.php');
Stripe::setApiKey("oCh62C8D0FsV8ZwZePMpRu2rdkLI3b9L");

//get invoice IDS (removing duplicates)
$invoice_ids_dirty = array_unique(explode(",", $_GET['id']));

//Check status to prevent double payment

$invoices = array();

foreach ($invoice_ids_dirty as $key => $value) {

 //prevent hack attempts
 if (!is_numeric($value) && !empty($value)) { die("Fatal error."); }

 //check that invoice status is unpaid and belongs to current user (for each invoice)
 $sql = "SELECT * FROM payments WHERE id = '".$value."' AND user_id='".$_SESSION['member_id']."' LIMIT 1";
 $result = mysql_query($sql) or die(mysql_error());
 $row = mysql_fetch_array($result);
 $status = $row['status'];
 if ($status == "unpaid" or $status == "overdue") {
   $invoices[$value] = $row;
 }

}

$description = "";
$description_with_ids = "";
$payment_total = 0;

foreach ($invoices as $key => $value) {
if ($invoices[$key]['annual_dues_flag']) { $description .= ", Annual dues: "; $description_with_ids .= ", Annual dues: "; }
else { $description .= ", "; }
$description .= $invoices[$key]['description'];
$description_with_ids .= $invoices[$key]['description'] . " [" . $key . "]";
$payment_total = $payment_total + $invoices[$key]['amount'];
}

$payment_total = round($payment_total,2);

$description = substr($description, 2);
$description_with_ids = substr($description_with_ids, 2);

$invoice_count = count($invoices);

if ($invoice_count == 2) { $description = str_replace(","," and",$description); }


//check user has Stripe Customer ID
$sql = "SELECT stripe_customer_id FROM members WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$stripe_customer_id = $row['stripe_customer_id'];

//if customer id found, check it is found in Stripe, otherwise clear it
if (!empty($stripe_customer_id)) {

$response = Stripe_Customer::retrieve($stripe_customer_id);

//check if user account deleted on stripe
if(!empty($response->id) && ($response->deleted == 1)) {

$sql = "UPDATE members SET stripe_customer_id = '' WHERE id = '" . $_SESSION['user_id'] . "' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

}

}

//If no customer id found, create one
else {

$response = Stripe_Customer::create(array(
  "email" => $_SESSION['email'],
  "description" => $_SESSION['user_first_name'] . " " . $_SESSION['user_last_name'] . " (". $_SESSION['user_id'] . ")"
));

//now get the new customer ID and add to user in database
$stripe_customer_id = $response->id;

$sql = "UPDATE members SET stripe_customer_id = '".$stripe_customer_id."' WHERE id = '".$_SESSION['member_id']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());

}


//payment submitted
if (!empty($_POST)) {

if ($_POST['saved_card']) { //saved card

$error = 0; $error_s = array();

//check for invalid security code
if (isset($_POST['security_code']) && !is_numeric($_POST['security_code'])) {
$error++;
$error_s['security_code'] = 1;
}

elseif (strlen($_POST['security_code']) != 3 && strlen($_POST['security_code']) != 4) {
$error++;
$error_s['security_code'] = 1;
}

else { //code is ok

$response = Stripe_Charge::create(array(
  "amount" => $payment_total*100,
  "currency" => "usd",
  "customer" => $stripe_customer_id,
  "description" => $description_with_ids)
);

//transaction okay?
if ($response->paid) {
$sql = "UPDATE payments SET status = 'paid', updated = '".$now."', stripe_charge_id = '".$stripe_charge_id."' WHERE id IN(".implode(',',array_keys($invoices)).")";
$result = mysql_query($sql) or die(mysql_error());
email_receipt(implode(',',array_keys($invoices)));
header("Location: ./?pay_success=1");
}

else {
$error++;
$error_s['unknown_error'] = 1;
}

}


} //end saved card

else { //new card

$card_token = $_POST['stripeToken'];

//add card to user account
if (!empty($card_token)) {

$cu = Stripe_Customer::retrieve($stripe_customer_id);
$cu->card = $card_token; // obtained with stripe.js;
$cu->save();

//now charge the card and add card to customer
$response = Stripe_Charge::create(array(
  "amount" => $payment_total*100,
  "currency" => "usd",
  "customer" => $stripe_customer_id,
  "description" => $description_with_ids)
);

$stripe_charge_id = $response->id;

$sql = "UPDATE payments SET status = 'processing', updated = '".$now."', stripe_charge_id = '".$stripe_charge_id."' WHERE id IN(".implode(',',array_keys($invoices)).")";
$result = mysql_query($sql) or die(mysql_error());

//immediately check and see if Stripe mark the payments as 'paid'
$response = Stripe_Charge::retrieve($stripe_charge_id);
$paid = $response->paid;

//if paid - update db
if ($paid) {
$sql = "UPDATE payments SET status = 'paid', updated = '".$now."', stripe_charge_id = '".$stripe_charge_id."' WHERE id IN(".implode(',',array_keys($invoices)).")";
$result = mysql_query($sql) or die(mysql_error());
}

//now email user a receipt
email_receipt(implode(',',array_keys($invoices)));

//now send them back to the payments page
header("Location: ./?pay_success=1");

}

//card not successfully added - handle error
else {

$error++;
$errormsg .= "Error adding card - please contact admin.";

}

} //end new card

} //end payment submitted

//if previous customer, load their cards
$sql = "SELECT stripe_customer_id FROM members WHERE id = '" . $_SESSION['user_id'] ."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$response = Stripe_Customer::retrieve($row[0]);

//load active card into array
$active_card = array();
$active_card['expiry_month'] = $response->active_card->exp_month;
$active_card['expiry_year'] = $response->active_card->exp_year;
$active_card['last4'] = $response->active_card->last4;
$active_card['type'] = $response->active_card->type;

//check active card is not expired, if it is, pretend it doesn't exist, do the same if user requests to add a new card
$today = date("Y-m");
$active_card_expiry = $active_card['expiry_year'] . "-" . $active_card['expiry_month'];
if ($active_card_expiry < $today || $_GET['new_card']) {
unset($active_card);
}




// Assign variables
$smarty->assign('invoices', $invoices);
$smarty->assign('invoice_count', $invoice_count);
$smarty->assign('description', $description);
$smarty->assign('payment_total', $payment_total);
$smarty->assign('payment_total_cents', $payment_total*100);

$smarty->assign('invoice_list', $_GET['id']);
$smarty->assign('active_card', $active_card);
$smarty->assign('error_s', $error_s);
?>