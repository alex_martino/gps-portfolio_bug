<?
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/xml_parser.class.php");

if ($_POST['search_for']) {
	$query = $_POST['search_for'];
} else if ($_REQUEST['q']) {
	$query = $_REQUEST['q'];
} else if ($_GET['q']) {
	$query = $_GET['q'];
}

if ($_REQUEST['start']) {
	$start = $_REQUEST['start'];
} else {
	$start = 0;
}


//Get the XML document loaded into a variable
$xml_url = 'http://www.google.com/search?cx=011542389191668453129%3Afebsf2ygxba&client=google-csbe&output=xml_no_dtd&q=' . urlencode($query);
$xml_url .= '&start=' . $start; // Limit our results
$xml = file_get_contents($xml_url);

//Set up the parser object
$xml_parser = new XMLParser($xml);

//Work the magic...
$xml_parser->Parse();

//echo $xml_parser->GenerateXML();

// How many results did we get?
$total_results = count($xml_parser->document->res[0]->r);

if ($total_results < 1) {
	$search_results .= 'Your search - <strong>' . $query . '</strong> - did not match any documents.<br><br>';

	$search_results .= 'Suggestions:';
	$search_results .= '<ul>';
	$search_results .= '<li>Make sure all words are spelled correctly.</li>';
	$search_results .= '<li>Try different keywords.</li>';
	$search_results .= '<li>Try more general keywords.</li>';
	$search_results .= '</ul>';
} else {
	$search_results .= '<div style="float: right;">Results ' . $xml_parser->document->res[0]->tagAttrs['sn'] . ' - ' . $xml_parser->document->res[0]->tagAttrs['en'] . ' of ' . $xml_parser->document->res[0]->m[0]->tagData . ' for <strong>' . $query . '</strong></div>';

	foreach ($xml_parser->document->res[0]->r as $result) {
		$search_results .= '<a href="' . $result->u[0]->tagData . '">' . str_replace(array(SITE_NAME . ' :: ', '<b', '</b>'), array('', ' <strong>', '</strong> '), $result->t[0]->tagData) . "</a><br />";
		$search_results .= str_replace(array('<b>', '</b>'), array(' <strong>', '</strong> '), $description = $result->s[0]->tagData) . "<br />";
		$search_results .= '<span style="color: #999999; font-size: 10px;">' . (strlen($result->u[0]->tagData) > 100 ? substr(urldecode($result->u[0]->tagData), 0, 100) . "..." : $result->u[0]->tagData) . "</span><br />";
		$search_results .= "<br />";
	}
	
	// Pagination
	if ($xml_parser->document->res[0]->nb[0]->pu) {
		$search_results .= '<div style="float: left;"><a href="' . str_replace("/search", "", $xml_parser->document->res[0]->nb[0]->pu[0]->tagData) . '">&laquo; Previous 10</a></div>';
	}
	
	if ($xml_parser->document->res[0]->nb[0]->nu) {
		$search_results .= '<div style="float: right;"><a href="' . str_replace("/search", "", $xml_parser->document->res[0]->nb[0]->nu[0]->tagData) . '">Next 10 &raquo;</a></div>';
	}
}

$smarty->assign('search_results', $search_results);
?>