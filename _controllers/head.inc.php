<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/scripts/sIFR/sifr.css" />
<script language="javascript" type="text/javascript" src="/scripts/ext-2.2.1/adapter/ext/ext-base.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/ext-2.2.1/ext-core.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/sIFR/sifr.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/sIFR/sifr-config.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/swfobject/swfobject.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/javascripts.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/count.js"></script>
<? if ($_SERVER['PHP_SELF'] == '/about/photos.php' or $_SERVER['PHP_SELF'] == '/philanthropy/photos.php') { ?>
<link rel="stylesheet" type="text/css" href="/scripts/shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="/scripts/shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<? } ?>
<? if ($_SERVER['PHP_SELF'] == '/lists/' or $_SERVER['PHP_SELF'] == '/lists/index.php') { ?>
<script>
function list_redirect() {
var input = document.managelist.list_name.value;
if (input.length > 0) {
var page = 'http://fresco.supportedns.com/mailman/admin/'+document.managelist.list_name.value+'_gps100.com/members/list';
document.location = page;
}
else {
alert("Please enter a list name");
}
}
</script>
<? } ?>
<? if ($_SERVER['PHP_SELF'] == '/portal/profile/profile_edit.php') { ?>
<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple'
	});
</script>					
<? } ?>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/')) { ?>
<script language="javascript" type="text/javascript" src="/scripts/portal.js"></script>
<? } ?>

<!--[if lte IE 6]>
<script type="text/javascript" src="/scripts/supersleight/supersleight-min.js"></script>
<script language="javascript" type="text/javascript">
	Ext.onReady(function() {
		var nodes = Ext.DomQuery.select('#content_container > div');
		if (nodes.length > 0) {
			for(var i = 0; i < nodes.length; i++) {
				Ext.DomHelper.applyStyles(nodes[i], 'padding: 20px;');
			}
		}
		var first_child = Ext.DomQuery.selectNode('#content_container > div:first-child');
		if (first_child) {
			Ext.DomHelper.applyStyles(first_child, 'padding: 0px;');
		}
	});
</script>
<![endif]-->