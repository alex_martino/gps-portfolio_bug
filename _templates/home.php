<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="google-site-verification" content="08XbOJ3unLgivbPHTxHpRZ17GT0gX-8DOf4IhvsFrmk" />
<title><% $page_title %></title>

<% if ($secure_url != 1 && $logged_in == 1) %>
<!-- begin feedback script -->
<script type="text/javascript">
reformal_wdg_w    = "713";
reformal_wdg_h    = "460";
reformal_wdg_domain    = "gpsportal";
reformal_wdg_mode    = 0;
reformal_wdg_title   = "Portal Feedback";
reformal_wdg_ltitle  = "Feedback";
reformal_wdg_lfont   = "";
reformal_wdg_lsize   = "15px";
reformal_wdg_color   = "#FFA000";
reformal_wdg_bcolor  = "#516683";
reformal_wdg_tcolor  = "#FFFFFF";
reformal_wdg_align   = "left";
reformal_wdg_waction = 0;
reformal_wdg_vcolor  = "#9FCE54";
reformal_wdg_cmline  = "#E0E0E0";
reformal_wdg_glcolor  = "#105895";
reformal_wdg_tbcolor  = "#FFFFFF";
 
reformal_wdg_bimage = "bea4c2c8eb82d05891ddd71584881b56.png";
 
</script>

<script type="text/javascript" language="JavaScript" src="http://idea.informer.com/tabn2v4.js?domain=gpsportal"></script><noscript><a href="http://gpsportal.idea.informer.com">GPS Portal feedback</a> <a href="http://idea.informer.com"> Powered by <img src="http://widget.idea.informer.com/tmpl/images/widget_logo.jpg" /></a></noscript>
<! -- end feedback script -->
<% /if %>

<link rel="stylesheet" type="text/css" href="/scripts/ext-2.0/resources/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/css/styles.css" />
<% include_php file="$document_root/includes/head.inc.php" %>
</head>
<body>
<div id="main_container">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="250"><div style="margin-left: 14px;"><a href="/"><img src="/images/logo.gif" width="233" height="64" alt="Global Platinum Security" /></a></div></td>
		<td>
			<% include_php file="$document_root/includes/login_box.inc.php" %>
			<!-- REMOVED SEARCH AS NO LONGER SUBSCRIBED <% include_php file="$document_root/includes/search_box.inc.php" %> -->
		</td>
		</tr>
		</table>
		<% include_php file="$document_root/includes/top_menu.inc.php" %>
		<% include file="$page_file" %>
	</td>
	<td style="border-left: 1px solid #FFFFFF; width: 50px;"><img src="/images/side_background.gif" width="50" height="465" alt="" /></td>
	</tr>
	</table>
	<div id="footer_container">
		<% include_php file="$document_root/includes/footer.inc.php" %>
	</div>
</div>
<% include_php file="$document_root/includes/google_analytics.inc.php" %>
</body>
</html>