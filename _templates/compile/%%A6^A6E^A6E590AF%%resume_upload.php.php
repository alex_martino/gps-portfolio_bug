<?php /* Smarty version 2.6.17, created on 2012-10-20 16:33:11
         compiled from /home/gpscom/public_html/_pages/portal/toolbox/resume_upload.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/toolbox/resume_upload.php', 31, false),)), $this); ?>
<div><img src="/content_files/headers/portal.gif" width="800" height="90"></div>
<div>
<h2>Resume Upload</h2>
<p>Resumes can be uploaded in PDF format only. Only board members and VP Personnel can access resumes.</p>
    <link href="/scripts/uploadify-2.1.14/uploadify.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="/scripts/uploadify-2.1.14/swfobject.js"></script>
    <script type="text/javascript" src="/scripts/uploadify-2.1.14/jquery.uploadify.v2.1.4.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('#file_upload').uploadify({
        'uploader'  : '/scripts/uploadify-2.1.14/uploadify.swf',
        'script'    : '/scripts/uploadify-2.1.14/resume_upload.php',
        'cancelImg' : '/scripts/uploadify-2.1.14/cancel.png',
        'auto'      : true,
	'fileExt'   : '*.pdf',
	'fileDesc'  : 'Adobe PDF File (.PDF)',
	'buttonText'  : 'Upload Resume',
	'scriptData'  : {'uid':'<?php echo $_SESSION['user_id']; ?>
'},
	'sizeLimit' : 1048576,
	'onComplete': function(e, q, f, r, d) { window.location = 'resume_upload.php'; }
      });
    });
    </script>
<BR>
<?php if ($this->_tpl_vars['user_has_resume']): ?>
<h2>Your Resume</h2>
<p>
<?php if ($this->_tpl_vars['resume_has_problem']): ?>
<b>There is a problem with your resume :(</b> - please try using another method to convert it to a pdf... <a href="#" onClick="alert('We use a homemade program to allow us to take all the pdfs which are uploaded by our members and merge them into one pdf document. Occassionally the method used to convert the document into a pdf causes a problem - usually when a less common compression method is used.\n\nPerhaps send your resume to a friend and ask them to convert it to a pdf for you? Or try using a different program to do the conversion...'); return false;">Find out more</a>
<?php else: ?>
<a href="resume_upload.php?download=1&id=<?php echo $_SESSION['user_id']; ?>
"><?php echo $_SESSION['user_first_name']; ?>
 <?php echo $_SESSION['user_last_name']; ?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['resume_upload_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
).pdf</a>
&nbsp;<a href="resume_upload.php?delete=1&id=<?php echo $_SESSION['user_id']; ?>
" onClick="return confirm('Are you sure you wish to delete this resume?');"><img src="/content_files/images/delete.png" style="vertical-align: bottom"></a>
<?php endif; ?>
</p>
<BR>
<h2>Update Resume</h2>
<?php else: ?>
<h2>Add Your Resume</h2>
<p>You have not yet added your resume, click the button below to get started.</p>
<?php endif; ?>
<form id="upload_form" action="/scripts/uploadify-2.1.14/resume_upload.php" method="post" enctype="multipart/form-data">
    <input id="file_upload" name="file_upload" type="file" />
</form>


</div>