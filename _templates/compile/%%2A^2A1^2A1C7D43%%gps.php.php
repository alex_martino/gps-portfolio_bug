<?php /* Smarty version 2.6.17, created on 2012-10-16 16:23:53
         compiled from /home/gpscom/public_html/_pages/portal/network/view/gps.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/network/view/gps.php', 23, false),array('modifier', 'upper', '/home/gpscom/public_html/_pages/portal/network/view/gps.php', 41, false),array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/network/view/gps.php', 267, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90">

<?php if ($this->_tpl_vars['not_viewable']): ?>
<div style="margin-top: 15px; margin-left: 10px">
<p>This profile is not available. <a href="javascript:history.go(-1)">Go back.</a></p>

<?php else: ?>
<div id="network_t_menu">
  <ul>
<?php $_from = $this->_tpl_vars['network_t_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <li<?php if ($this->_tpl_vars['i']['current'] == '1'): ?> id="current"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['i']['url']; ?>
?id=<?php echo $this->_tpl_vars['profile']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</a></li>
<?php endforeach; endif; unset($_from); ?>
  </ul>
</div>
</div>
<?php if ($this->_tpl_vars['is_editing']): ?>
<script>
			window.addEvent('load', function(){
				
				// Autocomplete initialization
				var t1 = new TextboxList('sectors', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['sector']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t1.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t1.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_sectors.php', onSuccess: function(r){
					t1.plugins['autocomplete'].setValues(r);
					t1.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t2 = new TextboxList('pitches', {unique: true, plugins: {autocomplete: {}}});
<?php $_from = $this->_tpl_vars['tags']['pitch']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t2.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t2.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_pitches.php', onSuccess: function(r){
					t2.plugins['autocomplete'].setValues(r);
					t2.container.removeClass('textboxlist-loading');
				}}).send();	

				// Autocomplete initialization
				var t3 = new TextboxList('mentor', {unique: true, max: 1, plugins: {autocomplete: {}}});
<?php if ($this->_tpl_vars['profile']['alumni_mentor_id']): ?>
t3.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['alumni_mentor'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endif; ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t3.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_members.php', onSuccess: function(r){
					t3.plugins['autocomplete'].setValues(r);
					t3.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t4 = new TextboxList('advocate', {unique: true, max: 1, plugins: {autocomplete: {}}});
<?php if ($this->_tpl_vars['profile']['member_advocate_id']): ?>
t4.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['member_advocate'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endif; ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response examaple
				t4.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_members.php', onSuccess: function(r){
					t4.plugins['autocomplete'].setValues(r);
					t4.container.removeClass('textboxlist-loading');
				}}).send();

				// Autocomplete initialization
				var t5 = new TextboxList('expertise', {unique: true, plugins: {autocomplete: {}}});

<?php $_from = $this->_tpl_vars['tags']['expertise']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
t5.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
');
<?php endforeach; endif; unset($_from); ?>
				
				// sample data loading with json, but can be jsonp, local, etc. 
				// the only requirement is that you call setValues with an array of this format:
				// [
				//	[id, bit_plaintext (on which search is performed), bit_html (optional, otherwise plain_text is used), autocomplete_html (html for the item displayed in the autocomplete suggestions dropdown)]
				// ]
				// read autocomplete.php for a JSON response example
				t5.container.addClass('textboxlist-loading');				
				new Request.JSON({url: '/includes/autocomplete_expertise.php', onSuccess: function(r){
					t5.plugins['autocomplete'].setValues(r);
					t5.container.removeClass('textboxlist-loading');
					document.getElementById('savelink').innerHTML = '<a href="#" onClick="document.do_edit.submit(); return false;">Save</a>';
				}}).send();
			
								
			});


</script>
<?php endif; ?>
<div style="margin-top: 25px">

<?php if ($this->_tpl_vars['can_edit']): ?>
<div class="editlink">
<?php if ($this->_tpl_vars['is_editing']): ?>
<span id="savelink">Please wait...</span> |
<a href="gps.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
">Cancel</a>
<?php else: ?>
<a href="gps.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1">Edit</a>
<?php endif; ?>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
</h2>

<div style="float:left" style="width: 150px">

<?php if ($this->_tpl_vars['display_photo']): ?>

<img name="profile_img" src="<?php echo $this->_tpl_vars['profile']['current_photo']; ?>
" width="150" alt="Photo of <?php echo $this->_tpl_vars['profile']['first_name']; ?>
 <?php echo $this->_tpl_vars['profile']['last_name']; ?>
"
<?php if ($this->_tpl_vars['profile']['school_photo']): ?>
<?php endif; ?>
/>

<?php else: ?>

<img src="/images/male.png" width="150" alt="Photo missing" />

<?php endif; ?>
</div>

<div style="float: right; width: 610px">

<?php if ($this->_tpl_vars['error']): ?>
<div class="error"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>

<?php if (( $this->_tpl_vars['is_editing'] )): ?>

<form name="do_edit" action="gps.php?id=<?php echo $this->_tpl_vars['profile_id']; ?>
&edit=1" method="POST" accept-charset="utf-8">
<input type="hidden" name="submitted" value="1">

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Sectors</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="sectors" value="" id="sectors" />
<BR>
		<p class="note">Type the tag (one or more words) and press enter. Use left/right arrows, backspace, delete to navigate/remove boxes, and up/down/enter to navigate/add suggestions.</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Pitches</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="pitches" value="" id="pitches" />
<BR>
		<p class="note">Enter the <b>1 - 4 letter</b> stock market ticker</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Research Experience</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="expertise" value="" id="expertise" />
<BR>
		<p class="note">Please enter any research areas you have been exposed to. E.g. "Insurance". You can include experience gained during employment.</p>
</div>
</div>

<BR><BR>


		</fieldset>
</div>
<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Alumni Mentor</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="mentor" value="" id="mentor" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

<div class="network_data" style="width: 580px">
		<fieldset>
		<legend>Member Advocate</legend>

<div style="float:left">
		<div class="form_friends" style="float:left; margin-left: 15px">
			<input type="text" name="advocate" value="" id="advocate" />
</div>
</div>

<BR><BR>


		</fieldset>
</div>

</form>

		
<?php else: ?>

<div class="network_data">

<div id="title">
<div style="float:left" class="data_title">
Title
</div>
<div style="float:left" class="data_data">
<?php echo $this->_tpl_vars['profile']['title']; ?>

</div>
</div>
<BR><BR>
<?php if ($this->_tpl_vars['tags']['sector']): ?>
<div id="sectors" class="network_block_top">
<div style="float:left" class="data_title">
Sectors
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['sector']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<span class="button blue small"><a href="../search.php?qs=1&ttype=sector&tag=<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('replace', true, $_tmp, "&", "%26") : smarty_modifier_replace($_tmp, "&", "%26")); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['pitch']): ?>
<div id="pitches" class="network_block">
<div style="float:left" class="data_title">
Pitches
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['pitch']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button green small"><a href="../search.php?qs=1&ttype=pitch&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['tags']['expertise']): ?>
<div id="expertise" class="network_block">
<div style="float:left" class="data_title">
Research Experience
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['tags']['expertise']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ttype'] => $this->_tpl_vars['i']):
?>
<span class="button red small"><a href="../search.php?qs=1&ttype=expertise&tag=<?php echo $this->_tpl_vars['i']['tag']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['tag'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</a></span>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>
<BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['profile']['alumni_mentor_id']): ?>
<div id="mentor" class="network_block">
<div style="float:left" class="data_title">
Alumni Mentor
</div>
<div style="float:left" class="data_data">
<a href="overview.php?id=<?php echo $this->_tpl_vars['profile']['alumni_mentor_id']; ?>
"><?php echo $this->_tpl_vars['profile']['alumni_mentor']; ?>
</a><BR>
</div>
</div>
<BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['profile']['member_advocate_id']): ?>
<div id="Advocate" class="network_block">
<div style="float:left" class="data_title">
Member Advocate
</div>
<div style="float:left" class="data_data">
<a href="overview.php?id=<?php echo $this->_tpl_vars['profile']['member_advocate_id']; ?>
"><?php echo $this->_tpl_vars['profile']['member_advocate']; ?>
</a><BR>
</div>
</div>
<BR><BR>
<?php endif; ?>
<?php if ($this->_tpl_vars['profile']['mentees']): ?>
<div id="Mentees" class="network_block">
<div style="float:left" class="data_title">
Mentees
</div>
<div style="float:left" class="data_data">
<?php $_from = $this->_tpl_vars['profile']['mentees']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<a href="overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</a>&nbsp;
<?php endforeach; endif; unset($_from); ?>
<BR><BR>
</div>
</div>
<?php endif; ?>

</div>

<?php endif; ?>

</div> <!--end float right -->

<div style="clear:both">&nbsp;</div>

<?php endif; ?>

</div>