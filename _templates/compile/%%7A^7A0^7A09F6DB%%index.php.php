<?php /* Smarty version 2.6.17, created on 2012-10-20 16:35:02
         compiled from /home/gpscom/public_html/_pages/portal/transcripts/index.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/transcripts/index.php', 34, false),array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/transcripts/index.php', 41, false),array('modifier', 'truncate', '/home/gpscom/public_html/_pages/portal/transcripts/index.php', 73, false),)), $this); ?>
<div><img src="/content_files/headers/transcripts.gif" width="800" height="90"></div>
<div>
<?php if (! $this->_tpl_vars['sector']): ?>
<h2>Introduction</h2>
<p>Browse by sector using the menu on the left or search using the feature below.</p>
<BR>
<h2>Search</h2>
<form name="transcript_search" action="index.php?ts=1" method="post">
<div style="float:left; margin-right: 5px">
<div style="height: 22px">Search terms:</div>
<div style="height: 22px">Sector:</div>
<div style="height: 22px">
<span class="button default strong"><input type="button" onClick="document.transcript_search.submit()" value="Search Database"></span>
</div>
</div>
<div style="float:left">
<div style="height: 22px">
<input type="text" name="searchbox" value="<?php echo $_POST['searchbox']; ?>
" style="width: 200px">
</div>
<div style="height: 22px">
<select name="filter_sector">
<option value="0">* All Sectors *</option>
<?php $_from = $this->_tpl_vars['sectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['sname']; ?>
"<?php if ($_POST['filter_sector'] == $this->_tpl_vars['i']['sname']): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['sname']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
</div>
</div>
</form>
<div style="clear:both">&nbsp;</div><BR>
<?php elseif (! $this->_tpl_vars['date']): ?>

<?php if (( $this->_tpl_vars['sector'] != 'gm' )): ?>
<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['sector'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h2>
<?php else: ?>
<h2>GM</h2>
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['dates'] )): ?>
<?php $_from = $this->_tpl_vars['dates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<a href="index.php?sector=<?php echo $this->_tpl_vars['sector']; ?>
&date=<?php echo $this->_tpl_vars['i']['date']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%B %e, %Y") : smarty_modifier_date_format($_tmp, "%B %e, %Y")); ?>
</a><BR>
<?php endforeach; endif; unset($_from); ?>
<?php else: ?>
<p>No transcripts have been stored yet.</p>
<?php endif; ?>

<?php else: ?>

<?php if (( $this->_tpl_vars['sector'] != 'gm' )): ?>
<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['sector'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
: <?php echo ((is_array($_tmp=$this->_tpl_vars['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%A, %B %e, %Y") : smarty_modifier_date_format($_tmp, "%A, %B %e, %Y")); ?>
</h2>
<?php else: ?>
<h2>GM: <?php echo ((is_array($_tmp=$this->_tpl_vars['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%A, %B %e, %Y") : smarty_modifier_date_format($_tmp, "%A, %B %e, %Y")); ?>
</h2>
<?php endif; ?>

<?php if ($_GET['message_id'] > 0): ?>
<a href="javascript:history.go(-1);">Return to Search Results</a><BR><BR>
<?php endif; ?>

<div style="width: 700px">
<table class="admin_table_2">
<tr style="border-bottom: 1px solid black">
<th style="width:30px">Time</th>
<th style="width:100px">Member</th>
<th>Message</th>
</tr>
<?php $_from = $this->_tpl_vars['messages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<?php if ($_GET['message_id'] == $this->_tpl_vars['i']['id']): ?>
<tr style="background:yellow">
<?php else: ?>
<tr>
<?php endif; ?>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['timestamp'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%H:%M") : smarty_modifier_date_format($_tmp, "%H:%M")); ?>
</td>
<td><a href="/portal/network/view/overview.php?id=<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['userid'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "..", 'TRUE') : smarty_modifier_truncate($_tmp, 20, "..", 'TRUE')); ?>
" name="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['username']; ?>
</a></td>
<td><?php echo $this->_tpl_vars['i']['message']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>
<?php endif; ?>

<?php if ($_GET['ts']): ?>
<h2>Search Results: "<?php echo $this->_tpl_vars['s_query']; ?>
"</h2>

<script>
function show_message(id) {
window.location="index.php?show_message="+id;
}
</script>

<?php if ($this->_tpl_vars['error']): ?>
<?php echo $this->_tpl_vars['errormsg']; ?>

<?php else: ?>

<?php if ($this->_tpl_vars['results_count'] > 0): ?>

<?php echo $this->_tpl_vars['pagination']; ?>


<table class="admin_table" style="margin-top: 15px">
<tr style="border-bottom: 1px solid black">
<th style="width: 50px">Date</th>
<th>Sector</th>
<th style="width: 100px">User</th>
<th>Message</th>
</tr>
<?php $_from = $this->_tpl_vars['results_r']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row" onClick="show_message(<?php echo $this->_tpl_vars['i']['id']; ?>
);">
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['sent'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D") : smarty_modifier_date_format($_tmp, "%D")); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['sector']; ?>
</td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['username'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, '..', true, true) : smarty_modifier_truncate($_tmp, 20, '..', true, true)); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['message']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

<?php else: ?>

No results found for "<?php echo $this->_tpl_vars['s_query']; ?>
"

<?php endif; ?>

<?php endif; ?>
<?php endif; ?>

</div>