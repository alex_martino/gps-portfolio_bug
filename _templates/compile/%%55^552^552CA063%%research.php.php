<?php /* Smarty version 2.6.17, created on 2012-10-21 11:07:11
         compiled from /home/gpscom/public_html/_pages/portal/research/research.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/research/research.php', 58, false),)), $this); ?>
<div><img src="/content_files/headers/research.gif" width="800" height="90"></div>
<div>

<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple',
		editor_selector: 'mceEditor'
	});
</script>
<?php if ($this->_tpl_vars['research_item'] && ! $_GET['edit'] && $this->_tpl_vars['item_details']['deleted'] != 1): ?>
<div class="editlink">
<a href="research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $_GET['item']; ?>
&edit=1">Edit</a>
<?php if ($this->_tpl_vars['can_delete']): ?>
| <a href="research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $_GET['item']; ?>
&delete=1" onClick="return confirm('Are you sure you want to delete this company?');">Delete</a>
<?php endif; ?>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] && ! $_GET['edit'] && $this->_tpl_vars['item_details']['deleted'] == 1 && $this->_tpl_vars['can_restore'] == 1): ?>
<div class="editlink">
<a href="research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $_GET['item']; ?>
&restore=1">Restore</a>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] && $_GET['edit'] && $this->_tpl_vars['item_details']['deleted'] != 1): ?>
<div class="editlink">
<a href="#" onClick="document.edit_item.submit(); return false;">Save</a> | <a href="research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $_GET['item']; ?>
">Cancel</a>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['research_cat']; ?>
</h2>

<?php if ($this->_tpl_vars['sectorid'] && ! $this->_tpl_vars['research_item']): ?><p><?php echo $this->_tpl_vars['research_description']; ?>
</p>

<BR><h2>Current Pipeline</h2>

<div style="width:100%; text-align: center; margin: 10px 0">
<span class="button default strong"><input type="button" value="Add Company" class="submit" onClick="showform(); document.addform.type.value = 'pipeline'; update_explain(); document.addform.sector.value = '<?php echo $this->_tpl_vars['sectorid']; ?>
'; document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_pipeline').innerHTML; document.getElementById('userlabel').innerHTML = 'Person Researching'; return false;" /></span>
</div>

<?php if ($this->_tpl_vars['display_pipeline']): ?>

<center>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Person Researching</th>
</tr>

<?php $_from = $this->_tpl_vars['pipeline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row" onClick="window.location.href='research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $this->_tpl_vars['i']['id']; ?>
';">
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['entry_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%y") : smarty_modifier_date_format($_tmp, "%m/%d/%y")); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['company_name']; ?>

<td><?php echo $this->_tpl_vars['i']['ticker']; ?>

<td><?php echo $this->_tpl_vars['i']['sname']; ?>

<td><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>

</table>
</center>

<?php else: ?>
<p>There is no research currently in the pipeline in this sector.</p>

<?php endif; ?>

<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] < 1 && $this->_tpl_vars['display_wishlist'] == 1): ?>

<?php if ($this->_tpl_vars['sectorid']): ?><BR><h2>PM's Wishlist</h2><?php endif; ?>

<div style="width:100%; text-align: center; margin: 10px 0">
<span class="button default strong"><input type="button" value="Add Company" class="submit" onClick="showform(); document.addform.type.value = 'wishlist'; update_explain(); document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML; document.getElementById('userlabel').innerHTML = 'User'; document.addform.sector.value = '<?php echo $this->_tpl_vars['sectorid']; ?>
'; return false;" /></span>
</div>

<center>
<table class="admin_table" style="border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 100px">Entry Date</th>
<th style="width: 250px">Name</th>
<th style="width: 85px">Ticker</th>
<th style="width: 100px">Sector</th>
<th style="width: 200px">Suggested By</th>
</tr>

<?php $_from = $this->_tpl_vars['wishlist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row" onClick="window.location.href='research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $this->_tpl_vars['i']['id']; ?>
';">
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['entry_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%y") : smarty_modifier_date_format($_tmp, "%m/%d/%y")); ?>
</td>
<td><?php echo $this->_tpl_vars['i']['company_name']; ?>

<td><?php echo $this->_tpl_vars['i']['ticker']; ?>

<td><?php echo $this->_tpl_vars['i']['sname']; ?>

<td><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>

</table>
</center>
</div>

<?php else: ?>

<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] && $this->_tpl_vars['item_details']['deleted'] != 1 && $_GET['edit'] != 1): ?>
<div style="width: 570px; float: left">
<b>Description</b><BR>
<?php echo $this->_tpl_vars['item_details']['description']; ?>

<BR><BR>
<b>Why Interesting?</b><BR>
<?php echo $this->_tpl_vars['item_details']['why_interesting']; ?>

<BR><BR>
<b>Other Information</b><BR>
<?php echo $this->_tpl_vars['item_details']['other_information']; ?>

</div>
<div style="width: 171px; float: left; border: 1px dotted black; margin-left: 10px; padding: 2px">
<div style="text-align: center; margin-bottom: 3px"><b>Financial Data</b></div>
<div style="width: 90px; float: left; margin-left: 5px">
Entry Price:<BR>
Current Price:<BR>
Price Change:
</div>
<div style="width: 76px; float: left">
$<?php echo $this->_tpl_vars['item_details']['entry_price']; ?>
<BR>
$<?php echo $this->_tpl_vars['item_details']['current_price']; ?>
<BR>
<?php echo $this->_tpl_vars['item_details']['price_change']; ?>
%<BR>
</div>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] && $this->_tpl_vars['item_details']['deleted'] != 1 && $_GET['edit'] == 1): ?>
<form name="edit_item" action="research.php?sector=<?php echo $_GET['sector']; ?>
&item=<?php echo $_GET['item']; ?>
&edit_form=1" method="POST">

<div class="network_data" style="margin-left: 0">

		<fieldset>
		<legend>Description</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="description" name="description" style="width: 705px" class="mceEditor">
<?php echo $this->_tpl_vars['item_details']['description']; ?>

</textarea>
</div>

<BR><BR>

</fieldset>

		<fieldset>
		<legend>Why is it interesting?</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="why_interesting" name="why_interesting" style="width: 705px" class="mceEditor">
<?php echo $this->_tpl_vars['item_details']['why_interesting']; ?>

</textarea>
</div>

<BR><BR>

</fieldset>


		<fieldset>
		<legend>Other Information</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<textarea id="other_information" name="other_information" style="width: 705px" class="mceEditor">
<?php echo $this->_tpl_vars['item_details']['other_information']; ?>

</textarea>
</div>

<BR><BR>

</fieldset>


		<fieldset>
		<legend>Ticker</legend>

<div style="width: 735px">
		<div class="form_friends" style="float: left; margin-left: 15px">
<input type="text" name="ticker" value="<?php echo $this->_tpl_vars['item_details']['ticker']; ?>
" style="width: 80px" maxlength=8>
</div>

<BR><BR>

</fieldset>

</form>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['research_item'] && $this->_tpl_vars['item_details']['deleted'] == 1): ?>
<p>This item has been deleted. If you would like it restored please contact an admin.</p>
<?php endif; ?>

<div style="clear:both">&nbsp;</div>
    <div id="page_screen">

        &nbsp;
    </div>

<style>
.data_title_r { width: 130px }
</style>
<div id="addform" style="display:none; border-radius:15px; -moz-border-radius: 15px; height: 640px">
<?php if ($this->_tpl_vars['error']): ?>
<div class="error" id="error" style="text-align: center; width: 100%; margin-top: 5px"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<?php endif; ?>
<form name="addform" action="research.php?wishlist=<?php echo $_GET['wishlist']; ?>
&sector=<?php echo $_GET['sector']; ?>
&addcompany=1" method="POST">
<div class="network_block_edit">
<div class="dataform">
<div class="data_title_r">
Company Name<BR>
Sector<BR>
Ticker
</div>
<div style="float:left" class="data_data_edit">
<input type="text" name="company_name" class="edittextw" value="<?php echo $_POST['company_name']; ?>
" MAXLENGTH=255><BR>
<select name="sector" style="margin-bottom: 3px">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['sectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['fname']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
<BR>
<input type="text" name="ticker" value="<?php echo $_POST['ticker']; ?>
" style="width: 60px; margin-bottom: 10px" MAXLENGTH=8>
<span style="font-size:11px">I.e. This should match the ticker used by Google Finance</span>
</div>
<BR>
<hr class="editline">
<div class="data_title_r">
Description
</div>
<div style="float:left" class="data_data_edit">
<textarea style="width: 420px" name="description" class="mceEditor"><?php echo $_POST['description']; ?>

</textarea>
</div>
<div class="data_title_r">
Why is it interesting?
</div>
<div style="float:left" class="data_data_edit">
<textarea style="width: 420px" name="why_interesting" class="mceEditor"><?php echo $_POST['why_interesting']; ?>

</textarea>
</div>
<div class="data_title_r">
Other Information
</div>
<div style="float:left; margin-bottom: 10px" class="data_data_edit">
<textarea style="width: 420px" name="other_information" class="mceEditor"><?php echo $_POST['other_information']; ?>

</textarea>
</div>
<hr class="editline">
<div class="data_title_r">
Type<BR>
<div style="padding-top: 3px" id="userlabel">Person Researching</div>
</div>
<div style="float:left" class="data_data_edit">
<select name="type" onChange="update_explain();" style="margin-bottom: 3px">
<option value="pipeline">Pipeline</option>
<option value="wishlist">Wishlist</option>
</select>
<span id="type_explain" style="font-size:11px">I.e. This is a suggestion for future research</span>
<BR>
<div id="userdiv">
<select name="user" style="margin-bottom: 5px">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
</div>
</div>
<hr class="editline">
<div style="float:left;"><input type="button" value="Cancel" onClick="hideform();"></div>
<div style="float:right"><input type="submit" value="Save"></div>
</div>

</div>
</form>
</div>

<div id="userdiv_wishlist" style="display:none">
<input type="text" name="user" value="<?php echo $_SESSION['user_first_name']; ?>
 <?php echo $_SESSION['user_last_name']; ?>
" readonly style="border:0; background: #ededed; margin-top: 2px">
</div>

<div id="userdiv_pipeline" style="display:none">
<select name="user" style="margin-bottom: 5px" id="userdrop2">
<option value="0">** Please Select **</option>
<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"<?php if ($this->_tpl_vars['i']['id'] == $_SESSION['user_id']): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
</div>

<script>
document.addform.user.value = "<?php echo $_SESSION['user_id']; ?>
";
document.getElementById('userdrop2').value = "<?php echo $_SESSION['user_id']; ?>
";
<?php if (! $this->_tpl_vars['sectorid']): ?>
document.addform.type.value = "wishlist";
document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML;
<?php endif; ?>
<?php if ($_POST['sector']): ?>
document.addform.sector.value = '<?php echo $_POST['sector']; ?>
';
<?php endif; ?>
<?php if ($_POST['user']): ?>
document.addform.user.value = "<?php echo $_POST['user']; ?>
";
document.getElementById('userdrop2').value = "<?php echo $_POST['user']; ?>
";
<?php endif; ?>
<?php if ($_POST['type'] == 'wishlist'): ?>
document.addform.type.value = "wishlist"; document.getElementById('userdiv').innerHTML = document.getElementById('userdiv_wishlist').innerHTML; document.getElementById('userlabel').innerHTML = "User";
<?php endif; ?>
<?php if ($this->_tpl_vars['error']): ?>
showform();
<?php endif; ?>
update_explain();
</script>

</div>