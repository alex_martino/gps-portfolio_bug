<?php /* Smarty version 2.6.17, created on 2012-10-16 11:17:24
         compiled from /home/gpscom/public_html/_pages/press/index.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/press/index.php', 5, false),array('modifier', 'strip_tags', '/home/gpscom/public_html/_pages/press/index.php', 6, false),array('modifier', 'truncate', '/home/gpscom/public_html/_pages/press/index.php', 6, false),)), $this); ?>
<div><img src="/content_files/headers/press.gif" width="800" height="90"></div>
<div>
	<?php if ($this->_tpl_vars['press_releases'] != false): ?>
		<?php $_from = $this->_tpl_vars['press_releases']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['press_release']):
?>
			<p><a href="/press/article.php?id=<?php echo $this->_tpl_vars['press_release']['id']; ?>
"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['press_release']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")); ?>
 - <?php echo $this->_tpl_vars['press_release']['title']; ?>
</strong></a><br />
			<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['press_release']['body'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 300) : smarty_modifier_truncate($_tmp, 300)); ?>
</p>
		<?php endforeach; endif; unset($_from); ?>
	<?php else: ?>
		<p>There are no press releases at this time. Please check back soon.</p>
	<?php endif; ?>
</div>