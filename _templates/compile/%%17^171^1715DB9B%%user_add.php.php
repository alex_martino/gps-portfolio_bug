<?php /* Smarty version 2.6.17, created on 2012-12-05 10:49:20
         compiled from /home/gpscom/public_html/_pages/portal/admin/user_add.php */ ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>

<?php if ($this->_tpl_vars['add_success'] == 1): ?>

Congratulations! New User Added.<BR><BR>

<a href="user_add.php">Add another</a>

<?php else: ?>

<p>Fill out this form to add a user:</p>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>
<form name="add_user" method="POST" action="user_add.php">
<div style="float:left; width: 85px; line-height: 1.6">
First Name:<BR>
Last Name:<BR>
Email:<BR>
Gender:<BR>
Year:<BR>
Status:<BR>
University:<BR><BR>
<input type="submit" value="Make User">
</div>
<div style="float:left; width: 500px; line-height: 1.6">
<input type="text" name="first_name" id="first_name" value="<?php echo $this->_tpl_vars['fname']; ?>
"><BR>
<input type="text" name="last_name" id="last_name" value="<?php echo $this->_tpl_vars['sname']; ?>
"><BR>
<input type="text" name="email" id="email" value="<?php echo $this->_tpl_vars['email']; ?>
" style="width: 180px"> (I.e. tshannon@gps100.com)<BR>
<select name="gender">
<option value="1">Male</option>
<option value="0">Female</option>
</select><BR>
<select name="year">
<?php $_from = $this->_tpl_vars['years']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']; ?>
" <?php if (( $_POST['year'] == $this->_tpl_vars['i'] )): ?> SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select><BR>
<select name="status" id="status">
<option value="analyst" <?php if ($this->_tpl_vars['status'] == 'analyst'): ?>SELECTED<?php endif; ?>>Analyst</option>
<option value="member" <?php if ($this->_tpl_vars['status'] == 'member'): ?>SELECTED<?php endif; ?>>Member</option>
<option value="alumni" <?php if ($this->_tpl_vars['status'] == 'alumni'): ?>SELECTED<?php endif; ?>>Alumni</option>
</select><BR>
<select name="university" id="university">
<?php $_from = $this->_tpl_vars['rowdetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
<option value="<?php echo $this->_tpl_vars['i']['uni_id']; ?>
" <?php if ($this->_tpl_vars['i']['selected'] == 1): ?>SELECTED<?php endif; ?>><?php echo $this->_tpl_vars['i']['uni_sname']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>
</div>
<input type="hidden" name="submitted" value="1">
</form>

<?php endif; ?>

</div>