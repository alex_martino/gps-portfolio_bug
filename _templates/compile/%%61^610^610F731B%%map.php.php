<?php /* Smarty version 2.6.17, created on 2012-10-16 16:17:31
         compiled from /home/gpscom/public_html/_pages/portal/network/map.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', '/home/gpscom/public_html/_pages/portal/network/map.php', 15, false),array('modifier', 'count', '/home/gpscom/public_html/_pages/portal/network/map.php', 51, false),)), $this); ?>
<div><img src="/content_files/headers/network.gif" width="800" height="90"></div>
<div>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>

		<div id="map" style="width: 760px; height: 400px;"></div>
  <style>
	.map_box { float: left; margin: 0 5px 0 0; }
	.map_container a { text-decoration: none }
  </style>
  <script type="text/javascript">
    var locations = [
	<?php $_from = $this->_tpl_vars['members_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['addresses'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['addresses']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['addresses']['iteration']++;
?>
		['<div class="map_container"><div class="map_box"><img src="/content_files/member_photos/<?php echo $this->_tpl_vars['i']['id']; ?>
-cropped.jpg"></div><b><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</b><BR><?php echo $this->_tpl_vars['i']['undergraduate_school_s']; ?>
 <?php echo $this->_tpl_vars['i']['graduation_year']; ?>
<div style="clear: both; text-align: center; padding: 12px 0 0 0"><a href="mailto: <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['email_address'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'quotes') : smarty_modifier_escape($_tmp, 'quotes')); ?>
">Email</a><?php if ($this->_tpl_vars['i']['contact_number']): ?> | <a href="tel:+<?php echo $this->_tpl_vars['i']['contact_number_country_code']; ?>
<?php echo $this->_tpl_vars['i']['contact_number']; ?>
">Call</a><?php endif; ?> | <a href="/portal/network/view/contact.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
">Address</a></div></div>', <?php echo $this->_tpl_vars['i']['address_lat']; ?>
, <?php echo $this->_tpl_vars['i']['address_long']; ?>
]
		<?php if (! ($this->_foreach['addresses']['iteration'] == $this->_foreach['addresses']['total'])): ?>, <?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: new google.maps.LatLng(40.43, -74.0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
	function share_address() {
		window.location = "map.php?share_location=1";
	}
	function check_address() {
		return true;
	}
  </script>
	<div style="float: right; margin-top: 5px">
		<i>Plotted: <?php echo count($this->_tpl_vars['members_array']); ?>
 awesome people!</i>
	</div>
  <BR>
  <b>Your Address</b><BR>
  <form name="address_form" action="map.php" method="POST" onSubmit="check_address();">
  <?php if ($this->_tpl_vars['user_details']['address'] == ''): ?>
	<div style="float: right; width: 240px; border: 1px dotted red; padding: 10px">
		<b style="color: red">Small problem...</b><BR>You haven't entered your address.<BR><BR>
	</div>
  <?php elseif ($this->_tpl_vars['address_not_found']): ?>
	<div style="float: right; width: 240px; border: 1px dotted red; padding: 10px">
		<b style="color: red">Google couldn't find you!</b><BR>Your address was unreadable by Google!
	</div>
  <?php endif; ?>
  <?php if ($this->_tpl_vars['user_details']['address_private']): ?>
    <?php endif; ?>
  <div style="float: left">
	<textarea name="address" style="width: 200px; height: 70px; margin: 5px 0; resize: none"><?php echo $this->_tpl_vars['user_details']['address']; ?>
</textarea><BR>
	<span class="button default strong"><input type="submit" value="Save address"></span>
  </div>
  <div style="clear:both">&nbsp;</div>
  </form>

</div>