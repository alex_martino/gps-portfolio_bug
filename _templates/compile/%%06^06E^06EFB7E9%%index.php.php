<?php /* Smarty version 2.6.17, created on 2012-11-04 12:07:51
         compiled from /home/gpscom/public_html/_pages/portal/admin/index.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/admin/index.php', 547, false),array('modifier', 'truncate', '/home/gpscom/public_html/_pages/portal/admin/index.php', 609, false),array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/admin/index.php', 637, false),array('modifier', 'file_size', '/home/gpscom/public_html/_pages/portal/admin/index.php', 791, false),)), $this); ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90">

<style>
.d_table td { padding-right: 8px }
.cp_menu { height: 20px; background: grey; border-right: 1px dotted black; border-left: 1px dotted black }
.cp_option { float: left; padding: 2px 7px; color: white; border-right: 1px dotted black; height: 16px }
.cp_option_cur { float: left; padding: 2px 7px; color: black; border-right: 1px dotted black; background: #ededed; height: 16px }
.cp_option a { color: white }
.cp_option_cur a { color: black }
.cp_option a, .cp_option_cur a { font-weight: normal; text-decoration: none }
.cp_option a:hover, .cp_option_cur a:hover { text-decoration: underline }
#successbar { background: #95B9C7; color: black; padding: 3px; margin: 2px 0 0 0 }
.progress { width: 380px; height: 85px; overflow: none; border: none }
</style>
<script>
//Define variables

//counts
var count = new Array();
count['docs'] = 4;
count['payments'] = 4;

function switch_tab(item) {
item = item.split('_');
item_name = item[0];
item_num = item[1];
id_current = item_name + "_" + item_num + "_menu";
new_content = document.getElementById(item_name + "_" + item_num + "_content").innerHTML;
content_div = item_name + "_content";

//loop through each tab
for (x=1;x<=count[item_name];x=x+1) {
id_menu = item_name + "_" + x + "_menu";
document.getElementById(id_menu).setAttribute("class", "cp_option");
}

//Edit current tab
document.getElementById(id_current).setAttribute("class", "cp_option_cur");
document.getElementById(content_div).innerHTML = new_content;
document.getElementById(content_div).focus(); document.getElementById(content_div).blur(); //this line ensures focus is not on link
}

//Mailing list management
function open_list(listid) {

listid_db = listid.replace("_gps100.com","");

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){
		listpass = "Please wait...";
	if(ajaxRequest.readyState == 4){
		listpass = ajaxRequest.responseText;

listwin = "http://gps100.com/mailman/admin/"+listid+"/members/list/";

var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", listwin);

// setting form target to a window named 'formresult'
form.setAttribute("target", "formresult");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "adminpw");
hiddenField.setAttribute("value", listpass);
listpass = null;
form.appendChild(hiddenField);
document.body.appendChild(form);

// creating the 'formresult' window with custom features prior to submitting the form
var w=screen.width*0.8;
var h=screen.height*0.8;

if (w > 1200) { w = 1200; }
if (h > 800) { h = 800; }

var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);

popupwin = window.open('popup.php', 'formresult', 'scrollbars=yes,menubar=no,height='+h+',width='+w+',resizable=yes,toolbar=no,status=no, top='+top+', left='+left);

form.submit();

	}
}

var theurl = "index.php?fl=1&listname=" + listid_db;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

var ef_form_disp = 0;
function show_add_fwd_form() {

if (ef_form_disp != 1) {
document.getElementById('email_fwd_add_restore').innerHTML = document.getElementById('email_fwd_div').innerHTML;
document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_1').innerHTML;
ef_form_disp = 1;
}

else {
restore_email_forward_list();
}

}

function restore_email_forward_list() {
document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_restore').innerHTML;
ef_form_disp = 0;
}

function clear_default() {
if(document.getElementById('add_gps_email').value == 'E.g. tshannon@gps100.com') {
document.getElementById('add_gps_email').value = '';
}
}

function clear_default_dest() {
if(document.getElementById('add_email_dest').value == 'E.g. bob@gmail.com') {
document.getElementById('add_email_dest').value = '';
}
}



function add_fwd_step1() {

email_try = document.getElementById('add_gps_email').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		email_check = ajaxRequest.responseText;

if (email_check == "EMAIL_INVALID") {
document.getElementById('resultstxt').innerHTML = 'Email invalid - please enter a valid email';
document.getElementById('resultstxt').style.display = 'block';
}

else if (email_check == "FORWARD_EXISTS") {
document.getElementById('resultstxt').innerHTML = 'An email forward already exists for this user';
document.getElementById('resultstxt').style.display = 'block';
}

else if (email_check == "MAILING_LIST_EXISTS") {
document.getElementById('resultstxt').innerHTML = 'A mailing list exists with this name';
document.getElementById('resultstxt').style.display = 'block';
}

else {

document.getElementById('email_fwd_div').innerHTML = document.getElementById('email_fwd_add_2').innerHTML;
document.getElementById('add_gps_email').value = ajaxRequest.responseText;

}

  }
}

var theurl = "index.php?ef=1&gps_email=" + email_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

function add_fwd_step2() {

gps_email = email_try;

dest_try = document.getElementById('add_email_dest').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		email_check = ajaxRequest.responseText;

if (email_check == "EMAIL_INVALID") {
document.getElementById('resultstxt').innerHTML = 'Email invalid - please enter a valid email';
document.getElementById('resultstxt').style.display = 'block';
}

else {


var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", "index.php");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "gps_email");
hiddenField.setAttribute("value", gps_email);

var hiddenField2 = document.createElement("input");              
hiddenField2.setAttribute("name", "email_dest");
hiddenField2.setAttribute("value", email_check);

form.appendChild(hiddenField);
form.appendChild(hiddenField2);
document.body.appendChild(form);

form.submit();


}

  }
}

var theurl = "index.php?ef=1&dest_email=" + dest_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

var ml_form_disp = 0;
function show_add_mailing_list_form() {

if (ml_form_disp != 1) {
document.getElementById('mailing_list_add_restore').innerHTML = document.getElementById('mailing_list_div').innerHTML;
document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_1').innerHTML;
ml_form_disp = 1;
}

else {
restore_mailing_list_list();
}

}

function restore_mailing_list_list() {
document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_restore').innerHTML;
document.getElementById('mailing_list_div').style.overflow = "auto";
ml_form_disp = 0;
}

function clear_mailing_list_default() {
if(document.getElementById('add_mailing_list').value == 'E.g. industrials@gps100.com') {
document.getElementById('add_mailing_list').value = '';
}
}

function add_mailing_list_step1() {

list_name_try = document.getElementById('add_mailing_list').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		list_name_check = ajaxRequest.responseText;

if (list_name_check == "LIST_NAME_INVALID") {
document.getElementById('ml_resultstxt').innerHTML = 'List name invalid - please enter a valid list name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else if (list_name_check == "FORWARD_EXISTS") {
document.getElementById('ml_resultstxt').innerHTML = 'An email forward already exists with this name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else if (list_name_check == "MAILING_LIST_EXISTS") {
document.getElementById('ml_resultstxt').innerHTML = 'A mailing list already exists with this name';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else {

document.getElementById('mailing_list_div').innerHTML = document.getElementById('mailing_list_add_2').innerHTML;
document.getElementById('add_mailing_list').value = ajaxRequest.responseText;

}

  }
}

var theurl = "index.php?ml=1&list_name=" + list_name_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}

function add_mailing_list_step2() {

document.getElementById('mailing_list_div').style.overflow = "hidden";

list_name = list_name_try;

password_try = document.getElementById('list_password').value;

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){

document.getElementById('resultstxt').innerHTML = '';
document.getElementById('resultstxt').style.display = 'none';

		
	if(ajaxRequest.readyState == 4){
		password_check = ajaxRequest.responseText;

if (password_check == "PASSWORD_INVALID") {
document.getElementById('ml_resultstxt').innerHTML = 'Password invalid - please enter a valid password';
document.getElementById('ml_resultstxt').style.display = 'block';
}

else {


var form = document.createElement("form");
form.setAttribute("method", "post");
form.setAttribute("action", "index.php");

var hiddenField = document.createElement("input");              
hiddenField.setAttribute("name", "list_name");
hiddenField.setAttribute("value", list_name);

var hiddenField2 = document.createElement("input");              
hiddenField2.setAttribute("name", "list_password");
hiddenField2.setAttribute("value", password_check);

form.appendChild(hiddenField);
form.appendChild(hiddenField2);
document.body.appendChild(form);

form.submit();


}

  }
}

var theurl = "index.php?ml=1&list_password=" + password_try;
ajaxRequest.open("GET", theurl, true);
ajaxRequest.send(null);

}




</script>

<?php if ($_GET['successmsg']): ?>
<div id="successbar">
<img src="/content_files/images/icons/notification.png" style="vertical-align: text-bottom; width: 18px; height: 15px">

<?php if ($_GET['message'] == 'email_fwd_deleted'): ?>
Email forward deleted
<?php endif; ?>

<?php if ($_GET['message'] == 'email_fwd_added'): ?>
Email forward added
<?php endif; ?>

<?php if ($_GET['message'] == 'mailing_list_added'): ?>
Mailing list added
<?php endif; ?>

</div>
<?php endif; ?>

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding:5px; margin:0 auto; text-align: center; background: black; color: white">Hosting Details</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<?php if (! $this->_tpl_vars['error']['hosting_details']): ?>
<table class="d_table">
<tr>
<td><b>Hosting Package:</b></td>
<td><a href="http://www.mddhosting.com/hosting.php"><?php echo $this->_tpl_vars['cp_admin']['hosting']['package_name']; ?>
</a></td>
</tr>
<tr>
<td><b>Disk Space Used:</b></td>
<td>
<?php if (( $this->_tpl_vars['cp_admin']['hosting']['disk_space_low'] )): ?><b style="color:red"><?php endif; ?>
<?php echo $this->_tpl_vars['cp_admin']['hosting']['disk_space_used']; ?>
 / <?php echo $this->_tpl_vars['cp_admin']['hosting']['disk_space_quota']; ?>

<?php if (( $this->_tpl_vars['cp_admin']['hosting']['disk_space_low'] )): ?></b><?php endif; ?>
</td>
</tr>
<tr>
<td><b>Bandwidth Used:</b></td>
<td>
<?php if (( $this->_tpl_vars['cp_admin']['hosting']['bandwidth_low'] )): ?><b style="color:red"><?php endif; ?>
<?php echo $this->_tpl_vars['cp_admin']['hosting']['bandwidth_used']; ?>
 / <?php echo $this->_tpl_vars['cp_admin']['hosting']['bandwidth_quota']; ?>

<?php if (( $this->_tpl_vars['cp_admin']['hosting']['bandwidth_low'] )): ?></b><?php endif; ?>
</td>
</tr>
<tr>
<td><b>Monthly Cost:</b></td>
<td>$6.38</td>
</tr>
</table>
<?php else: ?>
There was an error fetching the hosting details - script maintenance is required.
<?php endif; ?>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 37px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="admin_list.php"><img src="/content_files/images/icon_edit.gif"></a></div>
Portal Admins</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<?php $_from = $this->_tpl_vars['admin_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
</tr>
<td><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:262px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="activity_log.php?submitted=1&r[user_login]=1&r[user_login_remember_me]=1"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Logins</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<?php $_from = $this->_tpl_vars['login_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%H:%M:%S") : smarty_modifier_date_format($_tmp, "%H:%M:%S")); ?>
</b></td>
<td><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
User Management</div>

<div style="padding: 7px 5px 3px 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">

<div style="float:left; width: 95px; text-align: center">
<a href="user_add.php" style="text-decoration:none">
<img src="/content_files/images/icons/add-user.png" style="width:56px; height: 56px"><BR>
Add User</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="loginas.php" style="text-decoration:none">
<img src="/content_files/images/icons/login.png" style="width:56px; height: 56px"><BR>
Login As</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="user_edit.php" style="text-decoration:none">
<img src="/content_files/images/icons/edit-user.png" style="width:56px; height: 56px"><BR>
Edit User</a>
</div>

<div style="float:left; width: 95px; text-align: center">
<a href="user_groups.php" style="text-decoration:none">
<img src="/content_files/images/icons/group.png" style="width:56px; height: 56px"><BR>
Groups
</a>
</div>

</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="/portal/transcripts/"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Transcripts</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<table class="d_table">
<?php $_from = $this->_tpl_vars['transcript_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</b></td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['sector'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30, "...", 'TRUE') : smarty_modifier_truncate($_tmp, 30, "...", 'TRUE')); ?>
</td>
<td>(<a href="/portal/transcripts/index.php?sector=<?php echo $this->_tpl_vars['i']['sector_s']; ?>
&date=<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
"><?php echo $this->_tpl_vars['i']['message_num']; ?>
 messages</a>)</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
<?php if (! $this->_tpl_vars['error']['emailfwd_list']): ?>
	<div style="float:right"><a href="#" onClick="show_add_fwd_form(); return false;"><img src="/content_files/images/icons/plus.png" style="width:16px; height: 16px"></a></div>
<?php endif; ?>
Email Forwarders</div>

<div id="email_fwd_div" style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<?php if ($this->_tpl_vars['error']['emailfwd_list']): ?>
There was an error fetching the list, most likely caused by an update to cPanel. Script maintenance is required.
<?php else: ?>
<table>
<?php $_from = $this->_tpl_vars['emailfwd_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><a href="index.php?del_fwd=1&del_gps_email=<?php echo $this->_tpl_vars['i']['dest']; ?>
&del_dest_email=<?php echo $this->_tpl_vars['i']['forward']; ?>
" onClick="return confirm('Are you sure you want to delete: <?php echo $this->_tpl_vars['i']['dest']; ?>
 -> <?php echo $this->_tpl_vars['i']['forward']; ?>
?');"><img src="/content_files/images/delete.png" style="margin-right:3px"></a></td>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['dest'])) ? $this->_run_mod_handler('replace', true, $_tmp, "@gps100.com", "") : smarty_modifier_replace($_tmp, "@gps100.com", "")); ?>
</b></td>
<td style="text-align: right"><?php echo $this->_tpl_vars['i']['forward']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
</div>

</div>

<!-- hidden divs -->

<div id="email_fwd_add_1" style="display:none">
<div id="resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
GPS Email: <input type='text' name='gps_email' value='E.g. tshannon@gps100.com' style='width: 280px' onClick='clear_default();' id="add_gps_email"><BR><BR>
<span class='button default strong' onClick='restore_email_forward_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Next' onClick='add_fwd_step1();'></span>
</div>

<div id="email_fwd_add_restore" style="display:none">
&nbsp;
</div>

<div id="email_fwd_add_2" style="display:none">
<div id="resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
<input type='text' name='gps_email' value='' style='width: 165px; background: #ededed; border:0' id="add_gps_email" readonly> ->
<input type='text' name='email_dest' value='E.g. bob@gmail.com' style='width: 190px; margin-left: 5px' id="add_email_dest" onClick="clear_default_dest()";><BR><BR>
<span class='button default strong' onClick='restore_email_forward_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Save' onClick='add_fwd_step2();'></span>
</div>

<!-- end hidden divs -->

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
<?php if (! $this->_tpl_vars['error']['emailfwd_list']): ?>
	<div style="float:right"><a href="#" onClick="show_add_mailing_list_form(); return false;"><img src="/content_files/images/icons/plus.png" style="width:16px; height: 16px"></a></div>
<?php endif; ?>
Mailing Lists</div>

<div id="mailing_list_div" style="padding: 5px; border: 1px dotted black; border-top: none; height: 75px; overflow:auto">
<?php if ($this->_tpl_vars['error']['emailfwd_list']): ?>
There was an error fetching the list, most likely caused by an update to cPanel. Script maintenance is required.
<?php else: ?>
<table>
<?php $_from = $this->_tpl_vars['mailinglists_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><a href="#" onClick="open_list('<?php echo $this->_tpl_vars['i']['listid']; ?>
');return false;"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['list'])) ? $this->_run_mod_handler('replace', true, $_tmp, "@gps100.com", "") : smarty_modifier_replace($_tmp, "@gps100.com", "")); ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
</div>

</div>

<!-- hidden divs -->

<div id="mailing_list_add_1" style="display:none">
<div id="ml_resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
<?php if (! $this->_tpl_vars['mailman_curl_error']): ?>
List name: <input type='text' name='list_name' value='E.g. industrials@gps100.com' style='width: 280px' onClick='clear_mailing_list_default();' id="add_mailing_list"><BR><BR>
<span class='button default strong' onClick='restore_mailing_list_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Next' onClick='add_mailing_list_step1();'></span>
<?php else: ?>
<b>Important Notice:</b> Adding mailing lists through the admin control panel has been automatically disabled.<BR><BR>
<b>Technical details:</b> Script uses PHP cURL to interface with Mailman 2.1.14 (no API), it is likely a software upgrade is culpable. Script maintenance required - contact bwigoder@gps100.com for further info.
<?php endif; ?>
</div>

<div id="mailing_list_add_restore" style="display:none">
&nbsp;
</div>

<div id="mailing_list_add_2" style="display:none">
<div id="ml_resultstxt" style="display:none; color:red; margin-bottom: 4px">&nbsp;</div>
List name: <input type='text' name='list_name' value='' style='width: 280px; background: #ededed; border:0' id="add_mailing_list" readonly><BR>
Password: <input type='text' name='list_password' value='' style='width: 190px; margin-left: 5px' id="list_password"><BR>
<span class='button default strong' onClick='restore_mailing_list_list();'><input type='button' value='Cancel'></span>
<span class='button default strong' style='float:right'><input type='button' value='Save' onClick='add_mailing_list_step2();'></span>
</div>

<!-- end hidden divs -->

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Resumes</div>

<div style="float: left; border: 1px dotted black; border-top: none; border-right: none; padding: 5px; height: 75px; overflow:auto; width: 283px">
<table class="d_table">
<?php $_from = $this->_tpl_vars['resume_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['resume_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</b></td>
<td><a href="/portal/toolbox/resume_upload.php?download=1&id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>
<div style="float: left; border: 1px dotted black; border-top: none; border-left: none; padding: 7px 5px 3px 5px; height: 75px; width: 90px; text-align: center">
<a href="resume_booklets.php">
<img src="/content_files/images/icons/book_icon.png" style="width:72px; height: 55px"><BR>
<div style="padding-top: 2px">Create Books</div>
</a>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Documents</div>
<div class="cp_menu">
<div class="cp_option_cur cp_option_l" id="docs_1_menu"><a href="#" onClick="switch_tab('docs_1'); return false;">Overview</a></div>
<div class="cp_option" id="docs_2_menu"><a href="#" onClick="switch_tab('docs_2'); return false;">User Stats</a></div>
<div class="cp_option" id="docs_3_menu"><a href="#" onClick="switch_tab('docs_3'); return false;">File stats</a></div>
<div class="cp_option" id="docs_4_menu"><a href="#" onClick="switch_tab('docs_4'); return false;">Recycle Bin</a></div>
</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 55px; overflow:auto; clear: left" id="docs_content">

</div>

<div style="display:none" id="docs_1_content">
<table class="d_table">
<tr>
<td><b>Disk Usage:</b></td>
<td><?php echo $this->_tpl_vars['documents_total_disk_usage']; ?>
 MB</td>
</tr>
<tr>
<td><b>Files:</b></td>
<td><?php echo $this->_tpl_vars['documents_file_count']; ?>
 (<?php echo $this->_tpl_vars['documents_disk_usage']; ?>
 MB)</td>
</tr>
<tr>
<td><b>Recycle Bin:</b></td>
<td><a href="#" onClick="switch_tab('docs_4'); return false;"><?php echo $this->_tpl_vars['documents_recycled_file_count']; ?>
 files (<?php echo $this->_tpl_vars['documents_recycled_disk_usage']; ?>
 MB)</a></td>
</tr>
</table>
</div>

<div style="display:none" id="docs_2_content">
<table class="d_table">
<?php $_from = $this->_tpl_vars['dms_user_stats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['totalbytes'])) ? $this->_run_mod_handler('file_size', true, $_tmp) : smarty_modifier_file_size($_tmp)); ?>
</b></td>
<td><a href="/portal/network/view/overview.php?id=<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['first_name']; ?>
 <?php echo $this->_tpl_vars['i']['last_name']; ?>
</a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

<div style="display:none" id="docs_3_content">
<table class="d_table">
<?php $_from = $this->_tpl_vars['dms_file_stats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo $this->_tpl_vars['i']['download_count']; ?>
</b></td>
<td><a href="/portal/documents/folder/folders/<?php echo $this->_tpl_vars['i']['id_folder']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['file_name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50, '...', true, false) : smarty_modifier_truncate($_tmp, 50, '...', true, false)); ?>
</a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

<div style="display:none" id="docs_4_content">
<table class="d_table">
<tr>
<td><b>Disk Usage:</b></td>
<td><?php echo $this->_tpl_vars['documents_recycled_disk_usage']; ?>
 MB</td>
</tr>
<tr>
<td><b>Files:</b></td>
<td><?php echo $this->_tpl_vars['documents_recycled_file_count']; ?>
</td>
</tr>
<tr>
<td><b>Restore Files:</b></td>
<td><a href="/portal/documents/file/recycled_file_folders">View Recycle Bin</a></td>
</tr>
</table>
</div>

<script>
//load first tab into content div
switch_tab('docs_1');
</script>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px 5px 5px 23px; margin:0 auto; text-align: center; background: black; color: white">
<div style="float:right"><a href="surveys.php"><img src="/content_files/images/arrow_double_right.png" style="width:18px; height: 14.6px"></a></div>
Recent Surveys</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 105px; overflow:auto">
<table class="d_table">
<?php $_from = $this->_tpl_vars['survey_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
<td><b><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</b></td>
<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['question'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 35, "", 'TRUE') : smarty_modifier_truncate($_tmp, 35, "", 'TRUE')); ?>
</td>
<td>(<a href="surveys.php?download=<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['total']; ?>
 votes</a>)</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>

</div>

<!-- end segment -->

<!-- begin segment -->

<div style="float: left; width:395px; line-height:normal; margin: 2px">
<div style="padding: 5px; margin:0 auto; text-align: center; background: black; color: white">
Payments</div>

<div class="cp_menu">
<div class="cp_option_cur cp_option_l" id="payments_1_menu"><a href="#" onClick="switch_tab('payments_1'); return false;">Menu</a></div>
<div class="cp_option" id="payments_2_menu"><a href="#" onClick="switch_tab('payments_2'); return false;">Analysts</a></div>
<div class="cp_option" id="payments_3_menu"><a href="#" onClick="switch_tab('payments_3'); return false;">Members</a></div>
<div class="cp_option" id="payments_4_menu"><a href="#" onClick="switch_tab('payments_4'); return false;">Alumni</a></div>
</div>

<div style="padding: 5px; border: 1px dotted black; border-top: none; height: 85px; overflow:auto; clear: left" id="payments_content">

</div>

<div style="display:none" id="payments_1_content">
<table class="d_table" style="text-align: center; margin-top: 4px">
<tr>
  <td>
    <div style="float: left; width: 95px">
      <a href="payments_conferences.php" style="text-decoration: none"><img src="/content_files/images/icons/conferences.png" style="width:57px; height: 61px:"><BR>Conferences</a>
    </div>
    <div style="float: left; width: 95px">
      <a href="payments_invoices.php" style="text-decoration: none"><img src="/content_files/images/icons/invoices.png" style="width:57px; height: 61px:"><BR>Invoices</a>
    </div>
    <div style="float: left; width: 95px">
      <a href="payments_reports.php" style="text-decoration: none"><img src="/content_files/images/icons/reports.png" style="width:57px; height: 61px:"><BR>Reports</a>
    </div>
  </td>
</tr>
</table>
</div>

<div style="display:none" id="payments_2_content">
  <iframe src="/includes/payments/progress.php?group=analyst" class="progress" scrolling="no"></iframe>
</div>

<div style="display:none" id="payments_3_content">
  <iframe src="/includes/payments/progress.php?group=member" class="progress" scrolling="no"></iframe>
</div>

<div style="display:none" id="payments_4_content">
  <iframe src="/includes/payments/progress.php?group=alumni" class="progress" scrolling="no"></iframe>
</div>


<script>
//load first tab into content div
switch_tab('payments_1');
</script>

</div>

<!-- end segment -->


</div>
<div style="clear:both">

<!-- further text goes here -->

<!-- end further text goes here -->

</div>