<?php /* Smarty version 2.6.17, created on 2012-10-16 12:21:36
         compiled from /home/gpscom/public_html/_pages/approach/index.php */ ?>
<div><img src="/content_files/headers/approach.gif" alt="" width="800" height="90" /></div>
<div>
<p>We at GPS believe in line-by-line, bottom-up fundamental analysis and accounting review, but center our faith and beliefs on the foundations of cash flow yield investing. We seek to minimize risk via our edge: thorough analysis of the company focus, product line, financial condition, and management team. Our typical analysis process includes:</p>
<ul>
<li>Analysis of underlying business and competitive advantage</li>
<li>Macroeconomic and industry analysis</li>
<li>Valuation models based on discounted cash flow and relative valuation to determine intrinsic firm value</li>
<li>Evaluation of management ability</li>
<li>Scalability/Addressable Market analysis</li>
<li>Federal policy/Regulatory analysis</li>
</ul>
<p>This discipline allows us to find investments with favorable risk-reward profiles so we may profit from our research.</p>
</div>