<?php /* Smarty version 2.6.17, created on 2013-01-12 12:28:57
         compiled from /home/gpscom/public_html/_pages/portal/admin/resume_booklets.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', '/home/gpscom/public_html/_pages/portal/admin/resume_booklets.php', 40, false),array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/admin/resume_booklets.php', 40, false),)), $this); ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Resume Booklets</h2>
<p>The automatic generator allows you to build a resume booklet using filters and the manual generator is for selecting individuals for a booklet. The final option is for individual resume download.</p>
<BR>

<h2>Automatic Generator</h2>

<script>

			window.addEvent('load', function(){

				// Autocomplete with poll the server as you type
				var t1 = new TextboxList('quickresume', {unique: true, max: 1, plugins: {autocomplete: {
					minLength: 1,
					queryRemote: true,
					remote: {url: '/includes/autocomplete_resume.php'}
				}}});
				//t1.add('John Doe').add('Jane Roe');


			});

//declare filter arrays
lists = new Array();
var th_visible = 0;
var rowcount = 1;

var masterlist = ["f_employer", "f_gender", "f_gps_stage", "f_graduation_year", "f_expertise", "f_hobby", "f_hometown", "f_industry", "f_sector", "f_undergraduate_school"];

lists['f_employer'] = new Array();
lists['f_employer'][0] = 'Current Employer';
lists['f_employer'][1] = '=';
lists['f_employer'][2] = '<select id="f_employer" class="cs wide"><?php $_from = $this->_tpl_vars['employers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_employer'][3] = 0;

lists['f_expertise'] = new Array();
lists['f_expertise'][0] = 'Research Experience';
lists['f_expertise'][1] = '=';
lists['f_expertise'][2] = '<select id="f_expertise" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['expertise']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_expertise'][3] = 0;

lists['f_gender'] = new Array();
lists['f_gender'][0] = 'Gender';
lists['f_gender'][1] = '=';
lists['f_gender'][2] = '<select id="f_gender" class="cs wide"><option value="male">Male</option><option value="female">Female</option></select>';
lists['f_gender'][3] = 0;

lists['f_gps_stage'] = new Array();
lists['f_gps_stage'][0] = 'GPS Status';
lists['f_gps_stage'][1] = '=';
lists['f_gps_stage'][2] = '<select id="f_gps_stage" class="cs wide"><option value="alumni">Alumni</option><option value="analyst">Analyst</option><option value="member">Member</option></select>';
lists['f_gps_stage'][3] = 0;

lists['f_graduation_year'] = new Array();
lists['f_graduation_year'][0] = 'Graduation Year';
lists['f_graduation_year'][1] = '<select id="f_graduation_year_operator" class="cs"><option value=">">></option><option value="=">=</option><option value="<"><</option></select>';
lists['f_graduation_year'][2] = '<select id="f_graduation_year" class="cs wide"><?php $_from = $this->_tpl_vars['year']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value=<?php echo $this->_tpl_vars['i']; ?>
><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_graduation_year'][3] = 0;

lists['f_hobby'] = new Array();
lists['f_hobby'][0] = 'Hobby';
lists['f_hobby'][1] = '=';
lists['f_hobby'][2] = '<select id="f_hobby" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['hobbies']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_hobby'][3] = 0;

lists['f_hometown'] = new Array();
lists['f_hometown'][0] = 'Hometown';
lists['f_hometown'][1] = '=';
lists['f_hometown'][2] = '<select id="f_hometown" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['hometowns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_hometown'][3] = 0;

lists['f_industry'] = new Array();
lists['f_industry'][0] = 'Current Industry';
lists['f_industry'][1] = '=';
lists['f_industry'][2] = '<select id="f_industry" class="cs wide"><?php $_from = $this->_tpl_vars['industries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_industry'][3] = 0;

lists['f_sector'] = new Array();
lists['f_sector'][0] = 'Sector';
lists['f_sector'][1] = '=';
lists['f_sector'][2] = '<select id="f_sector" class="cs wide"><?php $_from = $this->_tpl_vars['tags']['sectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('replace', true, $_tmp, ', ', '_') : smarty_modifier_replace($_tmp, ', ', '_')); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_sector'][3] = 0;

lists['f_undergraduate_school'] = new Array();
lists['f_undergraduate_school'][0] = 'Undergraduate School';
lists['f_undergraduate_school'][1] = '=';
lists['f_undergraduate_school'][2] = '<select id="f_undergraduate_school" class="cs wide"><?php $_from = $this->_tpl_vars['uni']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value=<?php echo $this->_tpl_vars['i']['id']; ?>
><?php echo $this->_tpl_vars['i']['sname']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>';
lists['f_undergraduate_school'][3] = 0;


filter_added = new Array(); //define filter_added array

function add_filter() {
var the_filter = document.cs_form.filterlist.value;

// check not null
if (the_filter != 0) {

// check for headers row
if (th_visible != 1) {
document.getElementById("cs_headers").style.visibility = "visible";
document.getElementById('searchbutton1').style.display = "block";
th_visible = 1;
}

//if (filter_added[the_filter] != 1) {

var x=document.getElementById('cs_table').insertRow(-1);

var a=x.insertCell(0);
var b=x.insertCell(1);
var c=x.insertCell(2);
var d=x.insertCell(3);

a.className = "cs_a";
b.className = "cs_a";
c.className = "cs_a";
d.className = "cs_a";

a.innerHTML = lists[the_filter][0]
b.innerHTML = lists[the_filter][1].replace('" class="cs', '['+lists[the_filter][3]+']" onChange="hide_results();" class="cs');
c.innerHTML = lists[the_filter][2].replace('" class="cs', '['+lists[the_filter][3]+']" onChange="hide_results();" class="cs');
d.innerHTML = '<a href="#" onClick="remove_filter(this,\''+the_filter+'\');return false;"><img src="/content_files/images/delete.png"></a>';

rowcount++; //increase row count by 1
lists[the_filter][3]++;


filter_added[the_filter] = 1; //set variable to identify filter

//} else { alert("Filter already added!"); } //end if filter already added

} else { alert("Please select a filter from the dropdown box"); } //end if filter not null

hide_results();

} //end add_filter function

function remove_filter(mylink,the_filter) {
document.getElementById('cs_table').getElementsByTagName('tbody')[0].removeChild(mylink.parentNode.parentNode);
if (document.getElementById('cs_table').getElementsByTagName('tr').length == 1) { document.getElementById("cs_headers").style.visibility = "hidden"; th_visible = 0; document.getElementById('searchbutton1').style.display = "none"; }
rowcount--;
lists[the_filter][3]--;
hide_results();
             }

function remove_all() {
  if (confirm("Are you sure you want to delete all filters?")) {
var rownum = document.getElementById("cs_table").rows.length;
while (rownum>1) { document.getElementById("cs_table").deleteRow(rownum-1); rownum--; }
document.getElementById("cs_headers").style.visibility="hidden"; th_visible = 0; document.getElementById('searchbutton1').style.display = "none";
hide_results();
  }
}

//AJAX Code
function ajaxSearch(){

//define variables

var varlist = "";
var thatone = "";

for (i=0;i<masterlist.length;i++) {

for (j=0;j<lists[masterlist[i]][3];j++) {
var thisone = masterlist[i] + "[" + j + "]";
var thisop = masterlist[i] + "_operator" + "[" + j + "]";
thatone = thatone + thisone + ",";
if (document.getElementById(thisop) != null) {
varlist = varlist + document.getElementById(thisone).value + "_" + document.getElementById(thisop).value + ",";
} else {
varlist = varlist + document.getElementById(thisone).value + ",";
}

//check for operator
}
}

do_criteria = thatone.split(',');
do_values = varlist.split(',');

do_s_submit = "";

for (var i = 0; i < do_criteria.length; i++)
{
if (do_values[i].length > 0) { do_s_submit = do_s_submit + do_criteria[i] + "=" + do_values[i] + "&"; }
}

do_s_submit = do_s_submit.slice(0,do_s_submit.length-1); //remove final ampersand

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}

document.getElementById("cs-results").style.display = "block";
// Create a function that will receive data sent from the server
ajaxRequest.onreadystatechange = function(){
		document.getElementById("cs-results").innerHTML = "Please wait...";
	if(ajaxRequest.readyState == 4){
		document.getElementById("cs-results").innerHTML = ajaxRequest.responseText;
	}
}

var do_ss_submit = "resume_booklets.php?rs=1&" + do_s_submit;

ajaxRequest.open("GET", do_ss_submit, true);
ajaxRequest.send(null);

}

function view_results() {
document.getElementById("cs-results").style.display = "block";
}

function hide_results() {
document.getElementById("cs-results").style.display = "none";
document.getElementById("cs-results").innerHTML = "";
}

function generate_booklet(ids) {
url = "resume_booklets.php?make_booklet=1&ids="+ids;
window.location = url;
}


</script>

<div class="cs_container">
<form name="cs_form" id="cs_form">
<select name="filterlist" class="filterlist">
<option value="0" SELECTED>** Select One **</option>
<option value="f_employer">Current Employer</option>
<option value="f_industry">Current Industry</option>
<option value="f_gender">Gender</option>
<option value="f_gps_stage">GPS Status</option>
<option value="f_graduation_year">Graduation Year</option>
<option value="f_hobby">Hobby</option>
<option value="f_hometown">Hometown</option>
<option value="f_expertise">Research Experience</option>
<option value="f_sector">Sector</option>
<option value="f_undergraduate_school">Undergraduate School</option>
</select>
<div style="margin: 0 0 0 5px; padding-top: 4px; display: inline">
<span class="button default small strong"><input type="button" value="Add Filter" class="add_filter" onClick="add_filter();"></span>
</div>
<table id="cs_table" border="1">
<tr id="cs_headers"><th>Criteria</th><th style="width: 50px">Operator</th><th>Value</th><th style="width: 30px"><a href=# onClick="remove_all();return false;"><img src="/content_files/images/bin.gif"></a></th></tr>
</table>
<BR>
<div id="searchbutton1" style="display:none">
<span class="button default strong"><input type="button" onClick="ajaxSearch();" value="Search Database"></span>
<BR><BR>
<div id="cs-results" style="background: #ededed; border: 0; width: 750px"></div>
</div>
</form>
</div>
<BR><BR>
<h2>Manual Generator</h2>
<p>Coming Soon...</p>
<BR><BR>
<h2>Individual Resume Download</h2>

<form id="ir_form" name="ir_form" action="resume_booklets.php?ir=1" method="POST">
<div class="form_friends">
<div style="float:left"><input type="text" name="quickresume" value="" id="quickresume" /></div>
<div style="float:left; margin: 0 0 0 5px; padding-top: 2px"><span class="button default strong"><input type="submit" value="Download"></span></div>
</div>
</form>

<div style="clear:both">&nbsp;</div>

</div>