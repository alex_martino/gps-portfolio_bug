<?php /* Smarty version 2.6.17, created on 2013-01-12 13:18:08
         compiled from /home/gpscom/public_html/_pages/portal/admin/loginas.php */ ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Log In As...</h2>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>
<p>Please note that you <u>cannot</u> log in as another admin.</p>
<p>All "log in as" usage will be logged.</p>
<p>You cannot use instant messaging while logged in as another user.</p>
<script>
			window.addEvent('load', function(){

				// Autocomplete with poll the server as you type
				var t1 = new TextboxList('loginas_user', {unique: true, max: 1, plugins: {autocomplete: {
					minLength: 1,
					queryRemote: true,
<?php if (( $_SESSION['user_id'] == 103 )): ?>
					remote: {url: '/includes/autocomplete_loginas.php'}
<?php else: ?>
					remote: {url: '/includes/autocomplete_loginas.php?admin=1'}
<?php endif; ?>
				}}});
				//t1.add('John Doe').add('Jane Roe');


			});
</script>
<form name="loginas" action="loginas.php" method="POST">
<input type="hidden" name="submitted" value="1">
<div class="form_friends">
<div style="float:left"><input type="text" name="loginas_user" value="" id="loginas_user" /></div>
<div style="float:left; margin: 0 0 0 5px; padding-top: 2px"><span class="button default strong"><input type="submit" value="Log In As..."></span></div>
</div>
</form>
</div>