<?php /* Smarty version 2.6.17, created on 2012-11-04 11:02:10
         compiled from /home/gpscom/public_html/_pages/portal/admin/payments_reports.php */ ?>
<div><img src="/content_files/headers/payments.gif" width="800" height="90"></div>
<div>
<script>
function generatereport() {
due_cat = document.generate_report.due_cat.value;
uni = document.generate_report.university.value;
who = document.generate_report.who.value;
theurl = encodeURI("payments_reports.php?download=1&due_cat="+due_cat+"&uni="+uni+"&who="+who);
window.location=theurl;
}
</script>
<h2>Reports</h2>
<?php if ($this->_tpl_vars['error'] > 0): ?>
<div style="color:navy">
<?php echo $this->_tpl_vars['errormsg']; ?>

</div>
<BR>
<?php endif; ?>

<div style="float: left">

<form name="generate_report">

<select name="due_cat">
  <?php $_from = $this->_tpl_vars['due_cat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
  <?php endforeach; endif; unset($_from); ?>
</select>

<select name="university">
  <option value="all">All pods</option>
  <?php $_from = $this->_tpl_vars['universities']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <option value="<?php echo $this->_tpl_vars['i']['ID']; ?>
"><?php echo $this->_tpl_vars['i']['SNAME']; ?>
</option>
  <?php endforeach; endif; unset($_from); ?>
</select>

<select name="who">
  <option value="all">All GPSers</option>
  <option value="analyst">Analysts</option>
  <option value="member">Members</option>
  <option value="alumni">Alumni</option>
</select>

</div>

<div style="margin: 1px 0 0 10px; float: left">
  <span class="button default medium strong"><input type="button" value="Generate" onClick="generatereport();"></span>
</div>

</form>

</div>