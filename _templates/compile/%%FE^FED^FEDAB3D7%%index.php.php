<?php /* Smarty version 2.6.17, created on 2012-10-16 19:32:09
         compiled from /home/gpscom/public_html/_pages/privacy_policy/index.php */ ?>
<div><img src="/content_files/headers/privacy_policy.gif" width="800" height="90"></div>
<div>
	<p>At Global Platinum Securities&trade; LLC, we are 
		committed to safeguarding the personal information 
		that we collect. As we update this policy, 
		we will notify you about significant changes 
		as required by law. All personal data collected 
		in the &quot;contact us&quot; section of 
		the website will be used for internal GPS 
		uses only. We do not sell personal contact 
		information to other businesses or legal 
		entities.</p>
</div>