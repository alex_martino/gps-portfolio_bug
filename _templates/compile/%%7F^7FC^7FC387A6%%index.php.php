<?php /* Smarty version 2.6.17, created on 2012-10-16 13:02:13
         compiled from /home/gpscom/public_html/_pages/philanthropy/index.php */ ?>
<div><img src="/content_files/headers/philanthropy.gif" alt="" width="800" height="90" /></div>
<div>
<p>As a student-run organization driven by education and leadership, we believe that part of our mission is to give back to our community by offering both our expertise and our time. From the idea of "Giving Back", a generation of philanthropy and volunteering has been born. The GPS Giving Back Program was initiated in order to reach many underprivileged individuals who do not have access to opportunities in education and/or employment due to their socioeconomic status.</p>
<p>Through a structured program across the eight GPS pods that comprise our global member base, a club-wide initiative has been started to add value in the capacity of educating students and community individuals to heighten their awareness of financial and economic issues. These charitable events will be carried out each semester at every pod.</p>
<p>GPS believes that part of education and leadership entails giving back to our respective communities, and we hope that these our continuous philanthropic efforts will contribute to the betterment of our respective communities.</p>
</div>