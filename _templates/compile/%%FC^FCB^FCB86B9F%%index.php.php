<?php /* Smarty version 2.6.17, created on 2013-01-13 10:53:46
         compiled from /home/gpscom/public_html/_pages/portal/payments/index.php */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/gpscom/public_html/_pages/portal/payments/index.php', 48, false),array('modifier', 'capitalize', '/home/gpscom/public_html/_pages/portal/payments/index.php', 84, false),)), $this); ?>
<div><img src="/content_files/headers/payments.gif" width="800" height="90">

<?php if ($_GET['pay_success']): ?>
<div id="successbar">
<img src="/content_files/images/icons/notification.png" style="vertical-align: text-bottom; width: 18px; height: 15px">

Invoice successfully paid - thank you!

</div>
<?php endif; ?>


</div>
<script>
function invoice_open(id) {
window.open("https://www.gps100.com/includes/payments/invoice_details.php?id="+id,"invoice","width=900, height=600, left="+((screen.width/2)-450)+",top="+((screen.height/2)-330));
}
function pay_outstanding() {
boxes = document.getElementsByName("do_pay").length;
pay_ids = "";
for (i = 0; i < boxes; i++) {
if (document.getElementsByName("do_pay")[i].checked) {
pay_ids = pay_ids + document.getElementsByName("do_pay")[i].value + ",";
}
}
if (pay_ids.length > 0) {
window.location="https://www.gps100.com/portal/payments/pay_invoice.php?id="+pay_ids;
}
else {
alert("Please select an invoice to pay by ticking the checkbox next to the invoice.");
}
}
</script>

<div>
<h2>Invoices</h2>
<?php if (( $this->_tpl_vars['active_payments_count'] > 0 )): ?>
<table class="admin_table" style="width: 670px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 110px">Invoice Date</th>
<th style="width: 110px">Due Date</th>
<th style="width: 350px">Description</th>
<th style="width: 60px">Amount</th>
<th style="width: 80px">Pay</th>
</tr>
<?php $_from = $this->_tpl_vars['payments']['active']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row">
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['due_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php if (( $this->_tpl_vars['i']['annual_dues_flag'] )): ?>Annual dues: <?php endif; ?><?php echo $this->_tpl_vars['i']['description']; ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');">$<?php echo $this->_tpl_vars['i']['amount']; ?>
</td>
<td style="cursor: default"><input type="checkbox" name="do_pay" value="<?php echo $this->_tpl_vars['i']['id']; ?>
" style="margin-top: 1px" checked></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<BR>
<span class="button default strong"><input type="button" onClick="pay_outstanding();" value="Make Payment"></span>

<!--<b>GPS Payments are currently unavailable</b> - sorry for the inconvenience. We'll update you when they become available again.-->

<?php else: ?>
You have no active invoices.
<?php endif; ?>
<div style="clear:both">
<BR>&nbsp;
</div>
<h2>Past Payments</h2>
<?php if (( $this->_tpl_vars['past_payments_count'] > 0 )): ?>

<table class="admin_table" style="width: 670px; border: 1px solid black">
<tr style="border-bottom: 1px solid black">
<th style="width: 110px">Invoice Date</th>
<th style="width: 110px">Due Date</th>
<th style="width: 350px">Description</th>
<th style="width: 60px">Amount</th>
<th style="width: 80px">Status</th>
</tr>
<?php $_from = $this->_tpl_vars['payments']['past']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row">
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['due_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php if (( $this->_tpl_vars['i']['annual_dues_flag'] )): ?>Annual dues: <?php endif; ?><?php echo $this->_tpl_vars['i']['description']; ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');">$<?php echo $this->_tpl_vars['i']['amount']; ?>
</td>
<td onClick="invoice_open('<?php echo $this->_tpl_vars['i']['id']; ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['status'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

<?php else: ?>
You have not made any payments yet.
<?php endif; ?>

</div>