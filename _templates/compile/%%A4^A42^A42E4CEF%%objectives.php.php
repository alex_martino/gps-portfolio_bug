<?php /* Smarty version 2.6.17, created on 2012-10-19 17:07:09
         compiled from /home/gpscom/public_html/_pages/approach/objectives.php */ ?>
<div><img src="/content_files/headers/objectives.gif" alt="" width="800" height="90" /></div>
<div>
<p>
<p style="margin: 0.0px 0.0px 12.0px 0.0px; font: 12.0px Arial;">Global Platinum Securities follows a long-term-oriented, value-based investment strategy, seeking to identify securities that trade at a substantial disconnect to their intrinsic value. Members are divided into eight industry sectors, based on their interest. They focus on performing fundamental equity research by analyzing the underlying business model and financial strength of global companies.</p>
</p>
</div>