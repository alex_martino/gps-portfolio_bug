<?php /* Smarty version 2.6.17, created on 2013-01-13 12:08:56
         compiled from /home/gpscom/public_html/_pages/portal/admin/universities.php */ ?>
<div><img src="/content_files/headers/admin.gif" width="800" height="90"></div>
<div>
<h2>Settings: Universities</h2>

<?php if (( $this->_tpl_vars['display'] == 'default' )): ?>
<table class="admin_table" style="border: 1px solid black; margin-top: 20px">
<tr style="border-bottom: 1px solid black">
<th>ID</th>
<th>Short Name</th>
<th>Full Name</th>
</tr>
<?php $_from = $this->_tpl_vars['universities']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr class="row">
<td><?php echo $this->_tpl_vars['i']['ID']; ?>
</td>
<td><?php echo $this->_tpl_vars['i']['SNAME']; ?>
</td>
<td><?php echo $this->_tpl_vars['i']['FNAME']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<BR>
<input type="button" value="Add University" onClick="window.location='universities.php?display=add';">
<?php else: ?>
<?php if (( $this->_tpl_vars['display'] == 'add' ) && ( $this->_tpl_vars['error'] != 0 )): ?>
<div class="error"><?php echo $this->_tpl_vars['errormsg']; ?>
</div>
<div style="clear:both">&nbsp;</div>
<?php endif; ?>
<form name="adduni" action="universities.php" method="post">
<input type="hidden" name="addsubmitted" value="1">
<div style="float: left; line-height: 1.4; width: 140px;">
Abbreviated Name:<BR>
Full Name:<BR><BR>
<input type="submit" value="Save">
</div>
<div style="float: left; line-height: 1.4; width: 155px;">
<input type="text" name="sname" value="<?php echo $this->_tpl_vars['sname']; ?>
"><BR>
<input type="text" name="fname" value="<?php echo $this->_tpl_vars['fname']; ?>
">
</div>
<div style="float: left; line-height: 1.4">
E.g. Harvard or LSE<BR>
E.g. Harvard University or London School of Economics
</div>
</form>
<?php endif; ?>

</div>