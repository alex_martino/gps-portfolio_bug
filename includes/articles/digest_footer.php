                                                    <!-- article title -->
                                                    <tr>
                                                        <td valign="top"> <img src="http://www.gps100.com/content_files/mail/double-line-small.gif" alt="" style="display: block;" /> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="article-title" height="30" valign="middle" style="text-transform: uppercase; font-family: Georgia, serif; font-size: 16px; color: #2b2b2b; font-style: italic; border-bottom: 1px solid #c1c1c1;"> How do I contribute? </td>
                                                    </tr>
                                                    <!-- / article title -->
                                                    <!-- article -->
                                                    <tr>
                                                        <td class="copy" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #2b2b2b; line-height: 18px;"> <br />
                                                            <p>Just email your article suggestions to <a href="mailto:articles@gps100.com" style="color: #002868; text-decoration: none"><span style="color: #002868">articles@gps100.com</span></a> with <b>the link in the subject</b> and <b>a short description in the body</b>.</p>
                                                            <p>Or <a href="http://www.gps100.com/apps/">add the bookmarklet</a>.</p>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <!-- / article -->
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- sidebar -->
                                <td valign="top" width="260">
                                    <table cellspacing="0" border="0" cellpadding="0">
                                        <tr>
                                            <td valign="top"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" height="1" alt="" style="display: block;" width="35" /> </td>
                                            <!-- in this issue box -->
                                            <td valign="top">
                                                <table cellspacing="0" border="0" cellpadding="0">
                                                    <tr>
                                                        <td> <img src="http://www.gps100.com/content_files/mail/in-this-issue.gif" alt="" style="display: block;" /> </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" border="0" cellpadding="0">
                                                                <tr>
                                                                    <td valign="top" bgcolor="#dadada"> <img src="http://www.gps100.com/content_files/mail/box-side.gif" height="125" alt="" style="display: block;" width="10" /> </td>
                                                                    <td valign="top">
                                                                        <table cellspacing="0" border="0" cellpadding="0" width="206" style="background-color: #e6e6e6; background-image: url('http://www.gps100.com/content_files/mail/box-bg.gif');">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table cellspacing="0" border="0" cellpadding="0" width="206">
                                                                                        <tr>
                                                                                            <td valign="top" width="20"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="20" /> </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" width="20"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="20" /> </td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;"><strong>01.</strong></td>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="5" /></td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;">Weekly news round-up</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" height="5" alt="" style="display: block;" /></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" width="20"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="20" /> </td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;"><strong>02.</strong></td>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="5" /></td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;">Weekly news round-up</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" height="5" alt="" style="display: block;" /></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" width="20"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="20" /> </td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;"><strong>03.</strong></td>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="5" /></td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;">Weekly news round-up</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" height="5" alt="" style="display: block;" /></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" width="20"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="20" /> </td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;"><strong>04.</strong></td>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" alt="" style="display: block;" width="5" /></td>
                                                                                            <td class="list" style="font-family: Georgia, serif; font-size: 12px;">Weekly news round-up</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><img src="http://www.gps100.com/content_files/mail/spacer.gif" height="10" alt="" style="display: block;" /></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td valign="top" bgcolor="#dadada"> <img src="http://www.gps100.com/content_files/mail/box-side.gif" height="125" alt="" style="display: block;" width="10" /> </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> <img src="http://www.gps100.com/content_files/mail/box-bottom.gif" alt="" style="display: block;" /> </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- / in this issue box -->
                                        </tr>
                                        <tr>
                                            <td valign="top"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" height="20" alt="" style="display: block;" width="1" /> </td>
                                        </tr>
                                        <tr>
                                            <td valign="top"> <img src="http://www.gps100.com/content_files/mail/spacer.gif" height="1" alt="" style="display: block;" width="35" /> </td>
                                            <td valign="top">
                                                <forwardtoafriend><img src="http://www.gps100.com/content_files/mail/share.gif" border="0" alt="" style="display: block;" /></forwardtoafriend>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- / sidebar -->
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- / content -->
                <!-- footer -->
                <tr>
                    <td valign="top">
                        <table cellspacing="0" border="0" cellpadding="0" width="675">
                            <tr>
                                <td valign="top"> </td>
                            </tr>
                            <tr>
                                <td align="center" class="footer" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #2b2b2b; line-height: 18px;"> <img src="http://www.gps100.com/content_files/mail/double-line.gif" alt="" width="675" style="display: block;" />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="15"></td>
                                        </tr>
                                    </table>
                                    Don't want to receive these newsletters anymore?
                                    <a href="http://www.gps100.com/portal/settings/emails.php" style="color: #002868; font-weight: bold; text-decoration: none;">Unsubscribe</a>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="15"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- / footer -->
            </table>
            <!-- / main table -->
        </td>
    </tr>
</table>
</body>
</html>