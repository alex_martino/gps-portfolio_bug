<?
// Stat the session
session_start();

// Include reCAPTCHA
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/captcha/lib.php');

// Check for a valid response
$resp = recaptcha_check_answer($privatekey,
								$_SERVER['REMOTE_ADDR'],
								$_POST["recaptcha_challenge_field"],
								$_POST["recaptcha_response_field"]);

if ($resp->is_valid) {
	$_SESSION['valid_captcha'] = true;
	$result = array('success' => true);
} else {
	$_SESSION['valid_captcha'] = false;
	$result = array('success' => false);
}

// Output result
echo json_encode($result);
?>