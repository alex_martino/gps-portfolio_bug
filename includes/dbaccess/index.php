<?php
/*
 * Mysql Ajax Table Editor
 *
 * Copyright (c) 2008 Chris Kitchen <info@mysqlajaxtableeditor.com>
 * All rights reserved.
 *
 * See COPYING file for license information.
 *
 * Download the latest version from
 * http://www.mysqlajaxtableeditor.com
 */
require_once('Common.php');
class HomePage extends Common
{
	
	function displayHtml()
	{
		echo '<p><a href="edit_users.php">Edit Users</a></p>';
		echo '<p><a href="edit_jobs.php">Edit Jobs</a></p>';
		echo '<p><a href="edit_tags.php">Edit Tags</a></p>';
	}
	
	function HomePage()
	{
		$this->displayHeaderHtml();
		$this->displayHtml();
		$this->displayFooterHtml();
	}
}
$lte = new HomePage();
?>