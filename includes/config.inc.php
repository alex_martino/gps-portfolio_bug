<?

include ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

define("SITE_NAME", "Global Platinum Securities"); // This is used in various places throughout the site to show the sites name
define("PRIMARY_DOMAIN_NAME", $domain); // e.g. domain.com
define("FORCE_PRIMARY_DOMAIN_NAME", false); // True to force this site to only use the primary domain name

/* Database connection information */
define("DB_HOST", $db_host);	// The db host, i.e. localhost
define("DB_NAME", $db_name);			// The name of your db
define("DB_USERNAME", $db_user);		// Your db username, i.e. root
define("DB_PASSWORD", $db_pass);		// Your db password

?>