<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/scripts/sIFR/sifr.css" />
<? if (stristr($_SERVER['PHP_SELF'], '/portal/')) { ?>
<link rel="stylesheet" href="/css/themes/ui-lightness/jquery-ui-1.8.19.custom.css">
<script language="javascript" type="text/javascript" src="/scripts/jquery-1.5.1.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/jquery-ui-1.8.13.custom.min.js"></script>
<? } ?>
<script language="javascript" type="text/javascript" src="/scripts/ext-2.2.1/adapter/ext/ext-base.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/ext-2.2.1/ext-core.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/sIFR/sifr.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/sIFR/sifr-config.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/swfobject/swfobject.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/javascripts.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/count.js"></script>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/network/view/employment.php')) { ?>
<script type="text/javascript" src="http://www.linkedin.com/js/public-profile/widget-os.js"></script>
<script type="text/javascript" src="/scripts/employment.js"></script>
<? } ?>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/vote/') || stristr($_SERVER['PHP_SELF'], '/portal/admin/payments_conferences.php')) { ?>
<script type="text/javascript" src="/scripts/vote.js"></script>
<? } ?>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/research/')) { ?>
<script type="text/javascript" src="/scripts/research.js"></script>
<? } ?>
<? if ($_SERVER['PHP_SELF'] == '/about/photos.php' or $_SERVER['PHP_SELF'] == '/philanthropy/photos.php') { ?>
<link rel="stylesheet" type="text/css" href="/scripts/shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="/scripts/shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<? } ?>
<? if ($_SERVER['PHP_SELF'] == '/lists/' or $_SERVER['PHP_SELF'] == '/lists/index.php') { ?>
<script>
function list_redirect() {
var input = document.managelist.list_name.value;
if (input.length > 0) {
var page = 'http://www.gps100.com/mailman/admin/'+document.managelist.list_name.value+'_gps100.com/members/list';
document.location = page;
}
else {
alert("Please enter a list name");
}
}
</script>
<? } ?>
<? if ($_SERVER['PHP_SELF'] == '/portal/profile/profile_edit.php') { ?>
<script language="javascript" type="text/javascript" src="/scripts/tinymce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode: 'textareas',
		theme: 'simple'
	});
</script>					
<? } ?>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/')) { ?>
<link rel="stylesheet" type="text/css" href="/css/TextboxList.css">
<link rel="stylesheet" type="text/css" href="/css/TextboxList.Autocomplete.css">
<script language="javascript" type="text/javascript" src="/scripts/portal.js"></script>
<? if (stristr($_SERVER['PHP_SELF'], '/portal/network/') || stristr($_SERVER['PHP_SELF'], '/portal/admin/') || stristr($_SERVER['PHP_SELF'], '/portal/autocomplete_formatted_multi_example.php')) { ?>
<script language="javascript" type="text/javascript" src="/scripts/mootools-1.2.1-core-yc.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/GrowingInput.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/TextboxList.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/TextboxList.Autocomplete.js"></script>
<?
}
}
if (stristr($_SERVER['PHP_SELF'], 'portal/network/search.php')) { ?>
<script language="javascript" type="text/javascript" src="/scripts/TextboxList.Autocomplete.force.js"></script>
<? }
if (stristr($_SERVER['PHP_SELF'], 'portal/booklist.php')) { ?>
<!-- Amazon Affiliates code -->
<SCRIPT charset="utf-8" type="text/javascript" src="https://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fglobaplatisec-20%2F8005%2F01b6e817-4fff-42b4-8556-9e3ecf6aab82"> </SCRIPT>
<?
}
?>

<!--[if lte IE 6]>
<script type="text/javascript" src="/scripts/supersleight/supersleight-min.js"></script>
<script language="javascript" type="text/javascript">
	Ext.onReady(function() {
		var nodes = Ext.DomQuery.select('#content_container > div');
		if (nodes.length > 0) {
			for(var i = 0; i < nodes.length; i++) {
				Ext.DomHelper.applyStyles(nodes[i], 'padding: 20px;');
			}
		}
		var first_child = Ext.DomQuery.selectNode('#content_container > div:first-child');
		if (first_child) {
			Ext.DomHelper.applyStyles(first_child, 'padding: 0px;');
		}
	});
</script>
<![endif]-->
<script type"text/javascript">
<!--
function submitenter(myfield,e)
{
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
else return true;

if (keycode == 13)
   {
   myfield.form.submit();
   return false;
   }
else
   return true;
}
//-->
</SCRIPT>

<? if (stristr($_SERVER['PHP_SELF'], '/portal/admin/user_groups.php')) {
include($_SERVER['DOCUMENT_ROOT']."/includes/sortable/include.php");
} ?>

<? if ($_SESSION['admin_override'] > 0 && ($_SESSION['admin_override'] != $_SESSION['member_id']) && strlen($_SESSION['member_id']) > 0) { ?>
<div style="width: 100%; background:black; color: white; text-align: center; padding: 3px 0"><b>Admin Override:</b> <? echo $_SESSION['admin_name']; ?> is currently logged in as <? echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']; ?></div>
<? } ?>