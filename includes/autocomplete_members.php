<?php
session_start(); if (!is_numeric($_SESSION['member_id'])) { die(); } //(quick security check)
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");

if ($_GET['admin']) { $admin_filter = 1; }
if ($_GET['hidden']) { $showhidden = 1; }

//get members
$sql = "SELECT id, first_name, last_name, ug_school_id, graduation_year FROM members WHERE disabled != 1";
if (!$showhidden) { $sql .= " AND hidden != 1"; }
if ($admin_filter) { $sql .= " AND admin != 1"; }
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
$names[] = ucfirst($row[1])." ".ucfirst($row[2]);
$id[] = $row[0];
}


// TextboxList Autocomplete sample data for queryRemote: false (names are fetched all at once when TextboxList is initialized)

// get names (eg: database)
// the format is:
// id, searchable plain text, html (for the textboxlist item, if empty the plain is used), html (for the autocomplete dropdown)

$response = array();
//$names = array('Abraham Lincoln', 'Adolf Hitler', 'Agent Smith', 'Agnus', 'AIAI', 'Akira Shoji', 'Akuma', 'Alex', 'Antoinetta Marie', 'Baal', 'Baby Luigi', 'Backpack', 'Baralai', 'Bardock', 'Baron Mordo', 'Barthello', 'Blanka', 'Bloody Brad', 'Cagnazo', 'Calonord', 'Calypso', 'Cao Cao', 'Captain America', 'Chang', 'Cheato', 'Cheshire Cat', 'Daegon', 'Dampe', 'Daniel Carrington', 'Daniel Lang', 'Dan Severn', 'Darkman', 'Darth Vader', 'Dingodile', 'Dmitri Petrovic', 'Ebonroc', 'Ecco the Dolphin', 'Echidna', 'Edea Kramer', 'Edward van Helgen', 'Elena', 'Eulogy Jones', 'Excella Gionne', 'Ezekial Freeman', 'Fakeman', 'Fasha', 'Fawful', 'Fergie', 'Firebrand', 'Fresh Prince', 'Frylock', 'Fyrus', 'Lamarr', 'Lazarus', 'Lebron James', 'Lee Hong', 'Lemmy Koopa', 'Leon Belmont', 'Lewton', 'Lex Luthor', 'Lighter', 'Lulu');

// make sure they're sorted alphabetically, for binary search tests
array_multisort($names,$id);


foreach ($names as $i => $name)
{
$filename = str_replace(" ","-",strtolower($name))."-cropped.jpg";

$sql = "SELECT ug_school_id, graduation_year FROM members WHERE id = '$id[$i]' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
if ($row[0]>0) {
$sql = "SELECT SNAME FROM universities WHERE ID = '$row[0]' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row2 = mysql_fetch_array($result);
$info = "<BR>".$row2[0]." '".substr($row[1],2,2);
} //end if numeric school id

// Have changed profile photos to the format 'ID-current' or "ID-thumb" in case we have a situation where members have identical names
// As a result, we have a legacy issue, so this code will hopefully help the transition (the old photos are still in the format 'firstname-lastname-thumb.jpg'...)
if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/content_files/member_photos/'. $filename)) { $filename = $id[$i] . "-cropped.jpg"; }
elseif (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/content_files/member_photos/'. $filename)) { $filename = "../../images/male_thumb.png"; }

if (!$_GET['no_image']) { $image = '<img src="/content_files/member_photos/'. $filename .'" />'; }
$response[] = array($id[$i]."_".$name, $name, null, $image . $name .$info);

}

header('Content-type: application/json');
echo json_encode($response);

?>