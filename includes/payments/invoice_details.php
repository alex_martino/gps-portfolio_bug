<?php
//Site config file
require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$email_invoice = $_GET['email_invoice'];
$today = date("Y-m-d");

//start session
session_start();

//if scripts on the server are accessing, bypass security
if ($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR']) {

//check logged in
//are we logged in?
if (is_numeric($_SESSION['member_id'])) { $logged_in = 1; }
else { die("You must be logged in to view invoices. Please close this window and click refresh the page."); }

if ($logged_in == 1) {
require_once ($_SERVER['DOCUMENT_ROOT'] . "/config.php");
$sql = "SELECT disabled, admin FROM members WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
$results = mysql_query($sql) or die("FATAL ERROR - CONTACT ADMIN");
$row = mysql_fetch_array($results);
if ($row['disabled'] == 1) { die("Your account has been disabled."); }
if ($row['admin'] == 1) { $_SESSION['user_is_admin'] = 1; } else { $_SESSION['user_is_admin'] = 0; }
}

}

$invoice_id = $_GET['id'];
if (!is_numeric($invoice_id)) { die("Fatal error."); }

$sql = "SELECT p.id, p.user_id, p.annual_dues_flag, p.description, p.amount, p.due_date, p.status, p.created, p.updated, m.first_name, m.email_address, CONCAT(m.first_name,' ',m.last_name) name, m.graduation_year, u.sname university FROM payments p LEFT JOIN members m ON m.id = p.user_id LEFT JOIN universities u on u.id = m.ug_school_id WHERE p.id = '".$invoice_id."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) < 1) { die ("We could not find the invoice your requested. For help please email: <a href='mailto:payments@gps100.com'>payments@gps100.com</a>."); }

$invoice=mysql_fetch_array($result);
$invoice['due_date_f'] = date("jS F Y",strtotime($invoice['due_date']));
$invoice['created_f'] = date("jS M Y, H:i",strtotime($invoice['created']));
$invoice['updated_f'] = date("jS M Y, H:i",strtotime($invoice['updated']));

//check user is admin, or this is their invoice, or this is a script
if ($invoice['user_id'] != $_SESSION['user_id'] && $_SESSION['user_is_admin'] != 1 && ($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR'])) {
die("You are not authorised to view this invoice. If you believe this is a mistake please contact <a href='mailto:admin@gps100.com'>admin@gps100.com</a>");
}

//check for profile picture
$sql = "SELECT current_photo FROM members WHERE id = '".$invoice['user_id']."' LIMIT 1";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_array($result);
$profile_picture = $row['current_photo'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GPS Invoice: #GPS<?php echo $invoice['id']; ?>
</title>
</head>
<body>

<div style="max-width: 800px; margin: 0 auto; font-family: Verdana">

 <div style="border-bottom: 1px solid #ccc">

  <div style="float: right; font-size: 22px; margin: 25px 0 0 0"><?php if($invoice['status'] == "paid") { echo "RECEIPT"; } else { echo "INVOICE"; } ?></div>
  <div style="float: left">
   <img src="http://www.gps100.com/images/globe.gif" alt="GPS logo" style="width: 70px; height: 64px">
  </div>
  <div style="float: left; font-size: 11px; padding: 5px 0 0 15px">
   <b style="font-size: 13px">Global Platinum Securities LLC</b><BR>
   c/o The Corporation Trust Company<BR>
   Corporation Trust Center, 1209 Orange Street<BR>
   Wilmington, Delaware 19801
  </div>
  <div style="clear:both">&nbsp;</div>

 </div>

 <div style="margin: 20px 0 0 0">

  <div style="float: right">

   <div id="details" style="font-size: 11px; width: 215px; border: 1px dotted #ccc; padding: 10px; line-height: 1.5; margin: 0 0 20px 0; min-height: 50px">
    <div style="float: left">
     Invoice #:<BR>
     Date created:<BR>
     Date updated:
    </div>
    <div class="i" style="float: left; color: navy; font-style: italic; margin: 0 0 0 5px">
     GPS<?php echo $invoice['id']; ?><BR>
     <?php echo $invoice['created_f']; ?><BR>
     <?php echo $invoice['updated_f']; ?>
    </div>
   </div>

   <div id="profile" style="font-size: 11px; width: 215px; border: 1px dotted #ccc; padding: 10px; line-height: 1.5; margin: 0 0 20px 0; text-align: center">
    <b style="font-size: 12px"><?php echo $invoice['name']; ?></b><BR>
    <?php echo $invoice['university']." ".$invoice['graduation_year']; ?><BR>
    <img src="http://www.gps100.com<?php echo $profile_picture; ?>" alt="<?php echo $invoice['name']; ?>" style="margin: 15px 0 0 0; width: 150px">
   </div>

   <div id="login" style="font-size: 11px; width: 215px; border: 1px dotted #ccc; padding: 10px; line-height: 1.5; min-height: 50px; line-height: 1.5; margin: 0">
    <div class="title" style="float: none; text-align: center; font-weight: bold; margin: 0 0 10px 0; line-height: 0.9">Login details</div>
    <div style="float: left">
     Username:<BR>
     Password:
    </div>
    <div class="i" style="float: left; color: navy; font-style: italic; margin: 0 0 0 5px">
     <?php echo $invoice['email_address']; ?><BR>
     <a href="http://www.gps100.com/members/passreset.php?request_reset=1">Reset password</a>
    </div>
   </div>

  </div>

 <div style="float: left; width: 510px; border: 1px dotted #ccc; padding: 15px; font-size: 14px; margin-bottom: 20px">

  Dear <?php echo $invoice['first_name']; ?>,<BR><BR>

<?php

if($invoice['status'] == "paid") { echo "We have received payment for this invoice.<BR><BR>"; } else {

  if ($invoice['due_date'] > $today) {

  echo 'You have received an invoice from Global Platinum Securities.<BR><BR>

    Your invoice is due: <b>' . $invoice['due_date_f'] . '</b><BR><BR>';

  }

  else {

  echo '  Your invoice from the ' . date("jS F Y", strtotime($invoice['created'])) . ' is now overdue.<BR><BR>';

  }

}

if($_GET['email_invoice']) { ?>

  <a href="https://www.gps100.com/portal/payments/">Please click here to pay online</a><BR><BR>

  <?php } ?>

  Thank you for paying it forward.<BR><BR>

  Sincerely,<BR>
  <b>GPS Board of Managers</b>

 </div>

 <div style="float: left; width: 510px; border: 1px dotted #ccc; padding: 15px; height: 85px">

  Invoice details<BR><BR>

  <div style="float: left; font-size: 11px">
   Amount:<BR>
   Description:<BR>
   Status:
  </div>

  <div class="i" style="float: left; font-size: 11px; color: navy; font-style: italic; margin: 0 0 0 5px">
  $<?php echo $invoice['amount']; ?><BR>

  <?php if($invoice['annual_dues_flag']) { echo "Annual dues: "; } echo $invoice['description']; ?><BR>

  <?php echo ucwords($invoice['status']); ?>

  </div>

 </div>

   <div style="clear:both">&nbsp;</div>

 </div>

 <div style="font-size: 11px; border-top: 1px solid #ccc; padding: 15px 0 0 0">

  If you believe this invoice is in error or if you have any queries please email <a href="mailto:payments@gps100.com">payments@gps100.com</a> or click the reply button.

 </div>

</div>

</body>
</html>