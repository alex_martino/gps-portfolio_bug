<?php require_once("db_inc.php"); ?>
<?php
class db_members {
	
   	var $table_name;
   	var $queryh;
 	var $edit_id;
 	var $iserror;
 	var $erromsg;

   	var $id;
	var $lname;
	var $fname;
	var $email;
	var $pass;

   	function db_members() {
		$this->table_name = "members";
		$this->queryh = "";
		$this->edit_id = 0;
		$this->iserror = false;
		$this->erromsg = "";

		$this->id = "";
		$this->lname = "";
		$this->fname = "";
		$this->email = "";
		$this->pass = "";
	}

   	function selectstmt() {
 		return	"SELECT 
		  		id,
				lname,
				fname,
				email,
				pass,
				mdate ";		    
   	}

   	function reset_handle() {
		$dbh = dbconnect();
	    $sql = $this->selectstmt();
	    $sql .= " FROM $this->table_name ";
	    $sql .= "ORDER BY lname ";
	    // $sql .= "ORDER BY sortdate DESC, tb_id DESC ";
 		$this->queryh = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		return mysql_num_rows($this->queryh);
	}

   	function copyrow($row) {
		$this->id = $row['id'];
		$this->lname = $row['lname'];
		$this->fname = $row['fname'];
		$this->email = $row['email'];
		$this->pass = $row['pass'];
	}

   	function copypost() {
		$this->id = trim(stripslashes($_POST['id']));
		$this->lname = trim(stripslashes($_POST['lname']));
		$this->fname = trim(stripslashes($_POST['fname']));
		$this->email = trim(stripslashes($_POST['email']));
		$this->pass = trim(stripslashes($_POST['pass']));
	}

   	function dbseek ($rownum) {
   		if ($rownum > 0)
   			return mysql_data_seek($this->queryh, $rownum);
		else
			return false;   			
   		
	}

   	function next_row() {
		if ($row = mysql_fetch_array($this->queryh))  {
			$this->copyrow($row);
			return true;
		} else {
			return false;
		}
   	}

	function retrieve($id = 0) {
		$this->id = $id;
   		if ($this->id > 0) {
			$dbh = dbconnect();
		    $sql = $this->selectstmt();	    
		    $sql .= " FROM $this->table_name ";
		    $sql .= " WHERE id = '$id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
			if ($row = mysql_fetch_array($result))  {
				$this->copyrow($row);
			}
		} else {
			$this->id = 0;
		}
	}

	function update() {
		$dbh = dbconnect();
	    $sql = $this->selectstmt();	    
	    $sql .= " FROM $this->table_name ";
	    $sql .= " WHERE id <> '$this->id' and email = '$this->email' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		if ($row = mysql_fetch_array($result))  {
			$this->iserror = true;
			$this->errormsg = "Database Error: Duplicate Email Address";
			return;
		}
		if ($this->id <= 0) { // insert
	   		$sql = "INSERT INTO $this->table_name 
		  		(id,
				lname,
				fname,
				email,
				pass,
				mdate)	
		   	VALUES 
		   	(
		   		'', 
   				'".mysql_escape_string($this->lname)."',
   				'".mysql_escape_string($this->fname)."',
   				'".mysql_escape_string($this->email)."',
   				'".mysql_escape_string($this->pass)."',
		   		now())";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
			$this->id = mysql_insert_id();
		} else { //update
			$sql = "UPDATE $this->table_name SET
	   				lname = '".mysql_escape_string($this->lname)."',
	   				fname = '".mysql_escape_string($this->fname)."',
	   				email = '".mysql_escape_string($this->email)."',
	   				pass = '".mysql_escape_string($this->pass)."', ";
		   	$sql .= "mdate = now()
		   			WHERE id ='".$this->id."'";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		}
		$this->iserror = false;
	}

	function delete($id = 0) {
		if ($id > 0) {
			$this->id = $id;
		}
   		if ($this->id > 0) {
			$dbh = dbconnect();
		    $sql = "DELETE ";
		    $sql .= "FROM $this->table_name ";
		    $sql .= "WHERE id = '$this->id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());

		    $sql = "DELETE "; // also delete poll answers
		    $sql .= "FROM pollanswers ";
		    $sql .= "WHERE member_id = '$this->id' ";
			$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		} else {
			$this->id = 0;
		}
	}	

	function writefld($fldname, $width = 10) {
		if ($this->edit_id == $this->id) {
			echo "<input type=\"text\" name=\"$fldname\" value=\"";
			if ($this->iserror) {
				echo htmlspecialchars(stripslashes($_POST[$fldname]));
			} else {
				echo htmlspecialchars($this->$fldname);
			}
			echo "\" size=\"$width\">";
		} else {
			echo $this->$fldname;
		}
		// eval ($code_str);
	}

	function writebutton() {
		if ($this->edit_id == $this->id) {
			echo '<a href="javascript:document.form1.submit();" onMouseOut="MM_swapImgRestore()" ';
			echo 'onMouseOver="MM_swapImage(\'update\',\'\',\'/ima/bt_update1.gif\',1)">';
			echo '<img name="update" border="0" src="/ima/bt_update.gif" width="50" height="17"></a>';
			// echo '<input type="submit" value="Update" class="arial12greylink">';
			echo '<input type="hidden" name="action1" value="update" class="arial12greylink">';
			echo '<input type="hidden" name="id" value="'. $this->id . '" class="arial12greylink">';
		} else {
			echo '<a href="'. $_SERVER['PHP_SELF'] . 
			'?mode=edit&id=' . urlencode($this->id) . '" class="arial12greylink">edit</a> &nbsp;&nbsp;';
			// echo '<a href="wsites.php?cid=' . urlencode($this->id) . '" class="arial12greylink">web</a> &nbsp;&nbsp;';
			echo '<a href="javascript:form1delete(\''. urlencode($this->id) . '\')" class="arial12greylink">delete</a>';
		}
	}
}

?>