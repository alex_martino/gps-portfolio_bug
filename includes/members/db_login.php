<?php require_once("db_inc.php"); ?>
<?php
class db_login {
   	var $table_name;
   	var $queryh;
 	var $edit_id;
 	var $iserror;
 	var $erromsg;

   	var $id;
	var $lname;
	var $fname;
	var $email;

   	function db_login() {
		$this->table_name = "members";
		$this->queryh = "";
		$this->edit_id = 0;
		$this->iserror = false;
		$this->erromsg = "";

		if (isset($_SESSION['user'])) {
			$this->id = $_SESSION['user']['id'];
			$this->lname = $_SESSION['user']['lname'];
			$this->fname = $_SESSION['user']['fname'];
			$this->email = $_SESSION['user']['email'];
			$this->disabled = $_SESSION['user']['disabled'];
		} else {
			$this->id = 0;
			$this->lname = "";
			$this->fname = "";
			$this->email = "";
		}
	}

   	function selectstmt() {
 		return	"SELECT 
		  		id,
				lname,
				fname,
				email,
				pass,
				mdate ";		    
   	}

   	function copyrow($row) {
		$this->id = $row['id'];
		$this->lname = $row['lname'];
		$this->fname = $row['fname'];
		$this->email = $row['email'];
		$this->disabled = $row['disabled'];
	}

   	function valid() {
		$this->erromsg = "";
   		$this->email = trim(stripslashes($_POST['email']));
   		$pass = trim(stripslashes($_POST['pass']));
		$dbh = dbconnect();
	    $sql = $this->selectstmt();	    
	    $sql .= " FROM $this->table_name WHERE email = '$this->email' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		if ($row = mysql_fetch_array($result))  {
			if ($row['pass'] == $pass) {
				$this->copyrow($row);
				$_SESSION['user'] = array(
								"id" => $this->id,
								"email" => $this->email,
								"fname" => $this->fname,
								"lname" => $this->lname
								"disabled" => $this->disabled);
				return true;
			} else {
				$this->erromsg = "Invalid Password.";
				return false;
			}		
		} else {
			$this->erromsg = "No such email address in the database.";
			return false;
		}
   	}

	function redirect() {
		if (isset($_SESSION['returnlogin'])) {
			header("Location: " . $_SESSION['returnlogin']);
		} else {
			header("Location: /members/poll.php");
		}
	}
	
	function name() {
		if (isset($_SESSION['user'])) {
			return $_SESSION['user']['fname'] . " "  . $_SESSION['user']['lname'];
		} else {
			return "";
		}
	}

	function logout() {
		unset($_SESSION['user']);
	}

	function retpw() {
		$this->erromsg = "";
   		$this->email = trim(stripslashes($_POST['email']));
   		$pass = trim(stripslashes($_POST['pass']));
		$dbh = dbconnect();
	    $sql = $this->selectstmt();	    
	    $sql .= " FROM $this->table_name ";
	    $sql .= " WHERE email = '$this->email' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		if ($row = mysql_fetch_array($result))  {
			$subject = "GPS100.COM Member Password Retrieval";
			$message = "\nThe password for $this->email is: ";
			$message .= $row['pass'] . "\n";
			// In case any of our lines are larger than 70 characters, we should use wordwrap()
			$message = wordwrap($message, 70);
			// Send
			if (mail($this->email, $subject, $message)) {
				$this->erromsg = "Password sent to $this->email.";
				return;
			} else {
				$this->erromsg = "Mail functions failed.";			
			}
		} else {
			$this->erromsg = "No such email address in the database.";
			return;
		}
	}

   	function isindb() {
		$this->erromsg = "";
		$dbh = dbconnect();
	    $sql = $this->selectstmt();	    
	    $sql .= " FROM $this->table_name ";
	    $sql .= " WHERE id = '$this->id' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		if ($row = mysql_fetch_array($result))  {
			return true;
		} else {
			$this->erromsg = "The member record no longer exists in database.";
			return false;
		}
   	}
}
?>