<?php require_once("db_inc.php"); ?>
<?php
class db_pollanswers {
	
   	var $table_name;
   	var $queryh;
   	var $tally;

   	var $member_id;
   	var $poll_id;
	var $a1;
	var $a2;
	var $a3;
	var $a4;
	var $a5;

   	function db_pollanswers() {
		$this->table_name = "pollanswers";
		$this->queryh = "";
		$this->tally = array();
		
		$this->reset();
		$this->reset_tally();
	}

   	function reset() {
		$this->member_id = 0;
		$this->poll_id = 0;
		$this->a1 = "";
		$this->a2 = "";
		$this->a3 = "";
		$this->a4 = "";
		$this->a5 = "";
	}

   	function reset_tally() {
   		for ($i = 1; $i <= 5; $i++) {
   			$this->tally[$i]['y'] = 0;
   			$this->tally[$i]['n'] = 0;
   			$this->tally[$i]['o'] = 0;
   		}
   	}
   	function selectstmt() {
 		return	"SELECT 
		  		member_id,
				poll_id,
				a1,
				a2,
				a3,
				a4,
				a5,
				mdate ";		    
   	}

   	function reset_handle() {
		$dbh = dbconnect();
	    $sql = $this->selectstmt();
	    $sql .= " FROM $this->table_name ";
	    $sql .= " WHERE poll_id = '$this->poll_id' ";
	    // $sql .= "ORDER BY member_id ASC, poll_id ASC ";
	    // $sql .= "ORDER BY sortdate DESC, tb_id DESC ";
 		$this->queryh = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		return mysql_num_rows($this->queryh);
	}

   	function copyrow($row) {
		$this->member_id = $row['member_id'];
		$this->poll_id = $row['poll_id'];
		$this->a1 = $row['a1'];
		$this->a2 = $row['a2'];
		$this->a3 = $row['a3'];
		$this->a4 = $row['a4'];
		$this->a5 = $row['a5'];
	}

   	function copypost($idx) {
		$this->member_id = trim(stripslashes($_POST['member_id'][$idx]));
		$this->poll_id = trim(stripslashes($_POST['poll_id'][$idx]));
		for ($i = 1; $i <= 5; $i++) {
			$ai = 'a' . $i;
			$this->$ai = (isset($_POST[$ai][$idx])) ? 'Y' : 'N';
		}
	}

   	function dbseek ($rownum) {
   		if ($rownum > 0)
   			return mysql_data_seek($this->queryh, $rownum);
		else
			return false;   			
   		
	}

   	function next_row() {
		if ($row = mysql_fetch_array($this->queryh))  {
			$this->copyrow($row);
			return true;
		} else {
			return false;
		}
   	}

	function retrieve($member_id, $poll_id, $isinsert = true) {
		if ($member_id <= 0 || $poll_id <= 0)  {
			return;
		}
		$dbh = dbconnect();
	    $sql = $this->selectstmt();	    
	    $sql .= " FROM $this->table_name ";
	    $sql .= " WHERE member_id = '$member_id' AND poll_id = '$poll_id' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
		if ($row = mysql_fetch_array($result))  {
			$this->copyrow($row);
			return true;
		} else {
			$this->reset();
			if ($isinsert) {
				$this->member_id = $member_id;
				$this->poll_id = $poll_id;
				$this->insert();
			}
			return false;
		}
	}

	function insert() {
		$dbh = dbconnect();
   		$sql = "INSERT INTO $this->table_name 
	  		(
	  		member_id,
			poll_id,
			a1,
			a2,
			a3,
			a4,
			a5,
			mdate)	
	   	VALUES 
	   		(
	   		'$this->member_id', 
	   		'$this->poll_id', 
			'".mysql_escape_string($this->a1)."',
			'".mysql_escape_string($this->a2)."',
			'".mysql_escape_string($this->a3)."',
			'".mysql_escape_string($this->a4)."',
			'".mysql_escape_string($this->a5)."',
	   		now())";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
	}

	function update() {
		$dbh = dbconnect();
		$sql = "UPDATE $this->table_name SET
   				a1 = '".mysql_escape_string($this->a1)."',
   				a2 = '".mysql_escape_string($this->a2)."',
   				a3 = '".mysql_escape_string($this->a3)."',
   				a4 = '".mysql_escape_string($this->a4)."',
   				a5 = '".mysql_escape_string($this->a5)."', ";
	   	$sql .= "mdate = now()
		   			WHERE member_id ='".$this->member_id.
		   			"' AND poll_id ='" . $this->poll_id . "'";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
	}

	function delete($member_id, $poll_id) {
		$dbh = dbconnect();
	    $sql = "DELETE ";
	    $sql .= "FROM $this->table_name ";
	    $sql .= "WHERE member_id = '$member_id' AND poll_id = '$poll_id' ";
		$result = @mysql_query($sql, $dbh) or die($sql.mysql_error());
	}	

	function tally($poll_id = 0) {
		if ($poll_id > 0) {
			$this->poll_id = $poll_id;
		}
		$this->reset_tally();
		$this->reset_handle();
		while ($row = mysql_fetch_array($this->queryh))  {
	   		for ($i = 1; $i <= 5; $i++) {
	   			$ai = 'a' . $i;
	   			if ($row[$ai] == 'Y') {
	   				$this->tally[$i]['y']++;
	   			} else if ($row[$ai] == 'N') {
	   				 $this->tally[$i]['n']++;
	   			} else {
	   				$this->tally[$i]['o']++;
	   			}
	   		}
		}
	}

	function tallyprint($idx, $answertype) {
		$cnt = $this->tally[$idx][$answertype];
		$tmpstr = "";
		for ($i = 0; $i < $cnt; $i++) {
			$tmpstr .= '*';
		}
		$tmpstr .= " ($cnt)";
		return $tmpstr;
	}

	function tallywidth($idx, $answertype, $width) {
		$total = $this->tally[$idx]['y'] + $this->tally[$idx]['n'];
		$total += $this->tally[$idx]['o'];
		$cnt = $this->tally[$idx][$answertype];
		$barwidth = ($cnt/$total)*$width;
		return sprintf("%d", $barwidth);
	}

	function isYes($idx) {
		$ai = 'a' . $idx;
		return ($this->$ai == 'Y') ? true : false;
	}

	function isNo($idx) {
		$ai = 'a' . $idx;
		return ($this->$ai == 'N') ? true : false;
	}

}

?>