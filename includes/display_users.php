<!doctype html>
<head>
<link rel="stylesheet" href="/css/style.css" type="text/css">
<link rel="stylesheet" href="/css/portal.css" type="text/css">
<link rel="stylesheet" href="/css/button.css" type="text/css">
</head>
<body>

<?php
$user_list = explode(",",substr($_GET['users'],1));

//sanitize
foreach ($user_list as $value) {
	if (!is_numeric($value)) {
		die("Sorry - there was a problem.");
	}
}

require_once($_SERVER['DOCUMENT_ROOT']."/config.php");

$sql = "SELECT id, first_name, last_name FROM members WHERE id IN(".implode(',',$user_list).") ORDER BY first_name, last_name";
$result = mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result) > 0) {
echo "<table class='display_users_table'>";
}
while ($row = mysql_fetch_array($result)) {
	echo "<tr>";
	echo "<td class='picture'><img src='/content_files/member_photos/".$row['id']."-cropped.jpg'></td>";
	echo "<td class='name'><a href='/portal/network/view/interests.php?id=".$row['id']."' target=_top>".$row['first_name']." ".$row['last_name']."</a><BR></td>";
	echo "<td class='actions'><span class='button default strong'><input type='button' onClick='window.top.location=\"/portal/network/view/interests.php?id=".$row['id']."\";' value='View Profile'></span></td>";
	echo "</tr>";
}
if (mysql_num_rows($result) > 0) {
echo "</table>";
}

?>
</body>
</html>