<?php

// Include main config file
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include database class
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");

session_start();

if ($_GET['logout']) {
unset($_SESSION['member_id']);

		// delete remember me cookie
		setcookie ('rememberme', "", time() - 3600, '/', '.gps100.com');

header("Location: index.php");
}

//check for remember me cookie and process

if (isset($_COOKIE['rememberme']) && !is_numeric($_SESSION['member_id'])) {
parse_str($_COOKIE['rememberme']);

$do_email = $r_email_address;
$do_pass = $r_hash;

$sql = "SELECT * FROM members WHERE email_address = '".$do_email."' AND pass_enc_sec = '".$do_pass."' AND disabled = 0 LIMIT 1";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
if (mysql_num_rows($result) == 1) { //the following will log the user in if the cookie matches a record in the database

				// Setup session variables
				$_SESSION['member_id'] = $row['id'];
				$_SESSION['user_id'] = $row['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $row['first_name'];
				$_SESSION['user_last_name'] = $row['last_name'];
				$_SESSION['user_ug_school_id'] = $row['ug_school_id'];
				$_SESSION['user_is_analyst'] = $row['analyst'];
				$_SESSION['user_is_member'] = $row['member'];
				$_SESSION['user_is_um'] = $row['upper_management'];
				$_SESSION['user_is_board'] = $row['board_manager'];
				$_SESSION['user_is_alumni'] = $row['alumni'];
				$_SESSION['user_is_admin'] = $row['admin'];
				$_SESSION['user_gps_position_held'] = $row['gps_position_held'];

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('user_login_remember_me','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','".$_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING']."','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());
}
}

if (is_numeric($_SESSION['member_id'])) {
$logged_in = 1;
}

if ($_POST['submitted']) {

$db_connection = new db_connection('members');

$_POST['email'] = addslashes($_POST['email']);
$_POST['password'] = addslashes($_POST['password']);

if (empty($_POST['email'])) {
$error++;
$errormsg .= "Please enter your email address.<BR>";
}

elseif (empty($_POST['password'])) {
$error++;
$errormsg .= "Please enter your password.<BR>";
}

else { //both email and password not empty

//sanitise and check
$results = $db_connection->select_array("email_address = '" . mysql_real_escape_string($_POST['email']) . "' AND pass_enc_sec = sha1('" . sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8')");

if (is_array($results) && $results[0]['disabled'] != 1) { //row found, do log in


				// Setup session variables
				if (!session_is_registered('member_id')) {
					session_register('member_id');
				}
				$_SESSION['member_id'] = $results[0]['id'];
				$_SESSION['user_id'] = $results[0]['id']; //new "user_" convention for all user variables - hence repeat
				$_SESSION['user_first_name'] = $results[0]['first_name'];
				$_SESSION['user_last_name'] = $results[0]['last_name'];
				$_SESSION['user_ug_school_id'] = $results[0]['ug_school_id'];
				$_SESSION['user_is_member'] = $results[0]['member'];
				$_SESSION['user_is_um'] = $results[0]['upper_management'];
				$_SESSION['user_is_board'] = $results[0]['board_manager'];
				$_SESSION['user_is_alumni'] = $results[0]['alumni'];
				$_SESSION['user_is_admin'] = $results[0]['admin'];

				// Add log
				$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mobile_login','".$_SESSION['user_id']."','".date("Y-m-d H:i:s")."','mobile_site','".$_SERVER['REMOTE_ADDR']."')";
				$result = mysql_query($sql) or die(mysql_error());

				// Check for remember me, if checked set remember me cookie
				if ($_POST['remember_me']) {
				$cookie_path = "/";
				$cookie_timeout = 60 * 60 * 24 * 30; 
				$user_nohash = $_POST['email_address'];
				$pass_hash = sha1(sha1(mysql_real_escape_string($_POST['password'])) . "3l0bsbTdC3x8gS79F9jK3piAHxBtg5aJMrT05sp9C1VsdX6v8");
				setcookie ('rememberme', 'r_email_address='.$user_nohash.'&r_hash='.$pass_hash, time() + $cookie_timeout, "/", '.gps100.com');
				}

				header("Location: index.php");



} //end row found, do log in

elseif (!is_array($results)) {
$error++;
$errormsg .= "Username and password combination not valid.";
}

elseif ($results[0]['disabled'] == 1) {
$error++;
$errormsg .= "Account disabled.";
	$sql = "INSERT INTO logs (type, uid, time, data, ip_address) VALUES ('mobile_login_fail_disabled','".$results[0]['id']."','".date("Y-m-d H:i:s")."','mobile_site','".$_SERVER['REMOTE_ADDR']."')";
	$result = mysql_query($sql) or die(mysql_error());
}

} //end both email and password not empty
} //end form posted
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>GPS Mobile Directory
</title>
<style type="text/css">
.labels { width: 80px; float:left }
.errormsg { color:red }
#phone_number { border: 0 }
a { text-decoration: none }
</style>
<script type="text/javascript">
function update() {
document.getElementById('phone_number').innerHTML = document.selectphone.phone.value;
}
</script>
</head>
<body>
<h1>GPS Mobile Directory</h1>

<?php if($error) {
echo "<div class='errormsg'>".$errormsg."</div>";
} ?>

<?php if (!$logged_in) { ?>
<form action="index.php?login=1" method="post">
<div id="log_in">
<input type="hidden" name="submitted" value="1">
<div class="labels"><label for=email accesskey=u>Email:</label></div>
<input type="text" name="email" id="email" value="<? echo $_POST['email']; ?>"><br>
<div class="labels"><label for=password accesskey=p>Password:</label></div>
<input type="password" name="password" id="password" value="<? echo $_POST['password']; ?>"><br>
<input type="checkbox" name="remember_me" value="1" onKeyPress="return submitenter(this,event)" /> Remember me<br>
<div class="labels"><input type="submit" value="Log In"></div>
</div>
</form>
<?php }
else {
echo "<form name='selectphone'>";
echo "<p>Welcome " . $_SESSION['user_first_name'] . "! (<a href=\"index.php?logout=1\">Logout</a>)</p>";
echo "<select name='phone' onChange='update();'>";
echo "<option value='' SELECTED>** Please Select **</option>";
$sql = "SELECT CONCAT(first_name, ' ', last_name) fullname, contact_number_country_code, contact_number, email_address FROM members WHERE disabled != 1 AND LENGTH(contact_number) > 0 AND LENGTH(contact_number_country_code) > 0 ORDER BY first_name ASC";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_array($result)) {
if (substr($row['contact_number'], 0, 1) == '0') { $row['contact_number'] = substr($row['contact_number'], 1); }
$entry[] = $row;
echo "<option value='&lt;a href=\"tel:+".$row['contact_number_country_code'].$row['contact_number']."\"&gt;"."+".$row['contact_number_country_code'].$row['contact_number']."&lt;/a&gt; | &lt;a href=\"mailto:".$row['email_address']."\"&gt;". $row['email_address'] ."&lt;/a&gt;'>".$row['fullname']."</option>";
}
echo "</select>";
echo "</form>";
?>
<BR>
<div id="phone_number"></div>

<?php } ?>
</body>
</html>