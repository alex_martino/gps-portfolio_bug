<?
// Start the session for this page
session_start();
header("Cache-control: private");

// Include site config file
//include ($_SERVER['DOCUMENT_ROOT'] . "/includes/config.inc.php");

// Include function libraries
include ($_SERVER['DOCUMENT_ROOT'] . "/includes/classes/database.class.php");
include ($_SERVER['DOCUMENT_ROOT'] . "/siteadmin/modules/calendar_administration/includes/iCalcreator.class.php");

$db_connection = new db_connection("_sa_calendar_admin");
$results = $db_connection->select_array();

$c = new vcalendar();

foreach ($results as $result) {
	$e = new vevent();
	$e->setProperty('tzid', 'US-Pacific');

	$e->setProperty('summary', $result["title"]);
	$e->setProperty('description', str_replace("\n", "", str_replace("\r\n", "", $result["description"])));

	if ($result["start_date"] && $result["start_time"] && $result["end_date"] && $result["end_time"]) {
		$e->setProperty('dtstart', date("Y", $result["start_timestamp"]), date("m", $result["start_timestamp"]), date("d", $result["start_timestamp"]), date("H", $result["start_timestamp"]), date("i", $result["start_timestamp"]));
		$e->setProperty('dtend', date("Y", $result["end_timestamp"]), date("m", $result["end_timestamp"]), date("d", $result["end_timestamp"]), date("H", $result["end_timestamp"]), date("i", $result["end_timestamp"]));
	} else {
		$e->setProperty('dtstart', date("Ymd", $result["start_timestamp"]), array("VALUE" => "DATE"));
	}

	if ($result["recurrence"] == "daily") {
		$rrules["FREQ"] = strtoupper($result["recurrence"]);
		$rrules["INTERVAL"] = ($result["recurrence_interval"] != 0 ? $result["recurrence_interval"] : 1);
		if ($result["recurrence_end_timestamp"]) {
			$rrules["UNTIL"] = date("Ymd", $result["recurrence_end_timestamp"]);
		}
		$e->setProperty('rrule', $rrules);
	} else if ($result["recurrence"] == "weekly") {
		$rrules["FREQ"] = strtoupper($result["recurrence"]);
		$rrules["INTERVAL"] = ($result["recurrence_interval"] != 0 ? $result["recurrence_interval"] : 1);
		if ($result["recurrence_end_timestamp"]) {
			$rrules["UNTIL"] = date("Ymd", $result["recurrence_end_timestamp"]);
		}
		//if ($result["recurrence_options"]) {
			//$rrules["BYDAY"] = $result["recurrence_options"];
		//}
		$e->setProperty('rrule', $rrules);
	} else if ($result["recurrence"] == "monthly") {
		$rrules["FREQ"] = strtoupper($result["recurrence"]);
		$rrules["INTERVAL"] = ($result["recurrence_interval"] != 0 ? $result["recurrence_interval"] : 1);
		$rrules["BYMONTHDAY"] = date("d", $result["start_timestamp"]);
		if ($result["recurrence_end_timestamp"]) {
			$rrules["UNTIL"] = date("Ymd", $result["recurrence_end_timestamp"]);
		}
		$e->setProperty('rrule', $rrules);
	} else if ($result["recurrence"] == "yearly") {
		$rrules["FREQ"] = strtoupper($result["recurrence"]);
		$rrules["INTERVAL"] = ($result["recurrence_interval"] != 0 ? $result["recurrence_interval"] : 1);
		if ($result["recurrence_end_timestamp"]) {
			$rrules["UNTIL"] = date("Ymd", $result["recurrence_end_timestamp"]);
		}
		$e->setProperty('rrule', $rrules);
	}
	
	//$e->setProperty('categories', 'ANNIVERSARY');
	
	$c->setComponent($e);
}

$str = $c->createCalendar();
echo $str;
?>