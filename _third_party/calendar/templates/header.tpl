<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>{CALENDAR_NAME} - {DISPLAY_DATE}</title>
<link rel="stylesheet" type="text/css" href="/scripts/ext-1.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="/calendar/templates/default.css" />
<link rel="stylesheet" type="text/css" href="/css/styles.css" />
<!-- switch rss_available on -->
<link rel="alternate" type="application/rss+xml" title="RSS" href="/calendar/rss/rss.php?cal={CAL}&amp;rssview={CURRENT_VIEW}">
<!-- switch rss_available off -->
<script language="javascript" type="text/javascript" src="/scripts/ext-1.0/adapter/ext/ext-base.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/ext-1.0/ext-all.js"></script>	
{EVENT_JS}
<? include ($_SERVER["DOCUMENT_ROOT"] . "/includes/head.inc.php"); ?>
</head>

<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td>