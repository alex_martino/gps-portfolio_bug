{HEADER}

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="calborder">
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td class="navback">{DISPLAY_DATE}</td>
		<td align="right" width="120" class="navback">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><a class="psf" href="day.php?getdate={GETDATE}"><img src="images/day_on.gif" alt="{L_DAY}" border="0" /></a></td>
			<td><a class="psf" href="week.php?getdate={GETDATE}"><img src="images/week_on.gif" alt="{L_WEEK}" border="0" /></a></td>
			<td><a class="psf" href="month.php?getdate={GETDATE}"><img src="images/month_on.gif" alt="{L_MONTH}" border="0" /></a></td>
			<td><a class="psf" href="year.php?getdate={GETDATE}"><img src="images/year_on.gif" alt="{L_YEAR}" border="0" /></a></td>
			<td><a class="psf" href="month.php?getdate={PREV_MONTH}"><img src="images/left_day.gif" alt="[Previous Month]" border="0" /></a></td>
			<td><a class="psf" href="month.php?getdate={NEXT_MONTH}"><img src="images/right_day.gif" alt="[Next Month]" border="0" /></a></td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	<br />
	
	{MONTH_LARGE|+0}
	<br />

	<!-- switch showbottom on -->
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="calborder">
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td class="navback">{L_THIS_MONTHS}</td>
		<td align="right" width="120" class="navback">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><a class="psf" href="month.php?getdate={PREV_MONTH}"><img src="images/left_day.gif" alt="[Previous Month]" border="0" /></a></td>
			<td><a class="psf" href="month.php?getdate={NEXT_MONTH}"><img src="images/right_day.gif" alt="[Next Month]" border="0" /></a></td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	
	<tr>
	<td>
		<table width="100%" cellspacing="1" cellpadding="4" border="0">
		<!-- loop showbottomevents_odd on -->
		<tr align="left" valign="top">
		<td width="170" nowrap="nowrap"><a class="psf" href="day.php?getdate={DAYLINK}">{START_DATE}</a><br /><span class="V9G">{START_TIME}</span></td>
		<td>{EVENT_TEXT}<br /><span class="V9G">{CALNAME}</span></td>
		</tr>
		<!-- loop showbottomevents_odd off -->
		
		<!-- loop showbottomevents_even on -->
		<tr align="left" valign="top">
		<td width="170" nowrap="nowrap" bgcolor="#EEEEEE"><a class="psf" href="day.php?getdate={DAYLINK}">{START_DATE}</a><br /><span class="V9G">{START_TIME}</span></td>
		<td bgcolor="#EEEEEE">{EVENT_TEXT}<br /><span class="V9G">{CALNAME}</span></td>
		</tr>
		<!-- loop showbottomevents_even off -->
		</table>
	</td>
	</tr>
	</table>
	<!-- switch showbottom off -->

{FOOTER}
