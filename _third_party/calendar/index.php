<?php
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

if (!isset($ALL_CALENDARS_COMBINED)) {
	$ALL_CALENDARS_COMBINED = 'all_calendars_combined';
}

include (BASE . "config.inc.php");

if (isset($_COOKIE['phpicalendar'])) {
	$phpicalendar 		= unserialize(stripslashes($_COOKIE['phpicalendar']));
	$default_view 		= $phpicalendar['cookie_view'];
}

$check = array ('day', 'week', 'month', 'year');
if (in_array($default_view, $check)) {
	$default_view = '/calendar/' . $default_view . '.php';
} else {
	die;
}

if (isset($_GET['cpath'])){
	$default_view .= '?cpath=' . $_GET['cpath'];
}

header("Location: $default_view");
?>
