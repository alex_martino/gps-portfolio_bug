<?php 
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

include_once(BASE . 'functions/init.inc.php'); 
require_once(BASE . 'functions/template.php');
include_once(BASE . 'functions/date_functions.php');

$unix_time = strtotime($_POST['date']);
$display_date = localizeDate($dateFormat_day, $unix_time);

$event = unserialize(stripslashes($_REQUEST['event_data']));
$organizer = unserialize($event['organizer']);
$attendee = unserialize($event['attendee']);

// Format event time
// All day
if ($_POST['time'] == -1) {
	$event_times = $lang['l_all_day'] . ' - ' . $display_date;
} else {
	$event_times = date($timeFormat, $event['start_unixtime']) . ' - ' .  date($timeFormat, $event['end_unixtime']) . ' - ' . $display_date; 
}

$event['description'] 	= stripslashes(utf8_decode(urldecode($event['description'])));
$event['event_text'] = stripslashes(utf8_decode(urldecode($event['event_text'])));
$event['location'] = stripslashes(utf8_decode(urldecode($event['location'])));

if (is_array($organizer)) {
	$i = 0;
	$display .= $organizer_lang . ' - ';
	foreach ($organizer as $val) {	
		$organizers .= $organizer[$i]["name"] . ', ';
		$i++;
	}
	$organizer = substr($organizers,0,-2);
}
if (is_array($attendee)) {
	$i = 0;
	$display .= $attendee_lang . ' - ';
	foreach ($attendee as $val) {	
		$attendees .= $attendee[$i]["name"] . ', ';
		$i++;
	}
	$attendee = substr($attendees, 0, -2);
}

if ($event['location']) {
	if ($event['url'] != '') $event['location'] = '<a href="'.$event['url'].'" target="_blank">'.stripslashes($event['location']).'</a>';
}else{
	$event['location'] = stripslashes($event['location']);
}

if (!$event['location'] && $event['url']) {
	$event['location'] = '<a href="'.$event['url'].'" target="_blank">'.$event['url'].'</a>';
	$lang['l_location'] = 'URL';
}

if (sizeof($attendee) == 0) $attendee = '';
if (sizeof($organizer) == 0) $organizer = '';

switch ($event['status']){
	case 'CONFIRMED':
		$event['status'] =	$lang['l_status_confirmed'] ; 
		break;
	case 'CANCELLED':
		$event['status'] =	$lang['l_status_cancelled'] ; 
		break;
	case 'TENTATIVE':
		$event['status'] =	$lang['l_status_tentative'] ; 
		break;
}
?>
<? /*{'success':'true','charset':'<? print $charset; ?>','cal':'<? print $event['calname']; ?>','event_text':'<? print $event['event_text']; ?>','event_times':'<? print $event_times; ?>','description':'<? print urlencode($event['description']); ?>','organizer':'<? print $organizer; ?>','attendee':'<? print $attendee; ?>','status':'<? print $event['status']; ?>','location':'<? print stripslashes($event['location']); ?>','cal_title_full':'<? print $event['calname'].' '.$lang['l_calendar']; ?>','l_organizer':'<? print $lang['l_organizer']; ?>','l_attendee':'<? print $lang['l_attendee']; ?>','l_status':'<? print $lang['l_status']; ?>','l_location':'<? print $lang['l_location']; ?>'}*/ ?>
{'success':'true','cal':'<? print urlencode($event['calname']); ?>','event_text':'<? print urlencode($event['event_text']); ?>','event_times':'<? print urlencode($event_times); ?>','description':'<? print urlencode($event['description']); ?>','cal_title_full':'<? print urlencode($event['calname'].' '.$lang['l_calendar']); ?>'}