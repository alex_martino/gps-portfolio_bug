<?php
define('BASE', $_SERVER['DOCUMENT_ROOT'] . '/_third_party/calendar/');

$current_view = 'day';

if (isset($_GET['jumpto_day'])) {
	$jumpto_day_time = strtotime($_GET['jumpto_day']);
	if ($jumpto_day_time == -1) {
		$getdate = date('Ymd', time() + $second_offset); 
	} else {
		$getdate = date('Ymd', $jumpto_day_time);
	}
}

require_once(BASE . 'functions/ical_parser.php');
require_once(BASE . 'functions/list_functions.php');
require_once(BASE . 'functions/template.php');

header("Content-Type: text/html; charset=$charset");

if ($minical_view == 'current') $minical_view = 'day';

$weekstart 		= 1;
$unix_time 		= strtotime($_REQUEST['getdate']);
$today_today 	= date('Ymd', time() + $second_offset);  
$next_day		= date('Ymd', strtotime("+1 day",  $unix_time));
$prev_day 		= date('Ymd', strtotime("-1 day",  $unix_time));

$display_date = localizeDate($dateFormat_day, $unix_time);
$sidebar_date = localizeDate($dateFormat_week_list, $unix_time);
$start_week_time = strtotime(dateOfWeek($getdate, $week_start_day));


// select for calendars
$list_icals 	= display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED));
$list_years 	= list_years();
$list_months 	= list_months();
$list_weeks 	= list_weeks();
$list_jumps 	= list_jumps();
$list_calcolors = list_calcolors();
$list_icals_pick = display_ical_list(availableCalendars($ALL_CALENDARS_COMBINED), TRUE);

$page = new Page(BASE . 'templates/day.tpl');

$page->replace_files(array(
	'header'			=> BASE . 'templates/header.tpl',
	'event_js'			=> BASE . 'functions/event.js',
	'footer'			=> BASE . 'templates/footer.tpl',
    'sidebar'           => BASE . 'templates/sidebar.tpl'
));

$page->replace_tags(array(
	'charset'			=> $charset,
	'default_path'		=> '',
	'getdate'			=> $getdate,
	'getcpath'			=> "&cpath=$cpath",
	'cpath'				=> $cpath,
	'calendar_name'		=> $cal_displayname,
	'current_view'		=> $current_view,
	'display_date'		=> $display_date,
	'sidebar_date'		=> $sidebar_date,
	'next_day' 			=> $next_day,
	'prev_day'	 		=> $prev_day,
	'show_goto' 		=> '',
	'list_icals' 		=> $list_icals,
	'list_icals_pick' 	=> $list_icals_pick,
	'list_years' 		=> $list_years,
	'list_months' 		=> $list_months,
	'list_weeks' 		=> $list_weeks,
	'list_jumps' 		=> $list_jumps,
	'legend'	 		=> $list_calcolors,
	'style_select' 		=> $style_select,
	'l_goprint'			=> $lang['l_goprint'],
	'l_calendar'		=> $lang['l_calendar'],
	'l_legend'			=> $lang['l_legend'],
	'l_tomorrows'		=> $lang['l_tomorrows'],
	'l_jump'			=> $lang['l_jump'],
	'l_todo'			=> $lang['l_todo'],
	'l_day'				=> $lang['l_day'],
	'l_week'			=> $lang['l_week'],
	'l_month'			=> $lang['l_month'],
	'l_year'			=> $lang['l_year'],
	'l_pick_multiple'	=> $lang['l_pick_multiple'],
	'l_subscribe'		=> $lang['l_subscribe'],
	'l_download'		=> $lang['l_download'],
	'l_search'			=> $lang['l_search']
));

$page->draw_day($page);
$page->tomorrows_events($page);
$page->draw_subscribe($page);
$page->output();
?>
